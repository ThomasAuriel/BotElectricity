from datetime import datetime

import numpy
import pandas
from matplotlib import ticker

from data import Category, Country, DataPower, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import PeriodPlot
from lib import StringUtils
from lib.pandas import PandasSelection

# Dates
start = pandas.Timestamp(datetime(2016, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2023, 1, 1), tz="UTC")
# Countries
listCountries = [
    Country.BE,
    Country.CH,
    Country.DE,
    Country.DK,
    Country.ES,
    Country.FR,
    Country.GB,
    Country.IT,
    Country.NO,
    Country.PL,
    Country.PT,
    Country.SE,
]
listCountries = [
    Country.FR,
]
# Categories
listCategory = None

# Data
generation_all = DataPower.Generation(start=start, end=end)

generation_all.dataframe = PandasSelection.convertCategory(
    data=generation_all.dataframe,
    categories=Category.ENTSOE_TO_DETAIL_MIX,
    groupingOperation=numpy.sum,
)

for c in listCountries:
    generation = PandasSelection.selectCountry(data=generation_all, country=[c.iso])

    for cat in ["Coal", "Gas", "Nuclear", "Wind", "Solar", "Hydro"]:

        generation_tmp = PandasSelection.selectCategory(data=generation.copy(), category=cat)

        filename = StringUtils.format_map(
            "./output/test/test_bot_specific/02/02-test_Generation_Cumulative_{country}_{category}.png",
            {"country": c.iso, "category": cat.replace("/", "-")},
        )
        title = StringUtils.format_map(
            "Electricity generation for {category} in {country}\n{start} to {end}",
            {"category": cat.replace("/", "-"), "country": c.iso},
        )

        # Linear figures
        figure = Figure(
            filename=filename,
            data=[generation_tmp],
            title=title,
        )
        s1 = Subplot(
            functionPlot=PeriodPlot.linear,
            abscisseIndex="years",
            categoryIndex=Index.country,
            majorLocatorInterval=ticker.AutoLocator(),
            minorLocatorInterval=ticker.AutoMinorLocator(),
            majorFormatter=None,
            minorFormatter=None,
        )
        figure.addSubplots(subplots=[s1])
        figure.plot(plotIfExist=True)

# balance_all = DataPower.Balance(start=start, end=end)
# for c in listCountries:
# balance = PandasSelection.selectCountry(data=balance_all, country=[c.iso])

# # Balance
# filename = StringUtils.format_map(
#     "./output/test/test_bot_specific/02/02-test_Balance_{country}.png", {"country": c.iso}
# )
# title = StringUtils.format_map(
#     "Detailed electricity balance for {country}\n{start} to {end}", {"country": c.iso}
# )

# # Linear figures
# figure = Figure(
#     filename=filename,
#     data=[balance],
#     title=title,
# )
# s1 = Subplot(
#     functionPlot=PeriodPlot.linear,
#     abscisseIndex="years",
#     categoryIndex=index.country,
#     majorLocatorInterval=ticker.AutoLocator(),
#     minorLocatorInterval=ticker.AutoMinorLocator(),
#     majorFormatter=None,
#     minorFormatter=None,
# )
# figure.addSubplots(subplots=[s1])
# figure.plot(plotIfExist=True)

# load_all = DataPower.Load(start=start, end=end)
# for c in listCountries:
# balance = PandasSelection.selectCountry(data=balance_all, country=[c.iso])

# # Balance
# filename = StringUtils.format_map(
#     "./output/test/test_bot_specific/02/02-test_Balance_{country}.png", {"country": c.iso}
# )
# title = StringUtils.format_map(
#     "Detailed electricity balance for {country}\n{start} to {end}", {"country": c.iso}
# )

# # Linear figures
# figure = Figure(
#     filename=filename,
#     data=[balance],
#     title=title,
# )
# s1 = Subplot(
#     functionPlot=PeriodPlot.linear,
#     abscisseIndex="years",
#     categoryIndex=index.country,
#     majorLocatorInterval=ticker.AutoLocator(),
#     minorLocatorInterval=ticker.AutoMinorLocator(),
#     majorFormatter=None,
#     minorFormatter=None,
# )
# figure.addSubplots(subplots=[s1])
# figure.plot(plotIfExist=True)


print("end")
