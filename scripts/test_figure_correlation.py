from datetime import datetime

import pandas

from data import Country, DataPower, DataPower_ENTSOE, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import CorrelationPlot

## TODO : To finish

# Dates
start = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
listCountry = [Country.DE, Country.DK, Country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)
generation.downloader = DataPower_ENTSOE.Generation_ENTSOE()

# generation.download()

# Box figures
figure = Figure(
    filename="./output/test/test_figure/test_correlation/test_correlation_01.png",
    data=[generation],
    title="Generation",
    size=(48, 32),
)
s1 = Subplot(
    functionPlot=CorrelationPlot.matrix,
    abscisseIndex=Index.country,
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=CorrelationPlot.matrix,
    abscisseIndex=Index.country,
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=2,
)
s3 = Subplot(
    functionPlot=CorrelationPlot.matrix,
    abscisseIndex=Index.category,
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=3,
)
s4 = Subplot(
    functionPlot=CorrelationPlot.matrix,
    abscisseIndex=Index.category,
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=4,
)
figure.addSubplots(subplots=[s1, s2, s3, s4])
figure.plot(plotIfExist=True)


print("end")
