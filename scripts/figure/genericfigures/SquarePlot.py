from enum import Enum
from typing import Any, Callable, List, Optional, Tuple

from pandas import DataFrame
from data.Data import GenericData
from lib.matplotlib import FigureColor
from lib.pandas import PandasIndexes, PandasOperation, PandasSelection
from matplotlib.patches import Rectangle


class Animal(Enum):
    sum = 1
    mean = 2


def squareplot(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=None,
    minorLocatorInterval=None,
    majorFormatter=None,
    minorFormatter=None,
    cumulative: bool = True,
    grouping: Callable = DataFrame.mean,
    sortValues: bool = True,
    **kwargs
):
    """Plot data following a square representation

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    cumulative: bool
        Cumulative square along the x-axis

    Return
    ------
    xlabel: str
    ylabel: str
    """

    abscicaData = data[dataIndex[0]]
    ordinateData = data[dataIndex[1]]

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(
        data=abscicaData.dataframe, level=categoryIndex
    )
    FigureColor.defineColorCycle(categories)

    """ Compute """
    abscicaData = abscicaData.dataframe.copy()
    abscicaData = PandasOperation.sumOver(abscicaData, levels=[categoryIndex, abscisseIndex])
    abscicaData = grouping(abscicaData, level=[categoryIndex])

    ordinateData = ordinateData.dataframe.copy()
    ordinateData = PandasOperation.sumOver(ordinateData, levels=[categoryIndex, abscisseIndex])
    ordinateData = grouping(ordinateData, level=[categoryIndex])

    # Sort data
    if sortValues:
        data = abscicaData * ordinateData
        data = data.sort_values(by="data", axis=0, ascending=False)
        abscicaData = abscicaData.reindex(data.index)
        ordinateData = ordinateData.reindex(data.index)

        categories = PandasIndexes.getIndexeUniqueValues(data=abscicaData, level=categoryIndex)

    """ Plot the data """
    originX = 0
    for idx, category in enumerate(categories):

        tmp_abscicaData = abscicaData.copy()
        tmp_ordinateData = ordinateData.copy()

        x = (
            PandasSelection.selectDataframe(tmp_abscicaData, categoryIndex, category)
            .values[0]
            .flatten()
        )[0]
        y = (
            PandasSelection.selectDataframe(tmp_ordinateData, categoryIndex, category)
            .values[0]
            .flatten()
        )[0]

        # Create a rectangle
        rect = Rectangle(
            xy=(originX, 0),
            width=x,
            height=y,
            facecolor=FigureColor.colors[category],
            edgecolor=FigureColor.colors[category],
        )

        # Add the patch to the Axes
        ax.add_patch(rect)

        # For the legend
        ax.scatter(originX + (x) / 2, (y) / 2, label=category, c=FigureColor.colors[category])

        if cumulative:
            originX += x

    """ Attribute of the figure """

    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)

    # Ticks
    if majorLocatorInterval:
        ax.xaxis.set_major_locator(majorLocatorInterval)
    if majorFormatter:
        ax.xaxis.set_major_formatter(majorFormatter)
    if minorLocatorInterval:
        ax.xaxis.set_minor_locator(minorLocatorInterval)
    if minorFormatter:
        ax.xaxis.set_minor_formatter(minorFormatter)

    return None, None
