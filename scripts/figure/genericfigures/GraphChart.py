from typing import Any, List, Optional, Tuple

import networkx as nx
import numpy
from data import Index
from data.Country import getAllCountriesPos
from data.Data import GenericData
from lib.matplotlib import FigureColor
from lib.pandas import PandasIndexes, PandasOperation
from pandas import DataFrame, Series


def plot(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    **kwargs
) -> Tuple[str, str]:
    """
    plot
        Plot the graph on a realistic map or a classic graph

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    countryIndex = Index.country
    neighborsIndex = Index.category

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[countryIndex, neighborsIndex])

    countries = PandasIndexes.getIndexeUniqueValues(tmp, countryIndex)
    neighbors = PandasIndexes.getIndexeUniqueValues(tmp, neighborsIndex)

    pos = getAllCountriesPos()
    # Filter countries and neighbors
    countries = [c for c in countries if c in pos]
    neighbors = [n for n in neighbors if n in pos]

    """ Plot the data """

    # Create graph
    G = nx.DiGraph()

    for c in countries:
        for n in neighbors:

            try:
                exchange = tmp.xs(c, level=countryIndex, drop_level=False).xs(
                    n, level=neighborsIndex, drop_level=False
                )
                if isinstance(exchange, Series):
                    exchange = exchange.values[0]
                if isinstance(exchange, DataFrame):
                    exchange = exchange.values.flatten()[0]
                if exchange > 0:
                    G.add_edge(n, c, weight=exchange)

            except Exception:
                pass

    weights = numpy.array([G[u][v]["weight"] for u, v in G.edges()])
    maxWeight = numpy.max(a=weights)
    edgeWidth = numpy.sqrt(weights / maxWeight) * 3
    arrowWidth = 20
    nx.draw_networkx_edges(
        G, pos=pos, node_size=1000, arrows=True, arrowsize=arrowWidth, width=edgeWidth
    )
    nx.draw_networkx_edge_labels(
        G,
        pos,
        edge_labels={(u, v): "{:.0f}".format(G[u][v]["weight"]) for u, v in G.edges()},
        font_size=12,
    )

    for country in neighbors:
        nx.draw_networkx_nodes(
            G,
            pos={country: pos[country]},
            nodelist=[country],
            node_size=500,
            node_color=FigureColor.colors[country],
        )
    nx.draw_networkx_labels(G, pos, {n: n for n in neighbors})

    return None, None
