from typing import Any, List, Optional, Tuple

from data.Data import GenericData
from lib.pandas import PandasOperation
from calculation import CurveCalculation
from matplotlib import ticker
from figure.genericfigures import LinearPlot


def linearplot(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
    **kwargs
) -> Tuple[str, str]:
    """Plot unstacked duration curve

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])
    tmp = CurveCalculation.getDurationCurve(tmp)
    data.dataframe = tmp

    """ Plot the data """
    return LinearPlot.linearplot(
        data=[data],
        dataIndex=[0],
        ax=ax,
        artists=artists,
        abscisseIndex="index",
        categoryIndex=categoryIndex,
        yLim=yLim,
        majorLocatorInterval=majorLocatorInterval,
        minorLocatorInterval=minorLocatorInterval,
        majorFormatter=majorFormatter,
        minorFormatter=minorFormatter,
        **kwargs
    )


def stackplot(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
    **kwargs
) -> Tuple[str, str]:
    """Plot stacked duration curve

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    majorLocatorInterval, minorLocatorInterval
        Where to place the locator

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])
    tmp = CurveCalculation.getDurationCurve(tmp)
    data.dataframe = tmp

    """ Plot the data """
    return LinearPlot.stackplot(
        data=[data],
        dataIndex=[0],
        ax=ax,
        artists=artists,
        abscisseIndex="index",
        categoryIndex=categoryIndex,
        yLim=yLim,
        majorLocatorInterval=majorLocatorInterval,
        minorLocatorInterval=minorLocatorInterval,
        majorFormatter=majorFormatter,
        minorFormatter=minorFormatter,
        **kwargs
    )
