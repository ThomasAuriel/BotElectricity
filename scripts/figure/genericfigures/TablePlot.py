from textwrap import wrap
from typing import List

import numpy as np
from data.Data import GenericData

# import scipy.stats as stats
from lib.pandas import PandasOperation, PandasSelection

import matplotlib


def createStatisticalTable(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    abscisseIndex: str,
    categoryIndex: str,
    **kwargs
):

    """Plot a table in an ax object

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display

    Return
    ------
    xlabel: str
    ylabel: str
    """
    columns = []
    rowLabels = []
    rowValues = []
    for i in dataIndex:

        d = data[i].copy()

        """ Compute """
        df = d.dataframe.copy()
        df = PandasOperation.sumOver(df, levels=[abscisseIndex, categoryIndex])

        """ Plot the data """

        unit = d.unit
        dataRowLabels = [
            "Count\nNb points",
            "Mean\n{}".format(unit.symbol),
            "Std\n{}".format(unit.symbol),
            "Minimum\n{}".format(unit.symbol),
            "Maximum\n{}".format(unit.symbol),
            "25%\n{}".format(unit.symbol),
            "50%\n{}".format(unit.symbol),
            "75%\n{}".format(unit.symbol),
        ]

        dataRowValues = []
        columns = []

        for column in d.dataframe.index.unique(level=categoryIndex):

            tmp = PandasSelection.selectDataframe(df.copy(), categoryIndex, column)
            tmpStats = PandasOperation.stats(tmp)

            dataRowValues.append(
                [
                    "{0:.0f}".format(tmpStats.iloc[:, 0]["count"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["mean"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["std"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["min"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["max"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["25%"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["50%"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["75%"]),
                ]
            )

            columns.append(column)

        dataRowLabels = np.array(dataRowLabels)
        dataRowValues = np.array(dataRowValues)

        rowLabels.append(dataRowLabels)
        rowValues.append(dataRowValues)

    # Concatenate
    rowLabels = np.hstack(rowLabels)
    rowValues = np.hstack(rowValues)

    # Transpose the matrix
    rowLabels = np.transpose(rowLabels)
    rowValues = np.transpose(rowValues)

    table = addTable(ax=ax, cell_text=rowValues, columns=columns, rows=rowLabels)

    ax.axis("off")

    return "", ""


def addTable(ax, cell_text, columns, rows):

    columns = ["\n".join(wrap(str(text), 15)) for text in columns]

    table = matplotlib.table.table(
        ax=ax,
        cellText=cell_text,
        cellLoc="center",
        rowLabels=rows,
        rowLoc="center",
        colLabels=columns,
        colLoc="center",
        loc="lower center",
    )
    table.auto_set_font_size(False)
    table.scale(1, 4)

    return table
