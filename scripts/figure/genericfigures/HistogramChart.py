from typing import Any, List, Optional, Tuple

from data.Data import GenericData
from lib.matplotlib import FigureColor
from lib.pandas import PandasIndexes, PandasOperation, PandasSelection


def histogram(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    **kwargs
) -> Tuple[str, str]:
    """Plot histogram chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    kwargs["bins"]: int, List[float]
        bins used for the histogram

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    if "bins" in kwargs.keys():
        bins = kwargs["bins"]
    else:
        bins = 100

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])

    """ Plot the data """
    for idx, category in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp.copy(), categoryIndex, category)

        # tmp_category.hist(
        #     by=abscisseIndex,
        #     ax=ax,
        #     bins=bins,
        #     density=True,
        #     label=category,
        #     color=colorCycle[idx]
        # )
        count, bins, patches = ax.hist(
            tmp_category, density=True, bins=bins, label=category, color=colorCycle[idx]
        )

    """ Attribute of the figure """
    # Axis
    # pyplot.xticks(
    #     ticks=numpy.arange(len(data.dataframe.index.unique(abscisseIndex))),
    #     labels=PandasIndexes.getIndexeUniqueValues(data=tmp, level=abscisseIndex),
    # )

    # Limits
    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)

    # # Ticks
    # if majorLocatorInterval:
    #     ax.xaxis.set_major_locator(majorLocatorInterval)
    # if majorFormatter:
    #     ax.xaxis.set_major_formatter(majorFormatter)
    # if minorLocatorInterval:
    #     ax.xaxis.set_minor_locator(minorLocatorInterval)
    # if minorFormatter:
    #     ax.xaxis.set_minor_formatter(minorFormatter)

    xlabel = data.getGenericName()
    ylabel = "Density [Ratio]"
    return xlabel, ylabel
