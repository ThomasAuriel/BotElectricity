from typing import Any, List, Optional, Tuple

import numpy
from data.Data import GenericData
from lib.matplotlib import FigureColor, FigureUtils
from lib.pandas import PandasIndexes, PandasOperation, PandasSelection
from matplotlib import pyplot


def barchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    **kwargs
):
    """
    Plot bar chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])

    """ Plot the data """
    barWidth = 0.8 / len(categories)
    x = (
        numpy.arange(len(PandasIndexes.getIndexeUniqueValues(data.dataframe, level=abscisseIndex)))
        - barWidth * len(categories) / 2
    )

    # For later annotation
    rects = []
    previousValues = [0]
    for idx, category in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp.copy(), categoryIndex, category)
        y = tmp_category.values.flatten()

        x = x + barWidth

        rect = ax.bar(x, y, label=category, width=barWidth, color=colorCycle[idx])

        rects.extend(rect)
        previousValues.extend(y)

    """ Attribute of the figure """
    # Axis
    pyplot.xticks(
        ticks=numpy.arange(len(data.dataframe.index.unique(abscisseIndex))),
        labels=PandasIndexes.getIndexeUniqueValues(data=tmp, level=abscisseIndex),
    )

    # Limits
    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)
    # else:
    #     previousValues = [0, *list(itertools.chain(previousValues))]
    #     mininum = numpy.min(previousValues)
    #     maximum = numpy.max(previousValues)
    #     FigureUtils.setMinMax(ax=ax, minimum=mininum, maximum=maximum)

    # Ticks
    # if majorLocatorInterval:
    #     ax.xaxis.set_major_locator(majorLocatorInterval)
    # if majorFormatter:
    #     ax.xaxis.set_major_formatter(majorFormatter)
    # if minorLocatorInterval:
    #     ax.xaxis.set_minor_locator(minorLocatorInterval)
    # if minorFormatter:
    #     ax.xaxis.set_minor_formatter(minorFormatter)

    return None, None


def stack_barchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    **kwargs
):
    """
    Plot stacked bar chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])

    """ Plot the data """
    x = numpy.arange(len(PandasIndexes.getIndexeUniqueValues(data.dataframe, level=abscisseIndex)))

    # positiveData = data.dataframe.clip(lower=0, upper=None)
    # negativeData = data.dataframe.clip(lower=None, upper=0)

    """ Positive Values """
    # Previous values

    previousPositiveValue = [0] * len(x)
    previousNegativeValues = [0] * len(x)

    for idx, category in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp.copy(), categoryIndex, category)
        tmp_category = tmp_category.clip(lower=0, upper=None)

        """ Positive Values """
        y = tmp_category.clip(lower=0, upper=None).values.flatten()
        rect = ax.bar(x, y, bottom=previousPositiveValue, label=category, color=colorCycle[idx])
        previousPositiveValue = y + previousPositiveValue

        """ Negative Values """
        y = tmp_category.clip(lower=None, upper=0).values.flatten()
        rect = ax.bar(x, y, bottom=previousNegativeValues, color=colorCycle[idx])
        previousNegativeValues = y + previousNegativeValues

    """ Attribute of the figure """
    # Axis
    ax.set_xticks(x, PandasIndexes.getIndexeUniqueValues(data=tmp, level=abscisseIndex))

    # Limits
    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)
    else:
        previousValues = [0, *previousPositiveValue, *previousNegativeValues]
        mininum = numpy.min(previousValues)
        maximum = numpy.max(previousValues)
        FigureUtils.setMinMax(ax=ax, minimum=mininum, maximum=maximum)

    # # Ticks
    # if majorLocatorInterval:
    #     ax.xaxis.set_major_locator(majorLocatorInterval)
    # if majorFormatter:
    #     ax.xaxis.set_major_formatter(majorFormatter)
    # if minorLocatorInterval:
    #     ax.xaxis.set_minor_locator(minorLocatorInterval)
    # if minorFormatter:
    #     ax.xaxis.set_minor_formatter(minorFormatter)

    return None, None


def cumulative_barchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    **kwargs
) -> Tuple[str, str]:
    """
    Plot cmulative bar chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex])

    """ Plot the data """

    """ Previous Values """
    previousValue = [0]
    previousValues = [0]

    for idx, category in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp.copy(), categoryIndex, category)

        y = tmp_category.values.flatten()
        rect = ax.bar([idx], y, bottom=previousValue, label=category, color=colorCycle[idx])
        previousValue += y

        previousValues.extend(previousValue)

    """ Attribute of the figure """
    # Axis
    ax.set_xticks(
        numpy.arange(idx + 1), PandasIndexes.getIndexeUniqueValues(data=tmp, level=categoryIndex)
    )

    # Limits
    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)
    else:
        mininum = numpy.min(previousValues)
        maximum = numpy.max(previousValues)
        FigureUtils.setMinMax(ax=ax, minimum=mininum, maximum=maximum)

    # # Ticks
    # if majorLocatorInterval:
    #     ax.xaxis.set_major_locator(majorLocatorInterval)
    # if majorFormatter:
    #     ax.xaxis.set_major_formatter(majorFormatter)
    # if minorLocatorInterval:
    #     ax.xaxis.set_minor_locator(minorLocatorInterval)
    # if minorFormatter:
    #     ax.xaxis.set_minor_formatter(minorFormatter)

    return None, None
