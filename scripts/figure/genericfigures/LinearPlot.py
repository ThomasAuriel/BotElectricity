from typing import Any, List, Optional, Tuple

import matplotlib.dates as mdates
from data.Data import GenericData
from lib.matplotlib import FigureColor
from lib.pandas import PandasIndexes, PandasOperation, PandasSelection, PandasUtils


def linearplot(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.AutoDateLocator(),
    majorFormatter=mdates.DateFormatter("%d-%m-%Y"),
    minorFormatter=None,
    cumulative: bool = False,
    **kwargs
) -> Tuple[str, str]:
    """Plot unstacked linear chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    cumulative: boolean
        Sum cumulatively along the abscica

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp = data.dataframe.copy()

    if abscisseIndex:
        tmp = PandasOperation.sumOver(
            tmp,
            levels=[categoryIndex, abscisseIndex],
            cumulativeAxis=abscisseIndex if cumulative else None,
        )

    """ Plot the data """
    for idx, category in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp.copy(), categoryIndex, category)
        y = tmp_category.values.flatten()

        tmp_category = tmp_category.reset_index()
        if abscisseIndex is not None:
            x = tmp_category[abscisseIndex].values.flatten()
        else:
            x = range(len(y))

        ax.plot(
            x,
            y,
            label=category,
            antialiased=True,
            linestyle="-",
            marker="",
            linewidth=1,
            color=colorCycle[idx],
        )

    """ Attribute of the figure """
    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)

    # Ticks
    if majorLocatorInterval:
        ax.xaxis.set_major_locator(majorLocatorInterval)
    if majorFormatter:
        ax.xaxis.set_major_formatter(majorFormatter)
    if minorLocatorInterval:
        ax.xaxis.set_minor_locator(minorLocatorInterval)
    if minorFormatter:
        ax.xaxis.set_minor_formatter(minorFormatter)

    return None, None


def stackplot(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.AutoDateLocator(),
    majorFormatter=mdates.DateFormatter("%d-%m-%Y"),
    minorFormatter=None,
    **kwargs
) -> Tuple[str, str]:
    """Plot stacked linear chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    majorLocatorInterval, minorLocatorInterval
        Where to place the locator

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])
    tmp.dataframe = PandasUtils.removeNaNInf(tmp.dataframe)

    """ Plot the data """

    """ Positive Values """
    xValues = []
    yValuesPos = []
    for (idx, category) in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp, categoryIndex, category).clip(
            lower=0,
            upper=None,
        )
        y = tmp_category.values.flatten()

        tmp_category = tmp_category.reset_index()
        if abscisseIndex is not None:
            x = tmp_category[abscisseIndex].values.flatten()
        else:
            x = range(len(y))

        xValues = x
        yValuesPos.append(y)

    ax.stackplot(
        xValues,
        yValuesPos,
        labels=categories,
        antialiased=True,
        linestyle="solid",
        colors=colorCycle,
    )

    """ Negative Values """
    xValues = []
    yValuesNeg = []
    for (idx, category) in enumerate(categories):

        tmp_category = PandasSelection.selectDataframe(tmp, categoryIndex, category).clip(
            lower=None, upper=0
        )
        y = tmp_category.values.flatten()
        tmp_category = tmp_category.reset_index()
        x = tmp_category[abscisseIndex].values.flatten()

        xValues = x
        yValuesNeg.append(y)

    ax.stackplot(
        xValues,
        yValuesNeg,
        antialiased=True,
        linestyle="solid",
        colors=colorCycle,
    )

    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)

    # Ticks
    if majorLocatorInterval:
        ax.xaxis.set_major_locator(majorLocatorInterval)
    if majorFormatter:
        ax.xaxis.set_major_formatter(majorFormatter)
    if minorLocatorInterval:
        ax.xaxis.set_minor_locator(minorLocatorInterval)
    if minorFormatter:
        ax.xaxis.set_minor_formatter(minorFormatter)

    return None, None
