from typing import Any, List

import matplotlib.pyplot as plt
import numpy
import pandas
from data.Data import GenericData
from lib.pandas import PandasIndexes
from matplotlib import pyplot


def matrix(
    data: GenericData,
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: None = None,
    **kwargs
):
    """Plot unstacked linear chart

    Parameter
    ---------
    data: GenericData
        Data to plot
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Not used
    """

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = tmp.groupby(by=[categoryIndex, abscisseIndex]).sum()
    tmp = tmp.reset_index().rename(columns={"data": None})
    tmp = tmp.reset_index().pivot(index=abscisseIndex, columns=categoryIndex)
    tmp = tmp.groupby(by=[categoryIndex], axis=1).sum()

    """ Plot the data """
    pandas.plotting.scatter_matrix(tmp, diagonal="kde", ax=ax)

    """ Attribute of the figure """
    # Axis
    pyplot.xticks(
        ticks=numpy.arange(len(data.dataframe.index.unique(abscisseIndex))),
        labels=PandasIndexes.getIndexeUniqueValues(data=tmp, level=abscisseIndex),
    )
    # if majorLocatorInterval and abscisseIndex is not None:
    #     ax.xaxis.set_major_locator(majorLocatorInterval)
    # if majorFormatter and abscisseIndex is not None:
    #     ax.xaxis.set_major_formatter(majorFormatter)
    # if minorLocatorInterval and abscisseIndex is not None:
    #     ax.xaxis.set_minor_locator(minorLocatorInterval)
    # if minorFormatter and abscisseIndex is not None:
    #     ax.xaxis.set_minor_formatter(minorFormatter)

    # # Ticks
    plt.xticks(rotation=30, ha="right")

    # if majorLocatorInterval:
    #     ax.xaxis.set_major_locator(majorLocatorInterval)
    # if majorFormatter:
    #     ax.xaxis.set_major_formatter(majorFormatter)
    # if minorLocatorInterval:
    #     ax.xaxis.set_minor_locator(minorLocatorInterval)
    # if minorFormatter:
    #     ax.xaxis.set_minor_formatter(minorFormatter)
