from typing import Any, List, Optional, Tuple

import numpy
from data.Data import GenericData
from lib.matplotlib import FigureColor, FigureUtils
from lib.pandas import PandasIndexes, PandasOperation, PandasSelection
from matplotlib import pyplot as plt


def scatterchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=None,
    minorLocatorInterval=None,
    majorFormatter=None,
    minorFormatter=None,
    centered: bool = False,
    linearRegression: bool = False,
    drawLine: bool = False,
    **kwargs
):
    """Plot a scatter chart of two dataset.

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot
    center: bool
        Center the data (remove the mean value)
    linearRegression: bool
        Display a linear regression
    drawLine: bool
        Display lines a*x+b where b is always 0 and a fluctuate

    Return
    ------
    xlabel: str
    ylabel: str
    """

    abscicaData_df = data[dataIndex[0]]
    ordinateData_df = data[dataIndex[1]]

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(
        data=abscicaData_df.dataframe, level=categoryIndex
    )
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    abscicaData = abscicaData_df.dataframe.copy()
    abscicaData = PandasOperation.sumOver(abscicaData, levels=[categoryIndex, abscisseIndex])

    ordinateData = ordinateData_df.dataframe.copy()
    ordinateData = PandasOperation.sumOver(ordinateData, levels=[categoryIndex, abscisseIndex])

    """ Plot the data """
    for idx, category in enumerate(categories):

        # Get the simple values
        xValue = PandasSelection.selectDataframe(
            abscicaData, categoryIndex, category
        ).values.flatten()
        yValue = PandasSelection.selectDataframe(
            ordinateData, categoryIndex, category
        ).values.flatten()

        # Get the averages values
        xAverage = numpy.nanmean(xValue)
        yAverage = numpy.nanmean(yValue)

        if centered:
            xValue = xValue - xAverage
            yValue = yValue - yAverage

        plt.scatter(
            xValue,
            yValue,
            antialiased=True,
            label=None,
            marker=".",
            s=100,
            color=colorCycle[idx],
            alpha=0.5,
            zorder=2,
        )
        # Add to the plot with annotation
        if not centered:
            plt.scatter(
                xAverage,
                yAverage,
                antialiased=True,
                label="{}".format(category),
                color=colorCycle[idx],
                marker="o",
                linewidth=1,
                edgecolor="black",
                zorder=10,
            )
            plt.annotate(category, xy=(xAverage * 1.01, yAverage * 1.01))

        if linearRegression:

            x = xValue.flatten()
            y = yValue.flatten()

            fit = numpy.polyfit(x, y, 1)
            fit_fn = numpy.poly1d(fit)

            x = numpy.sort(x)
            plt.plot(x, fit_fn(x), "--", color=colorCycle[idx])

    """ Attribute of the figure """
    # Add line
    if drawLine:
        FigureUtils.addLine(ax)
        FigureUtils.addLine(ax, 1, 0, "b")

    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)

    # Ticks
    if majorLocatorInterval:
        ax.xaxis.set_major_locator(majorLocatorInterval)
    if majorFormatter:
        ax.xaxis.set_major_formatter(majorFormatter)
    if minorLocatorInterval:
        ax.xaxis.set_minor_locator(minorLocatorInterval)
    if minorFormatter:
        ax.xaxis.set_minor_formatter(minorFormatter)

    return abscicaData_df.getGenericName(), ordinateData_df.getGenericName()
