from typing import Any, Callable, List, Optional

import cartopy.crs as ccrs
import geopandas
import matplotlib
import matplotlib.pyplot as plt
import numpy
from cartopy.io import shapereader
from data import Index
from data.Data import GenericData
from lib.pandas import PandasIndexes
from matplotlib.colors import Normalize
from pandas import DataFrame
from matplotlib.axes import Axes


class MidpointNormalize(Normalize):
    # https://matplotlib.org/3.1.1/tutorials/colors/colormapnorms.html

    def __init__(self, positions: List[float], values: List[float], clip=False):

        super().__init__(vmin=min(values), vmax=max(values), clip=clip)

        self.positions = positions
        self.values = values

    def __call__(self, value, clip=False):
        x, y = self.values, self.positions
        # if clip:
        #     # I'm ignoring masked values and all kinds of edge cases to make a
        #     # simple example...
        #     res =  numpy.ma.masked_array(numpy.interp(value, x, y))
        # else:
        #     res= numpy.ma.array(numpy.interp(value, x, y))
        result, is_scalar = self.process_value(value)
        res = numpy.ma.array(numpy.interp(value, x, y), mask=result.mask, copy=False)
        return res


def plotMap(
    data: List[GenericData],
    dataIndex: List[int],
    ax: Axes,
    artists: List[Any],
    normalizer_positions: Optional[List[float]] = None,
    normalizer_value: Optional[List[float]] = None,
    cmap: matplotlib.colors.Colormap = plt.cm.Reds,
    grouping: Callable = DataFrame.mean,
    **kwargs
):
    """Plot data in a map

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = grouping(tmp, level=[Index.country])

    # Insert your lists of countries and lag times here
    countries = PandasIndexes.getIndexeValues(data=tmp, level=Index.country)
    values = tmp.values.flatten()

    # Get countries border
    resolution = "50m"
    category = "cultural"
    name = "admin_0_countries"  # ne_50m_admin_0_countries
    shpfilename = shapereader.natural_earth(resolution, category, name)

    # read the shapefile using geopandas
    df = geopandas.read_file(shpfilename)

    # Create normalizer
    if normalizer_value is None:
        normalizer_value = [
            numpy.nanmin(tmp),
            numpy.nanmax(tmp),
        ]
    if normalizer_positions is None:
        normalizer_positions = [0, 1]
    normalizer = MidpointNormalize(positions=normalizer_positions, values=normalizer_value)

    for singleCountry, value in zip(countries, values):
        # Get country geometry
        c = df[df.WB_A2 == singleCountry]

        # Get intensity color
        rgba = cmap(normalizer(value))

        # Plot the country
        try:
            ax.add_geometries(
                c.geometry,
                crs=ccrs.PlateCarree(),
                facecolor=rgba,
                edgecolor="none",
                zorder=1,
            )
            # print("added country (methode 1) " + str(singleCountry))
        except Exception:
            # print(e)
            import shapely.wkt as wkt
            from shapely.geometry import MultiPolygon

            list_str_polygons = [str(c.geometry)]
            ax.add_geometries(
                MultiPolygon(map(wkt.loads, list_str_polygons)),
                crs=ccrs.PlateCarree(),
                facecolor=rgba,
                edgecolor="none",
                zorder=1,
            )
            # print("added country (methode 2) " + str(singleCountry))

    """ Attribute of the figure """
    # Add a scatter plot for the colorbar
    dummy_scat = ax.scatter(
        values,
        values,
        c=values,
        cmap=cmap,
        norm=normalizer,
        zorder=-1,
    )

    # Add colorbar legend
    ax.get_figure().colorbar(
        dummy_scat,
        label=data.name + " [" + data.unit.symbol + "]",
        orientation="vertical",
        shrink=0.5,
    )

    return None, None
