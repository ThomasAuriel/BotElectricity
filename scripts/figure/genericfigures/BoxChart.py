from typing import Any, List, Optional, Tuple

import numpy
from data.Data import GenericData
from lib.matplotlib import FigureColor
from lib.pandas import PandasIndexes, PandasOperation, PandasSelection
from matplotlib import pyplot


def boxchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    **kwargs
) -> Tuple[str, str]:
    """Plot box chart

    Parameter
    ---------
    data: List[GenericData]
        Data to plot. Use only the first data in the array
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex, categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    data = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=data.dataframe, level=categoryIndex)
    colorCycle = FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp = data.dataframe.copy()
    tmp = PandasOperation.sumOver(tmp, levels=[categoryIndex, abscisseIndex])

    """ Plot the data """
    xValues = []
    yValues = []
    for idx, category in enumerate(categories):
        tmp_category = PandasSelection.selectDataframe(tmp.copy(), categoryIndex, category)
        y = tmp_category.values.flatten()

        xValues.append(category)
        yValues.append(y)

    box = ax.boxplot(
        yValues,
        labels=xValues,
        patch_artist=True,
        whis=2,
        medianprops=dict(color="k"),
    )
    ## Colors
    for patch, color in zip(box["boxes"], colorCycle):
        patch.set_facecolor(color)

    """ Attribute of the figure """
    # Axis
    pyplot.xticks(
        ticks=numpy.arange(len(data.dataframe.index.unique(categoryIndex))) + 1,
        labels=PandasIndexes.getIndexeUniqueValues(data=tmp, level=categoryIndex),
    )

    # Limits
    if yLim:
        ylimBot = yLim[0]
        ylimTop = yLim[1]

        ax.set_ylim(ylimBot, ylimTop)
    # else:
    #     previousValues = [0, *list(itertools.chain(*yValues))]
    #     mininum = numpy.min(previousValues)
    #     maximum = numpy.max(previousValues)
    #     FigureUtils.setMinMax(ax=ax, minimum=mininum, maximum=maximum)

    return categoryIndex, None
