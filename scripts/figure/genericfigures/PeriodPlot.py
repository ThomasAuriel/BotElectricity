from typing import Any, List, Optional, Tuple

from calculation.Grouping import Grouping
from data import Index
from data.Data import GenericData
from figure.genericfigures import BarChart, LinearPlot
from lib.matplotlib import FigureColor
from lib.pandas import PandasIndexes, PandasOperation
from matplotlib import ticker


def barchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    # majorLocatorInterval=mdates.DayLocator(interval=1),
    # minorLocatorInterval=mdates.AutoDateLocator(),
    # majorFormatter=mdates.DateFormatter("%d-%m-%Y"),
    # minorFormatter=None,
    grouping: Grouping = Grouping.mean,
    **kwargs
) -> Tuple[str, str]:
    """Plot the bar chart of the data grouped by periode.

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex: str
        Used for the interval estimation. It shall be : 'hours', 'weekdays', 'months' or 'years'
    categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    tmp = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=tmp.dataframe, level=categoryIndex)
    FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp.dataframe = PandasIndexes.defineTimeInterval(tmp.dataframe, abscisseIndex, categoryIndex)

    if grouping == Grouping.mean:
        tmp.dataframe = PandasOperation.meanOver(
            tmp.dataframe, levels=["timeInterval", categoryIndex]
        )
        tmp.name += " - Mean value"
    else:
        tmp.dataframe = PandasOperation.sumOver(
            tmp.dataframe, levels=["timeInterval", categoryIndex]
        )
        tmp.name += " - Total value (sum)"

    tmp.dataframe = tmp.dataframe.sort_index(level=[categoryIndex, "timeInterval"])

    """ Plot the data """
    return BarChart.barchart(
        data=[tmp],
        dataIndex=[0],
        ax=ax,
        artists=artists,
        abscisseIndex="timeInterval",
        categoryIndex=categoryIndex,
        yLim=yLim,
        **kwargs
    )


def boxchart(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    # majorLocatorInterval=mdates.DayLocator(interval=1),
    # minorLocatorInterval=mdates.AutoDateLocator(),
    # majorFormatter=mdates.DateFormatter("%d-%m-%Y"),
    # minorFormatter=None,
    grouping: Grouping = Grouping.mean,
    **kwargs
) -> Tuple[str, str]:
    """Plot the box chart of the data grouped by periode.

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex: str
        Used for the interval estimation. It shall be : 'hours', 'weekdays', 'months' or 'years'
    categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    tmp = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=tmp.dataframe, level=categoryIndex)
    FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp.dataframe = PandasIndexes.defineTimeInterval(tmp.dataframe, abscisseIndex, categoryIndex)

    if grouping == Grouping.mean:
        tmp.dataframe = PandasOperation.meanOver(
            tmp.dataframe, levels=["timeInterval", categoryIndex]
        )
        tmp.name += " - Mean value"
    else:
        tmp.dataframe = PandasOperation.sumOver(
            tmp.dataframe, levels=["timeInterval", categoryIndex]
        )
        tmp.name += " - Total value (sum)"

    tmp.dataframe = tmp.dataframe.sort_index(level=[categoryIndex, "timeInterval"])

    """ Plot the data """
    return BarChart.barchart(
        data=[tmp],
        dataIndex=[0],
        ax=ax,
        artists=artists,
        abscisseIndex="timeInterval",
        categoryIndex=categoryIndex,
        yLim=yLim,
        **kwargs
    )


def linear(
    data: List[GenericData],
    dataIndex: List[int],
    ax,
    artists: List[Any],
    abscisseIndex: str,
    categoryIndex: str,
    yLim: Optional[Tuple[float, float]] = None,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
    cumulative: bool = True,
    **kwargs
) -> Tuple[str, str]:
    """Plot the box chart of the data grouped by periode.

    Parameter
    ---------
    data: List[GenericData]
        Data to plot
    dataIndex: List[int]
        index of the data to use from data parameters
    ax:
        Subplot where to draw
    artists: List[Any]
        All the artists of the figure. New artists will placed in this list
    abscisseIndex: str
        Used for the interval estimation. It shall be : 'hours', 'weekdays', 'months' or 'years'
    categoryIndex: str
        Index to use the get the abscisse and the categories to display
    yLim: Optional[Tuple[float, float]]
        Upper and lower bound of the plot

    Return
    ------
    xlabel: str
    ylabel: str
    """

    tmp = data[dataIndex[0]].copy()

    # Define color cycle
    categories = PandasIndexes.getIndexeUniqueValues(data=tmp.dataframe, level=categoryIndex)
    FigureColor.defineColorCycle(categories)

    """ Compute """
    tmp.dataframe = PandasIndexes.defineTimeInterval(tmp.dataframe, abscisseIndex, categoryIndex)
    if cumulative:
        tmp.dataframe = (
            tmp.dataframe.groupby(by=[Index.datetime, categoryIndex, "timeInterval"])
            .sum()
            .groupby(by=["timeInterval"])
            .cumsum()
        )
    else:
        tmp.dataframe = tmp.dataframe.groupby(by=[Index.datetime, "timeInterval"]).sum()

    """ Plot the data """
    return LinearPlot.linearplot(
        data=[tmp],
        dataIndex=[0],
        ax=ax,
        artists=artists,
        abscisseIndex=None,
        categoryIndex="timeInterval",
        yLim=yLim,
        majorLocatorInterval=majorLocatorInterval,
        minorLocatorInterval=minorLocatorInterval,
        majorFormatter=majorFormatter,
        minorFormatter=minorFormatter,
        **kwargs
    )
