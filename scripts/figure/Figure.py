from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import matplotlib.dates as mdates
from data.Data import GenericData
from lib import FileUtils, StringUtils
from lib.matplotlib import FigureUtils
from matplotlib import pyplot


class Subplot:
    """
    Define subplot in a figure
    """

    def __init__(
        self,
        functionPlot: Callable,
        dataIndex: Union[List[int], int] = [0],
        plotArgs: Dict[str, Any] = {},
        title: Optional[str] = None,
        interval: str = "time interval: 1hr",
        nrows: int = 1,
        ncols: int = 1,
        index: int = 1,
        type: FigureUtils.Projection = FigureUtils.Projection.projection_2D,
        abscisseIndex: str = None,
        categoryIndex: str = None,
        yLim: Optional[Tuple[float, float]] = None,
        xlabel: Optional[str] = None,
        ylabel: Optional[str] = None,
        majorLocatorInterval=mdates.DayLocator(interval=1),
        minorLocatorInterval=mdates.AutoDateLocator(),
        majorFormatter=mdates.DateFormatter("%d-%m-%Y"),
        minorFormatter=None,
        showGrid: bool = True,
        subplot_kwargs: Dict[str, str] = {"aspect": "auto"},
        **kwargs,
    ) -> None:
        """
        Parameters
        ----------

        xLim, yLim: Tuple(float, float)
            x and y limits
            x[0] and y[0] must be the lower limit.
            x[1] and y[1] must be the higher limit.
        """

        self.functionPlot = functionPlot
        self.dataIndex = dataIndex
        self.plotArgs = plotArgs

        self.title: Optional[str] = title
        self.interval = interval

        self.nrows: int = nrows
        self.ncols: int = ncols
        self.index: int = index
        self.type: FigureUtils.Projection = type
        self.subplot_kwargs: Dict[str, str] = subplot_kwargs

        self.abscisseIndex = abscisseIndex
        self.categoryIndex = categoryIndex

        self.yLim = yLim

        self.xlabel = xlabel
        self.ylabel = ylabel

        self.majorLocatorInterval = majorLocatorInterval
        self.minorLocatorInterval = minorLocatorInterval
        self.majorFormatter = majorFormatter
        self.minorFormatter = minorFormatter

        self.showGrid = showGrid

        self.kwargs = kwargs

    def plot(
        self,
        fig,
        artists,
        mapAx: Optional[Dict],
        data: List[GenericData],
        verbose: bool = True,
        **kwargs,
    ):

        """Create the ax subplot"""
        ax, artists, mapAx = FigureUtils.addSubplot(
            fig=fig,
            artists=artists,
            type=self.type,
            nrows=self.nrows,
            ncols=self.ncols,
            index=self.index,
            subplot_kwargs=self.subplot_kwargs,
            righttext=self.interval,
            showGrid=self.showGrid,
            mapAx=mapAx,
            **self.kwargs,
            **kwargs,
        )

        if len(data) == 0:
            print("No data")
            return

        if verbose:
            print("Start to plot")

        xlabel, ylabel = self.functionPlot(
            data=data,
            dataIndex=self.dataIndex,
            ax=ax,
            artists=artists,
            abscisseIndex=self.abscisseIndex,
            categoryIndex=self.categoryIndex,
            yLim=self.yLim,
            majorLocatorInterval=self.majorLocatorInterval,
            minorLocatorInterval=self.minorLocatorInterval,
            majorFormatter=self.majorFormatter,
            minorFormatter=self.minorFormatter,
            **self.plotArgs,
        )

        """Decoration"""

        # Title
        if self.title:

            # Title
            start = data[0].start.strftime("%Y%m%dT%H%M%S")
            end = data[0].end.strftime("%Y%m%dT%H%M%S")

            title = self.title
            title = StringUtils.format_map(title, {"start": start, "end": end})
            ax.title.set_text(title)
        else:
            ax.title.set_text("")

        # Axis
        ax.set_xlabel(self.xlabel if self.xlabel else xlabel if xlabel else self.abscisseIndex)
        ax.set_ylabel(
            self.ylabel
            if self.ylabel
            else ylabel
            if ylabel
            else data[self.dataIndex[0]].getGenericName()
        )

        pyplot.xticks(rotation=30, ha="right")

        """ Add Legend """
        lgd = FigureUtils.addLegend(ax)
        artists.append(lgd)

        if verbose:
            print("Finished to plot")

        return mapAx


class Figure:
    """
    Define a figure
    """

    def __init__(
        self,
        filename: str,
        data: List[GenericData],
        title: str = "",
        author: Optional[str] = "@BotElectricity v0.8",
        source: Optional[str] = FigureUtils.defaultSource,
        size: Tuple[int, int] = (24, 16),
        dpi=200,
        verbose: bool = True,
    ) -> None:
        """
        Parameters
        ----------
        filename:
            path to store the plot
        data: Dataset
            Data to use for the plot
        title: str
            Title of the whole figure
        size: Tuple[int, int]
            set the size of the plot
        """

        self.data: GenericData = data

        self.filename: str = filename
        self.title: str = title
        self.author: Optional[str] = author
        self.source: Optional[str] = source
        self.size: Tuple[int, int] = size
        self.dpi: int = dpi

        self.subplots: List[Subplot] = []

        self.mapAx = {}

    def addSubplots(self, subplots: List[Subplot]):

        self.subplots.extend(subplots)

    def getFilename(self):

        try:
            start = self.data[0].start.strftime("%Y%m%dT%H%M%S")
            end = self.data[0].end.strftime("%Y%m%dT%H%M%S")

            formattedFilename = StringUtils.format_map(self.filename, {"start": start, "end": end})
        except Exception:
            formattedFilename = self.filename

        return formattedFilename

    def existingPlot(
        self,
        verbose: bool = True,
    ):

        plotExist = FileUtils.exists(self.getFilename())
        if verbose:
            if plotExist:
                print(self.getFilename() + " exists")
            else:
                print(self.getFilename() + " does not exists")

        return plotExist

    def plot(self, plotIfExist: bool = False, verbose: bool = False, **kwargs):

        if self.existingPlot() and not plotIfExist:
            return

        try:

            # Draw subplots

            """Create the plot figure"""
            fig, artists = FigureUtils.getFigure(
                self.size, self.dpi, author=self.author, source=self.source, **kwargs
            )
            for subPlot in self.subplots:
                self.mapAx = subPlot.plot(
                    fig=fig, artists=artists, data=self.data, mapAx=self.mapAx, **kwargs
                )

            # Title
            start = self.data[0].start.strftime("%H:%M %d/%m/%y")
            end = self.data[0].end.strftime("%H:%M %d/%m/%y")

            title = self.title
            title = StringUtils.format_map(title, {"start": start, "end": end})
            fig.suptitle(title)

            # Save the plot
            if verbose:
                print("Save plot at :" + self.getFilename())

            # fig.canvas.draw()
            pyplot.tight_layout()

            # Create the parent folder if does not exist
            FileUtils.createFolder(FileUtils.getParent(self.getFilename()))
            # Sve the plot
            pyplot.savefig(self.getFilename())

        except Exception as e:
            print("Fail to plot " + self.getFilename())
            print(e)
        finally:
            pyplot.close()
