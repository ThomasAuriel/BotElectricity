from datetime import datetime

import pandas
import pandas as pandas

from data import Country, DataPower
from lib.OutputUtils import Console

start = pandas.Timestamp(datetime(2019, 1, 1, 0), tz="Europe/Brussels")
end = pandas.Timestamp(datetime(2019, 1, 2, 0), tz="Europe/Brussels")

listCountries = Country.countries.values()

"""
Get data
"""
Console.print("Start to collect data")
# Select data
generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountries, listCategory=None
)
Console.print("End of collecting data")


Console.print("Start to format data")
df = generation
# df = df.stack().unstack(level=[0, 2])
Console.print("End of formating data")

Console.print("Write data")
df.dataframe.to_csv("./test.csv")

Console.print("End")
