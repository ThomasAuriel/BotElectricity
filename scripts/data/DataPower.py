from __future__ import annotations

from datetime import datetime
from typing import List, Optional, Union

import pandas as pandas
from pandas import DataFrame

from data import Country, Index, Units
from data.Data import GenericData


class Generation(GenericData):
    name = "electricity generation"
    dataframe = None
    unit = Units.MWh

    sqlTable = "generation"
    downloadParameters = {"nett": True}
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]


class Load(GenericData):
    name = "grid load"
    dataframe = None
    unit = Units.MWh

    sqlTable = "load"
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]


class Import(GenericData):
    name = "interconnexion import"
    dataframe = None
    unit = Units.MWh

    sqlTable = "import"
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]

    def __init__(
        self,
        dataframe: Union[DataFrame, GenericData, None] = None,
        name: Optional[str] = "interconnexion import",
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: Optional[List[Country.Country]] = None,
        listCategory: Optional[List[Country.Country]] = None,
    ) -> None:

        if name:
            self.name

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = [c.iso for c in listCategory]

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        elif self.sqlTable is not None:
            self.getFromDatabase()


class Export(GenericData):
    name = "interconnexion export"
    dataframe = None
    unit = Units.MWh

    sqlTable = "export"
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]

    def __init__(
        self,
        dataframe: Union[DataFrame, GenericData, None] = None,
        name: Optional[str] = "interconnexion export",
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: Optional[List[Country.Country]] = None,
        listCategory: Optional[List[Country.Country]] = None,
    ) -> None:

        if name:
            self.name

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = [c.iso for c in listCategory]

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        elif self.sqlTable is not None:
            self.getFromDatabase()


class Balance(GenericData):
    name = "Balance"
    dataframe = None
    unit = Units.MWh

    sqlTable = "balance"
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]

    def __init__(
        self,
        dataframe: Union[DataFrame, GenericData, None] = None,
        name: Optional[str] = "Balance",
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: Optional[List[Country.Country]] = None,
        listCategory: Optional[List[Country.Country]] = Country.getAllCountries(),
        dfExport: Optional[GenericData] = None,
        dfImport: Optional[GenericData] = None,
    ) -> None:

        if name:
            self.name

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = listCategory

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        else:
            if not dfExport:
                dfExport = Export(
                    start=start, end=end, listCountry=listCountry, listCategory=self.listCategory
                )
            if not dfImport:
                dfImport = Import(
                    start=start, end=end, listCountry=listCountry, listCategory=self.listCategory
                )

            self.dataframe = dfImport.dataframe - dfExport.dataframe
            self.dataframe = self.dataframe.fillna(0)


class DayAheadPrices(GenericData):
    name = "day Ahead Prices"
    dataframe = None
    unit = Units.euros

    sqlTable = "dayaheadprices"
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]


class GenerationBalance(GenericData):
    name = "Electricity generation and balance"
    dataframe = None
    unit = Units.MWh

    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]

    def __init__(
        self,
        dataframe: Union[DataFrame, GenericData, None] = None,
        generation: Optional[GenericData] = None,
        balance: Optional[GenericData] = None,
        sumBalance: Optional[List[str]] = [Index.datetime, Index.country],
        name: Optional[str] = "Electricity generation and balance",
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: Optional[List[Country.Country]] = None,
        listCategory: Optional[List[str]] = None,
        **kwargs,
    ) -> None:

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = listCategory

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        else:
            if generation is None:
                generation = Generation(
                    start=start, end=end, listCountry=listCountry, listCategory=listCategory
                )
            generation = generation.copy()
            generation = generation.convert(self.unit)
            generation_df = generation.dataframe

            if balance is None:
                balance = Balance(start=start, end=end, listCountry=listCountry)
            balance = balance.copy()
            balance = balance.convert(self.unit)
            balance_df = balance.dataframe

            if sumBalance:
                balance_df = balance_df.groupby(level=[Index.datetime, Index.country]).sum()
                balance_df = pandas.concat({"Balance": balance_df}, names=[Index.category])

            self.dataframe = pandas.concat([generation_df.reset_index(), balance_df.reset_index()])
            self.dataframe = self.dataframe.set_index(
                [Index.country, Index.category, Index.datetime]
            )


class GenerationBalanceLoad(GenericData):
    name = "Electricity generation, balance and load"
    dataframe = None
    unit = Units.MWh

    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]

    def __init__(
        self,
        dataframe: Union[DataFrame, GenericData, None] = None,
        generation: Optional[GenericData] = None,
        balance: Optional[GenericData] = None,
        load: Optional[GenericData] = None,
        sumBalance: Optional[List[str]] = [Index.datetime, Index.country],
        name: Optional[str] = "Electricity generation, balance and load",
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: Optional[List[Country.Country]] = None,
        listCategory: Optional[List[str]] = None,
    ) -> None:

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = listCategory

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        else:
            if generation is None:
                generation = Generation(
                    start=start, end=end, listCountry=listCountry, listCategory=listCategory
                )
            generation = generation.convert(self.unit)
            generation_df = generation.dataframe

            if balance is None:
                balance = Balance(
                    start=start, end=end, listCountry=listCountry, listCategory=listCategory
                )
            balance = balance.copy()
            balance = balance.convert(self.unit)
            balance_df = balance.dataframe

            if load is None:
                load = Load(
                    start=start, end=end, listCountry=listCountry, listCategory=listCategory
                )
            load = load.copy()
            load = load.convert(self.unit)
            load_df = load.dataframe

            if sumBalance:
                balance_df = balance_df.groupby(level=[Index.datetime, Index.country]).sum()
                balance_df = pandas.concat([balance_df], keys=["Balance"], names=[Index.category])

            self.dataframe = pandas.concat(
                [generation_df.reset_index(), balance_df.reset_index(), load_df.reset_index()]
            ).set_index([Index.country, Index.category, Index.datetime])
