from pandas import DataFrame

from data import Units
from lib.pandas import PandasToCSV


class CO2IntensityCategories(object):

    filepath: str
    unit: Units.Unit
    data: DataFrame

    def __init__(self) -> None:

        df = PandasToCSV.read(CSVFile=self.filepath)
        df = df.drop(columns="Comments")
        unit = Units.getUnitBySymbol(df.columns[0])
        df = df.rename({unit.symbol: "data"}, axis=1)

        self.data = df
        self.unit = unit

    def reindexToMatch(self, df: DataFrame) -> DataFrame:
        # Reindex
        return df * 0 + self.data


class CO2Intensity_IPCC(CO2IntensityCategories):
    filepath = "./input/CO2Intensity/CO2Intensity_IPCC.csv"


class CO2Intensity_ENTSOE(CO2IntensityCategories):
    filepath = "./input/CO2Intensity/CO2Intensity_ENTSOE.csv"


class CO2Intensity_MapElectricity(CO2IntensityCategories):
    filepath = "./input/CO2Intensity/CO2Intensity_MapElectricity.csv"


class CO2Intensity_ADEME_IPCCGrouping(CO2IntensityCategories):
    filepath = "./input/CO2Intensity/CO2Intensity_ADEME_IPCCGrouping.csv"


class CO2Intensity_ADEME_ENTSOEGrouping(CO2IntensityCategories):
    filepath = "./input/CO2Intensity/CO2Intensity_ADEME_ENTSOEGrouping.csv"


## https://ars.els-cdn.com/content/image/1-s2.0-S2211467X19300549-mmc1.pdf
class CO2Intensity_ECOInvent_ENTSOEGrouping(CO2IntensityCategories):
    filepath = "./input/CO2Intensity/CO2Intensity_ECOInvent_ENTSOEGrouping.csv"
