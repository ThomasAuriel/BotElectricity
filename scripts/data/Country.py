from typing import List, Tuple


class Country(object):
    """Country object group countries informations as:
    name : str
        Country name
    iso : str
        country iso code
    pos : Tuple[int, int]
        Country position (used to plot graphs)
    posCapital : Tuple[float, float]
        GPS country capital position (used to plot realistic map)
    isCountry : bool
        If the area is a country
    isBiddingZone : bool
        If the area is a bidding zone
    """

    def __init__(
        self,
        name: str,
        iso: str,
        pos: Tuple[int, int],
        posCapital: Tuple[float, float],
        isCountry: bool,
        isBiddingZone: bool,
    ):
        """
        Constructor
        """

        self.name = name
        self.iso = iso
        self.pos = pos
        self.posCapital = posCapital
        self.isCountry = isCountry
        self.isBiddingZone = isBiddingZone

    def getGenericName(self):
        """
        Get the generic name of the country composed of `{name} [{iso}]`.

        Return
        ------
        The generic name.
        """

        return self.name + " [" + self.iso + "]"


def getAllCountries(biddingZone=False) -> List[Country]:
    """
    Returns all the country or biddingZone in a list.

    Parameter
    ---------
    biddingZone: bool
        If true, only bidding zone are returns.
        If false, only countries are returns.

    Return
    ------
    Return a Country list
    """

    if biddingZone:
        return [country for country in countries.values() if country.isBiddingZone]
    else:
        return [country for country in countries.values() if country.isCountry]


def getAllCountriesISO(countryList: List[Country] = None) -> List[str]:
    """
    Returns all the country iso in a list.

    Parameter
    ---------
    countryList: List[Country]
        If provided, this list is used to get all the ISO instead of the whole list

    Return
    ------
    Return a Country iso list
    """

    if countryList:
        return [country.iso for country in countryList]
    else:
        return [country.iso for country in countries.values()]


def getAllCountriesName(countryList: List[Country] = None):
    """
    Returns all the country name in a list.

    Parameter
    ---------
    countryList: List[Country]
        If provided, this list is used to get all the name instead of the whole list

    Return
    ------
    Return a Country name list
    """

    if countryList:
        return {country.iso: country.name for country in countryList}
    else:
        return {country.iso: country.name for country in countries.values()}


def getAllCountriesPos(countryList: List[Country] = None):
    """
    Returns all the country position in a list.

    Parameter
    ---------
    countryList: List[Country]
        If provided, this list is used to get all the name instead of the whole list

    Return
    ------
    Return a Country name list
    """

    if countryList:
        return {country.iso: country.pos for country in countryList}
    else:
        return {country.iso: country.pos for country in countries.values()}


def getAllCapitalPos(countryList: List[Country] = None):
    """
    Returns all the country capital position in a list.

    Parameter
    ---------
    countryList: List[Country]
        If provided, this list is used to get all the name instead of the whole list

    Return
    ------
    Return a Country name list
    """

    if countryList:
        return {country.iso: country.posCapital for country in countryList}
    else:
        return {country.iso: country.posCapital for country in countries.values()}


AL = Country(
    iso="AL",
    pos=(40, 10),
    posCapital=(19.8188900, 41.3275000),
    name="Albania",
    isCountry=True,
    isBiddingZone=True,
)
AT = Country(
    iso="AT",
    pos=(40, 60),
    posCapital=(16.3738189, 48.2081743),
    name="Austria",
    isCountry=True,
    isBiddingZone=True,
)
BA = Country(
    iso="BA",
    pos=(40, 30),
    posCapital=(18.4130763, 43.8562586),
    name="Bosnia and Herzegovina",
    isCountry=True,
    isBiddingZone=True,
)
BE = Country(
    iso="BE",
    pos=(30, 80),
    posCapital=(04.3517103, 50.8503396),
    name="Belgium",
    isCountry=True,
    isBiddingZone=True,
)
BG = Country(
    iso="BG",
    pos=(60, 10),
    posCapital=(23.3218675, 42.6977082),
    name="Bulgaria",
    isCountry=True,
    isBiddingZone=True,
)
BY = Country(
    iso="BY",
    pos=(70, 60),
    posCapital=(27.5615244, 53.9045398),
    name="Belarus",
    isCountry=True,
    isBiddingZone=False,
)
CH = Country(
    iso="CH",
    pos=(30, 60),
    posCapital=(07.4446085, 46.9479222),
    name="Switzerland",
    isCountry=True,
    isBiddingZone=True,
)
CZ = Country(
    iso="CZ",
    pos=(50, 60),
    posCapital=(14.4378005, 50.0755381),
    name="Czechia",
    isCountry=True,
    isBiddingZone=True,
)
DE = Country(
    iso="DE",
    pos=(40, 70),
    posCapital=(13.4049540, 52.5200070),
    name="Germany",
    isCountry=True,
    isBiddingZone=False,
)
DK = Country(
    iso="DK",
    pos=(40, 90),
    posCapital=(12.5683371, 55.6760968),
    name="Denmark",
    isCountry=True,
    isBiddingZone=False,
)
EE = Country(
    iso="EE",
    pos=(70, 90),
    posCapital=(24.7535746, 59.4369608),
    name="Estonia",
    isCountry=True,
    isBiddingZone=True,
)
ES = Country(
    iso="ES",
    pos=(20, 40),
    posCapital=(-3.7037902, 40.4167754),
    name="Spain",
    isCountry=True,
    isBiddingZone=True,
)
FI = Country(
    iso="FI",
    pos=(70, 100),
    posCapital=(24.9410248, 60.1733244),
    name="Finland",
    isCountry=True,
    isBiddingZone=True,
)
FR = Country(
    iso="FR",
    pos=(20, 60),
    posCapital=(02.3522219, 48.8566140),
    name="France",
    isCountry=True,
    isBiddingZone=True,
)
GE = Country(
    iso="GE",
    pos=(21, 81),
    posCapital=(42.3198470, 43.3680550),
    name="Georgia",
    isCountry=True,
    isBiddingZone=True,
)
GB = Country(
    iso="GB",
    pos=(20, 90),
    posCapital=(-0.1277583, 51.5073509),
    name="United Kingdom",
    isCountry=True,
    isBiddingZone=True,
)
UK = Country(
    iso="UK",
    pos=(20, 90),
    posCapital=(-0.1277583, 51.5073509),
    name="United Kingdom",
    isCountry=False,
    isBiddingZone=True,
)
GR = Country(
    iso="GR",
    pos=(40, +0),
    posCapital=(23.7293599, 37.9839170),
    name="Greece",
    isCountry=True,
    isBiddingZone=True,
)
HR = Country(
    iso="HR",
    pos=(40, 40),
    posCapital=(15.9819190, 45.8150108),
    name="Croatia",
    isCountry=True,
    isBiddingZone=True,
)
HU = Country(
    iso="HU",
    pos=(50, 50),
    posCapital=(19.0402350, 47.4979120),
    name="Hungary",
    isCountry=True,
    isBiddingZone=True,
)
IE = Country(
    iso="IE",
    pos=(10, 90),
    posCapital=(-6.2603100, 53.3498050),
    name="Ireland",
    isCountry=True,
    isBiddingZone=True,
)
IT = Country(
    iso="IT",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italy",
    isCountry=True,
    isBiddingZone=False,
)
LT = Country(
    iso="LT",
    pos=(70, 70),
    posCapital=(25.2796514, 54.6871555),
    name="Lithuania",
    isCountry=True,
    isBiddingZone=True,
)
LU = Country(
    iso="LU",
    pos=(30, 70),
    posCapital=(06.1295830, 49.8152730),
    name="Luxembourg",
    isCountry=True,
    isBiddingZone=False,
)
LV = Country(
    iso="LV",
    pos=(70, 80),
    posCapital=(24.1051864, 56.9496487),
    name="Latvia",
    isCountry=True,
    isBiddingZone=True,
)
# "MD = Country(iso="", pos=(0, 50), posCapital=(), name="" , biddingZone="MD")
ME = Country(
    iso="ME",
    pos=(40, 20),
    posCapital=(19.2628920, 42.4412860),
    name="Montenegro",
    isCountry=True,
    isBiddingZone=False,
)
MK = Country(
    iso="MK",
    pos=(50, 20),
    posCapital=(21.4279956, 41.9973462),
    name="North Macedonia",
    isCountry=True,
    isBiddingZone=True,
)
MT = Country(
    iso="MT",
    pos=(30, 30),
    posCapital=(14.5145530, 35.8989080),
    name="Malta",
    isCountry=True,
    isBiddingZone=False,
)
NL = Country(
    iso="NL",
    pos=(30, 90),
    posCapital=(04.3007000, 52.0704980),
    name="Netherlands",
    isCountry=True,
    isBiddingZone=True,
)
NO = Country(
    iso="NO",
    pos=(50, 100),
    posCapital=(10.7522454, 59.9138688),
    name="Norway",
    isCountry=True,
    isBiddingZone=False,
)
PL = Country(
    iso="PL",
    pos=(50, 70),
    posCapital=(21.0117800, 52.2297700),
    name="Poland",
    isCountry=True,
    isBiddingZone=True,
)
PT = Country(
    iso="PT",
    pos=(10, 40),
    posCapital=(-9.1393366, 38.7222524),
    name="Portugal",
    isCountry=True,
    isBiddingZone=True,
)
RO = Country(
    iso="RO",
    pos=(60, 40),
    posCapital=(26.1025384, 44.4267674),
    name="Romania",
    isCountry=True,
    isBiddingZone=True,
)
RS = Country(
    iso="RS",
    pos=(50, 30),
    posCapital=(20.4489216, 44.7865680),
    name="Serbia",
    isCountry=True,
    isBiddingZone=True,
)
RU = Country(
    iso="RU",
    pos=(75, 70),
    posCapital=(37.6173000, 55.755826),
    name="Russia",
    isCountry=True,
    isBiddingZone=False,
)
RU_KGD = Country(
    iso="RU_KGD",
    pos=(60, 65),
    posCapital=(20.4522144, 54.7104264),
    name="Kaliningrad",
    isCountry=True,
    isBiddingZone=False,
)
SE = Country(
    iso="SE",
    pos=(60, 90),
    posCapital=(18.0685808, 59.3293235),
    name="Sweden",
    isCountry=True,
    isBiddingZone=False,
)
SI = Country(
    iso="SI",
    pos=(40, 50),
    posCapital=(14.5057515, 46.0569465),
    name="Slovenia",
    isCountry=True,
    isBiddingZone=True,
)
SK = Country(
    iso="SK",
    pos=(60, 60),
    posCapital=(17.1071373, 48.1458923),
    name="Slovakia",
    isCountry=True,
    isBiddingZone=True,
)
TR = Country(
    iso="TR",
    pos=(60, +0),
    posCapital=(32.8541100, 39.9207700),
    name="Turkey",
    isCountry=True,
    isBiddingZone=False,
)
UA = Country(
    iso="UA",
    pos=(70, 50),
    posCapital=(30.5234000, 50.4501000),
    name="Ukraine",
    isCountry=True,
    isBiddingZone=True,
)
XK = Country(
    iso="XK",
    pos=(50, 10),
    posCapital=(21.274842, 42.641633),
    name="Kosovo",
    isCountry=True,
    isBiddingZone=True,
)


# Bidding zone
DE_AT_LU = Country(
    iso="DE_AT_LU",
    pos=(40, 70),
    posCapital=(13.4049540, 52.5200070),
    name="Germany - Austria - Luxemburg",
    isCountry=False,
    isBiddingZone=True,
)
DE_LU = Country(
    iso="DE_LU",
    pos=(40, 70),
    posCapital=(13.4049540, 52.5200070),
    name="Germany - Luxemburg",
    isCountry=False,
    isBiddingZone=True,
)
CZ_DE_SK = Country(
    iso="CZ_DE_SK",
    pos=(40, 70),
    posCapital=(13.4049540, 52.5200070),
    name="Czechia - Germany - Slovakia",
    isCountry=False,
    isBiddingZone=True,
)
DK_1 = Country(
    iso="DK1",
    pos=(40, 80),
    posCapital=(12.5683371, 55.6760968),
    name="Danemark 1",
    isCountry=False,
    isBiddingZone=True,
)
DK_2 = Country(
    iso="DK2",
    pos=(40, 80),
    posCapital=(12.5683371, 55.6760968),
    name="Danemark 2",
    isCountry=False,
    isBiddingZone=True,
)

IT_Brindisi = Country(
    iso="IT_Brindisi",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Brindisi",
    isCountry=False,
    isBiddingZone=True,
)
IT_Calabria = Country(
    iso="IT_Calabria",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Calabria",
    isCountry=False,
    isBiddingZone=True,
)
IT_Centre_North = Country(
    iso="IT_Centre_North",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Centre North",
    isCountry=False,
    isBiddingZone=True,
)

IT_Centre_South = Country(
    iso="IT_Centre_South",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Centre South",
    isCountry=False,
    isBiddingZone=True,
)
IT_Foggia = Country(
    iso="IT_Foggia",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Foggia",
    isCountry=False,
    isBiddingZone=True,
)
IT_GR = Country(
    iso="IT_GR",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Greece",
    isCountry=False,
    isBiddingZone=True,
)
IT_Malta = Country(
    iso="IT_Malta",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Malta",
    isCountry=False,
    isBiddingZone=True,
)
IT_North = Country(
    iso="IT_North",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - North",
    isCountry=False,
    isBiddingZone=True,
)
IT_North_AT = Country(
    iso="IT_North_AT",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - North - Austria",
    isCountry=False,
    isBiddingZone=True,
)
IT_North_CH = Country(
    iso="IT_North_CH",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - North - Switzerland",
    isCountry=False,
    isBiddingZone=True,
)
IT_North_FR = Country(
    iso="IT_North_FR",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - North - French",
    isCountry=False,
    isBiddingZone=True,
)
IT_North_SI = Country(
    iso="IT_North_SI",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - North - Slovenia",
    isCountry=False,
    isBiddingZone=True,
)

IT_Priolo = Country(
    iso="IT_Priolo",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Priolo",
    isCountry=False,
    isBiddingZone=True,
)
IT_Rossano = Country(
    iso="IT_Rossano",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Rossano",
    isCountry=False,
    isBiddingZone=True,
)
IT_SACOAC = Country(
    iso="IT_SACOAC",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - SACOAC",
    isCountry=False,
    isBiddingZone=True,
)
IT_SACODC = Country(
    iso="IT_SACODC",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - SACODC",
    isCountry=False,
    isBiddingZone=True,
)
IT_Sardinia = Country(
    iso="IT_Sardinia",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Sardinia",
    isCountry=False,
    isBiddingZone=True,
)
IT_Sicily = Country(
    iso="IT_Sicily",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - Sicily",
    isCountry=False,
    isBiddingZone=True,
)
IT_South = Country(
    iso="IT_South",
    pos=(30, 40),
    posCapital=(12.4963655, 41.9027835),
    name="Italia - South",
    isCountry=False,
    isBiddingZone=True,
)

NO_1 = Country(
    iso="NO1",
    pos=(40, 90),
    posCapital=(10.7522454, 59.9138688),
    name="Norway 1",
    isCountry=False,
    isBiddingZone=True,
)
NO_2 = Country(
    iso="NO2",
    pos=(40, 90),
    posCapital=(10.7522454, 59.9138688),
    name="Norway 2",
    isCountry=False,
    isBiddingZone=True,
)
NO_3 = Country(
    iso="NO3",
    pos=(40, 90),
    posCapital=(10.7522454, 59.9138688),
    name="Norway 3",
    isCountry=False,
    isBiddingZone=True,
)
NO_4 = Country(
    iso="NO4",
    pos=(40, 90),
    posCapital=(10.7522454, 59.9138688),
    name="Norway 4",
    isCountry=False,
    isBiddingZone=True,
)
NO_5 = Country(
    iso="NO5",
    pos=(40, 90),
    posCapital=(10.7522454, 59.9138688),
    name="Norway 5",
    isCountry=False,
    isBiddingZone=True,
)

SE_1 = Country(
    iso="SE1",
    pos=(50, 80),
    posCapital=(18.0685808, 59.3293235),
    name="Sweden 1",
    isCountry=False,
    isBiddingZone=True,
)
SE_2 = Country(
    iso="SE2",
    pos=(50, 80),
    posCapital=(18.0685808, 59.3293235),
    name="Sweden 2",
    isCountry=False,
    isBiddingZone=True,
)
SE_3 = Country(
    iso="SE3",
    pos=(50, 80),
    posCapital=(18.0685808, 59.3293235),
    name="Sweden 3",
    isCountry=False,
    isBiddingZone=True,
)
SE_4 = Country(
    iso="SE4",
    pos=(50, 80),
    posCapital=(18.0685808, 59.3293235),
    name="Sweden 4",
    isCountry=False,
    isBiddingZone=True,
)


countries = {
    AL.iso: AL,
    AT.iso: AT,
    BA.iso: BA,
    BE.iso: BE,
    BG.iso: BG,
    BY.iso: BY,
    CH.iso: CH,
    CZ.iso: CZ,
    DE.iso: DE,
    DK.iso: DK,
    EE.iso: EE,
    ES.iso: ES,
    FI.iso: FI,
    FR.iso: FR,
    GB.iso: GB,
    UK.iso: UK,
    GR.iso: GR,
    HR.iso: HR,
    HU.iso: HU,
    IE.iso: IE,
    IT.iso: IT,
    LT.iso: LT,
    LU.iso: LU,
    LV.iso: LV,
    # MD.iso: MD,
    ME.iso: ME,
    MK.iso: MK,
    MT.iso: MT,
    NL.iso: NL,
    NO.iso: NO,
    PL.iso: PL,
    PT.iso: PT,
    RO.iso: RO,
    RS.iso: RS,
    RU.iso: RU,
    RU_KGD.iso: RU_KGD,
    SE.iso: SE,
    SI.iso: SI,
    SK.iso: SK,
    TR.iso: TR,
    UA.iso: UA,
    XK.iso: XK,
    DE_AT_LU.iso: DE_AT_LU,
    DE_LU.iso: DE_LU,
    CZ_DE_SK.iso: CZ_DE_SK,
    DK_1.iso: DK_1,
    DK_2.iso: DK_2,
    IT_Brindisi.iso: IT_Brindisi,
    IT_Calabria.iso: IT_Calabria,
    IT_Centre_North.iso: IT_Centre_North,
    IT_Centre_South.iso: IT_Centre_South,
    IT_Foggia.iso: IT_Foggia,
    IT_GR.iso: IT_GR,
    IT_Malta.iso: IT_Malta,
    IT_North.iso: IT_North,
    IT_North_AT.iso: IT_North_AT,
    IT_North_CH.iso: IT_North_CH,
    IT_North_FR.iso: IT_North_FR,
    IT_North_SI.iso: IT_North_SI,
    IT_Priolo.iso: IT_Priolo,
    IT_Rossano.iso: IT_Rossano,
    IT_SACOAC.iso: IT_SACOAC,
    IT_SACODC.iso: IT_SACODC,
    IT_Sardinia.iso: IT_Sardinia,
    IT_Sicily.iso: IT_Sicily,
    IT_South.iso: IT_South,
    NO_1.iso: NO_1,
    NO_2.iso: NO_2,
    NO_3.iso: NO_3,
    NO_4.iso: NO_4,
    NO_5.iso: NO_5,
    SE_1.iso: SE_1,
    SE_2.iso: SE_2,
    SE_3.iso: SE_3,
    SE_4.iso: SE_4,
}
