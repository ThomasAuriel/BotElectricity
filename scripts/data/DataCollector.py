from datetime import datetime
from typing import List, Optional, Union

from lib.pandas import PandasSelection, PandasStorage

from data import Country
from data.Data import GenericData
from data.Units import Unit


def getData(
    data: GenericData,
    start: Optional[datetime] = None,
    end: Optional[datetime] = None,
    countries: Union[List[Country.Country], None] = None,
    categories: Union[List[str], None] = None,
    unit: Optional[Unit] = None,
    verbose: bool = True,
) -> GenericData:

    ## Collect data
    if data.dataframe is None:
        if verbose:
            print("Collect data")
        data = PandasStorage.get(data=data, start=start, end=end, verbose=verbose)

    if start and end:
        if verbose:
            print("Filter data using start and end date")
        data = PandasSelection.selectBetweenDatetime(
            data=data, datetimeStart=start, datetimeEnd=end
        )

    # Select countries if required
    if countries:
        if verbose:
            print("Filter data using country list")
        data = PandasSelection.selectCountry(data=data, country=[c.iso for c in countries])

    # Select categories
    if categories is not None:
        if verbose:
            print("Filter data using category list")
        data = PandasSelection.selectCategory(data=data, country=categories)

    if unit:
        data.convert(unit=unit)

    return data
