from __future__ import annotations

import copy
from datetime import datetime
from typing import Any, Dict, List, Optional, Union

from lib.pandas import PandasStorage
from pandas import DataFrame

from data import Country, Units


class Downloader(object):
    name: str = "Generic Downloader"
    unit: Optional[Units.Unit]

    def download(
        self,
        start: datetime,
        end: datetime,
        listCountry: List[Country.Country],
        indexAxisName: List[str],
    ):
        raise Exception("Not implemented yet")


class GenericData(object):
    """Data object groups:

    Attributes
    ----------
    - name: str
        name of the table in the SQL database
    - dataframe: DataFrame
        dataframe storing the data

    - getFromDatabase: Callable
        function to retreive data from the database
    - downloadFunction: Callable
        function to call to get the data
    - indexAxisName: List[str]
        index axis name of the dataframe once extracted of the SQL database
    - unit: units.Unit
        Unit of the data
    """

    name: str = "Generic Data"
    dataframe: DataFrame = None
    unit: Units

    start: Optional[datetime] = None
    end: Optional[datetime] = None
    listCountry: List[Country.Country] = Country.getAllCountries()
    listCategory: List[str] = None

    sqlTable: Optional[str] = None
    indexAxisName: List[Union[str, None]]

    downloader: Downloader = Downloader()

    def __init__(
        self,
        dataframe: Union[DataFrame, GenericData, None] = None,
        unit: Optional[Units] = None,
        name: Optional[str] = "Generique Data",
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: Optional[List[Country.Country]] = None,
        listCategory: Optional[List[str]] = None,
    ) -> None:

        if name:
            self.name = name
        if unit:
            self.unit = unit

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = listCategory

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        elif self.sqlTable is not None:
            self.getFromDatabase()

    def __add__(self, other: GenericData):
        df = self.dataframe + other.dataframe
        data = GenericData(df)
        return data

    def __sub__(self, other: GenericData):
        df = self.dataframe - other.dataframe
        data = GenericData(df)
        return data

    def __mul__(self, other: GenericData):
        df = self.dataframe * other.dataframe
        data = GenericData(df)
        return data

    def __div__(self, other: GenericData):
        df = self.dataframe / other.dataframe
        data = GenericData(df)
        return data

    def copy(self) -> GenericData:
        return copy.copy(self)

    def getDatabaseCriteria(self, criteria: Dict[str, Any]) -> List:
        """
        Adapte database criteria to the query
        """
        return [criteria[key] if key in criteria.keys() else None for key in self.indexAxisName]

    def getFromDatabase(
        self, convertCategory: Optional[Dict[str, str]] = None, verbose: bool = True, **kwargs
    ) -> GenericData:
        """Get data from the database

        Parameters
        ----------
        convertCategory: Optional[Dict[str, str]]
        verbose: bool
            Display informationn on the

        Returns
        -------
            data between two dates and filtered by countries and categories
        """

        from lib.pandas import PandasSelection

        # Collect data
        if self.dataframe is None:
            if verbose:
                print("Collect data")
            PandasStorage.get(data=self, verbose=verbose)

        # Select dates
        if self.start and self.end:
            if verbose:
                print("Filter data using start and end date")
            PandasSelection.selectBetweenDatetime(
                data=self, datetimeStart=self.start, datetimeEnd=self.end
            )

        # Select countries if required
        if self.listCountry:
            if verbose:
                print("Filter data using country list")
            PandasSelection.selectCountry(data=self, country=[c.iso for c in self.listCountry])

        # Select categories if required
        if self.listCategory:
            if verbose:
                print("Filter data using category list")
            PandasSelection.selectCategory(data=self, category=self.listCategory)

        # Convert categories if required
        if convertCategory:
            if verbose:
                print("Filter data using category list")
            PandasSelection.convertCategory(data=self, categories=convertCategory)

        return self

    def download(self):
        self.downloader.sqlTable = self.sqlTable
        self.downloader.download(
            start=self.start,
            end=self.end,
            listCountry=self.listCountry,
            indexAxisName=self.indexAxisName,
        )

    ## Unit and names
    def getGenericName(self) -> str:
        """
        Return the generic name

        Returns
        -------
        Return the generic name (name and unit) of the data
        """

        return self.name + " [" + self.unit.symbol + "]"

    def setNameUnit(
        self, name: Optional[str] = None, unit: Optional[Units] = None, convert: bool = False
    ):
        """
        Set the name and the unit of the data. The function does not apply conversion.
        Convert automatically to the correct unit if requested

        Parameters
        ----------
        name: Optional[str]
            New name of the data
        unit: Optional[Unit]
            New unit of the data
        convert:bool
            Convert data to the new unit

        Returns
        -------
        self
        """

        if name:
            self.name = name
        if unit:
            self.unit = unit

        return self

    def convert(self, unit: Units):
        """
        Convert the data unit to a new unit

        Parameters
        ----------
        unit: Unit
            The unit to use

        Returns
        -------
        self
        """

        # Get the unit
        previousUnit = self.unit
        newUnit = unit

        if previousUnit == newUnit:
            return self

        if previousUnit:
            # Get the scales
            previousScale = previousUnit.value
            newScale = newUnit.value
            # compute
            try:
                self.dataframe = self.dataframe * previousScale / newScale
            except TypeError:
                pass

        self.setNameUnit(unit=newUnit, convert=False)

        return self
