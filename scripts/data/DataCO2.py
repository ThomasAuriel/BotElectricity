from datetime import datetime
from typing import Dict, List, Optional, Union

from pandas import DataFrame

from data import CO2IntensityDictionnary, Country, Index, Units
from data.Data import GenericData
from data.DataPower import Generation
from calculation import CO2Calculation


class DataCO2(GenericData):
    pass


class CO2Intensity(DataCO2):
    name = "CO2 intensity"
    unit = Units.gCO2eq_kWh

    sqlTable: str = None
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]

    CO2IntensityDict: Dict[str, float]
    levels: Union[List[str], None]

    def __init__(
        self,
        dataframe: Optional[DataFrame] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: List[Country.Country] = Country.getAllCountries(),
        listCategory: List[str] = None,
        generation: Optional[Generation] = None,
        co2Emission: Optional[GenericData] = None,
        CO2IntensityDict: CO2IntensityDictionnary.CO2IntensityCategories = CO2IntensityDictionnary.CO2Intensity_ENTSOE(),
        levels: Union[List[str], None] = None,
    ) -> None:

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = listCategory

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        else:

            CO2Intensity, CO2unit = CO2Calculation.computeCO2Intensity(
                generation=generation,
                CO2Emission=co2Emission,
                CO2IntensityDict=CO2IntensityDict,
                levels=levels,
            )
            self.dataframe = CO2Intensity
            self.unit = CO2unit
            self.CO2IntensityDict = CO2IntensityDict
            self.levels = levels


class CO2Emission(DataCO2):
    name = "CO2 emission"
    indexAxisName = [
        Index.country,
        Index.datetime,
        Index.category,
    ]
    unit = Units.tCO2eq
    CO2IntensityDict: Dict[str, float]
    levels: Union[List[str], None]

    def __init__(
        self,
        dataframe: Optional[DataFrame] = None,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        listCountry: List[Country.Country] = Country.getAllCountries(),
        listCategory: List[str] = None,
        generation: Optional[Generation] = None,
        CO2Intensity: Optional[CO2Intensity] = None,
        CO2IntensityDict: CO2IntensityDictionnary.CO2IntensityCategories = CO2IntensityDictionnary.CO2Intensity_ENTSOE(),
        levels: Union[List[str], None] = None,
    ) -> None:

        if start:
            self.start = start
        if end:
            self.end = end
        if listCountry:
            self.listCountry = listCountry
        if listCategory:
            self.listCategory = listCategory

        if type(dataframe) is DataFrame:
            self.dataframe = dataframe
        elif type(dataframe) is GenericData:
            self.dataframe = dataframe.dataframe
        else:

            CO2Emission, CO2unit = CO2Calculation.computeCO2Emission(
                generation=generation,
                CO2Intensity=CO2Intensity,
                CO2IntensityDict=CO2IntensityDict,
                levels=levels,
            )
            self.dataframe = CO2Emission
            self.unit = CO2unit
            self.CO2IntensityDict = CO2IntensityDict
            self.levels = levels
