from typing import List


class Mix(object):
    """Mix object to group generation categories

    Parameter
    ---------
    name : str
        Mix name
    values: List[str]
        List of the categories to group in the mix
    """

    def __init__(self, name: str, values: List[str]):
        """
        Constructor
        """

        self.name = name
        self.values = values


def getAllMixNames():

    return [mix.name for mix in Mix.values()]


ENTSOE_complete = Mix(
    name="complete mix",
    values=[
        "Mixed",
        "Biomass",
        "Fossil Brown coal/Lignite",
        "Fossil Coal-derived gas",
        "Fossil Gas",
        "Fossil Hard coal",
        "Fossil Oil",
        "Fossil Oil shale",
        "Fossil Peat",
        "Geothermal",
        "Hydro Pumped Storage",
        "Hydro Run-of-river and poundage",
        "Hydro Water Reservoir",
        "Marine",
        "Nuclear",
        "Other renewable",
        "Solar",
        "Waste",
        "Wind Offshore",
        "Wind Onshore",
        "Other",
    ],
)
ENTSOE_complete_Balance = Mix(
    name="complete mix with balance",
    values=[
        "Balance",
        "Mixed",
        "Biomass",
        "Fossil Brown coal/Lignite",
        "Fossil Coal-derived gas",
        "Fossil Gas",
        "Fossil Hard coal",
        "Fossil Oil",
        "Fossil Oil shale",
        "Fossil Peat",
        "Geothermal",
        "Hydro Pumped Storage",
        "Hydro Run-of-river and poundage",
        "Hydro Water Reservoir",
        "Marine",
        "Nuclear",
        "Other renewable",
        "Solar",
        "Waste",
        "Wind Offshore",
        "Wind Onshore",
        "Other",
    ],
)

ENTSOE_low_carbon = Mix(
    name="low carbon mix",
    values=[
        "Geothermal",
        "Hydro Pumped Storage",
        "Hydro Run-of-river and poundage",
        "Hydro Water Reservoir",
        "Marine",
        "Nuclear",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

ENTSOE_high_carbon = Mix(
    name="high carbon mix",
    values=[
        "Mixed",
        "Biomass",
        "Fossil Brown coal/Lignite",
        "Fossil Coal-derived gas",
        "Fossil Gas",
        "Fossil Hard coal",
        "Fossil Oil",
        "Fossil Oil shale",
        "Fossil Peat",
        "Waste",
        "Other",
    ],
)

ENTSOE_fossil = Mix(
    name="fossil fuel mix",
    values=[
        "Mixed",
        "Fossil Brown coal/Lignite",
        "Fossil Coal-derived gas",
        "Fossil Gas",
        "Fossil Hard coal",
        "Fossil Oil",
        "Fossil Oil shale",
        "Fossil Peat",
        "Other",
    ],
)

ENTSOE_coal = Mix(
    name="coal",
    values=[
        "Fossil Brown coal/Lignite",
        "Fossil Hard coal",
        "Fossil Peat",
    ],
)

ENTSOE_gas = Mix(
    name="gas",
    values=[
        "Fossil Coal-derived gas",
        "Fossil Gas",
    ],
)

ENTSOE_renewable_with_hydro = Mix(
    name="renewable mix with hydro",
    values=[
        "Biomass",
        "Geothermal",
        "Hydro Pumped Storage",
        "Hydro Run-of-river and poundage",
        "Hydro Water Reservoir",
        "Marine",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

ENTSOE_renewable_without_hydro = Mix(
    name="renewable mix without hydro",
    values=[
        "Biomass",
        "Geothermal",
        "Marine",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

ENTSOE_variable_renewable_energy = Mix(
    name="variable renewable mix",
    values=[
        "Marine",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

ENTSOE_biomass = Mix(
    name="biomass mix",
    values=[
        "Biomass",
    ],
)
ENTSOE_hydro = Mix(
    name="hydroelectricity mix",
    values=[
        "Hydro Pumped Storage",
        "Hydro Run-of-river and poundage",
        "Hydro Water Reservoir",
    ],
)
ENTSOE_solar = Mix(
    name="solar mix",
    values=[
        "Solar",
    ],
)
ENTSOE_wind = Mix(
    name="wind mix",
    values=[
        "Wind Offshore",
        "Wind Onshore",
    ],
)

ENTSOE_nuclear_with_hydro = Mix(
    name="nuclear mix with hydro",
    values=[
        "Hydro Pumped Storage",
        "Hydro Run-of-river and poundage",
        "Hydro Water Reservoir",
        "Nuclear",
    ],
)

ENTSOE_nuclear_without_hydro = Mix(
    name="nuclear mix",
    values=[
        "Nuclear",
    ],
)


ENTSOE_mixes = {
    ENTSOE_complete.name: ENTSOE_complete,
    ENTSOE_low_carbon.name: ENTSOE_low_carbon,
    ENTSOE_high_carbon.name: ENTSOE_high_carbon,
    ENTSOE_fossil.name: ENTSOE_fossil,
    ENTSOE_coal.name: ENTSOE_coal,
    ENTSOE_gas.name: ENTSOE_gas,
    ENTSOE_biomass.name: ENTSOE_biomass,
    ENTSOE_hydro.name: ENTSOE_hydro,
    ENTSOE_solar.name: ENTSOE_solar,
    ENTSOE_wind.name: ENTSOE_wind,
    ENTSOE_variable_renewable_energy.name: ENTSOE_variable_renewable_energy,
    ENTSOE_renewable_with_hydro.name: ENTSOE_renewable_with_hydro,
    ENTSOE_renewable_without_hydro.name: ENTSOE_renewable_without_hydro,
    ENTSOE_nuclear_with_hydro.name: ENTSOE_nuclear_with_hydro,
    ENTSOE_nuclear_without_hydro.name: ENTSOE_nuclear_without_hydro,
}


## IPCC

IPCC_complete = Mix(
    name="complete",
    values=[
        "Biomass",
        "Coal",
        # "Lignite",
        "Gas",
        "Geothermal",
        "Oil",
        "Hydro",
        # "Hydro Reservoir",
        "Nuclear",
        "Other",
        "Other renewable",
        "Solar",
        "Waste",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_complete_Balance = Mix(
    name="complete mix",
    values=[
        "Balance",
        "Biomass",
        "Coal",
        # "Lignite",
        "Gas",
        "Geothermal",
        "Oil",
        "Hydro",
        # "Hydro Reservoir",
        "Nuclear",
        "Other",
        "Other renewable",
        "Solar",
        "Waste",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_low_carbon = Mix(
    name="low carbon mix",
    values=[
        "Geothermal",
        "Hydro",
        "Hydro Reservoir",
        "Nuclear",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_high_carbon = Mix(
    name="high carbon mix",
    values=[
        "Biomass",
        "Coal",
        "Lignite",
        "Gas",
        "Oil",
        "Other",
        "Waste",
    ],
)

IPCC_fossil = Mix(
    name="fossil fuel mix",
    values=[
        "Coal",
        "Lignite",
        "Gas",
        "Oil",
        "Other",
    ],
)

IPCC_renewable_with_hydro = Mix(
    name="renewable mix with hydro",
    values=[
        "Biomass",
        "Geothermal",
        "Hydro",
        "Hydro Reservoir",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_renewable_without_hydro = Mix(
    name="renewable mix without hydro",
    values=[
        "Biomass",
        "Geothermal",
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_variable_renewable_energy = Mix(
    name="variable renewable mix",
    values=[
        "Other renewable",
        "Solar",
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_biomass = Mix(
    name="biomass mix",
    values=["Biomass"],
)
IPCC_hydro = Mix(
    name="hydroelectricity mix",
    values=["Hydro", "Hydro Reservoir"],
)
IPCC_Solar = Mix(
    name="solar mix",
    values=[
        "Solar",
    ],
)
IPCC_Wind = Mix(
    name="wind mix",
    values=[
        "Wind Offshore",
        "Wind Onshore",
    ],
)

IPCC_nuclear_with_hydro = Mix(
    name="nuclear mix with hydro",
    values=[
        "Hydro",
        "Hydro Reservoir",
        "Nuclear",
    ],
)

IPCC_nuclear_without_hydro = Mix(
    name="nuclear mix",
    values=[
        "Nuclear",
    ],
)


IPCC_mixes = {
    IPCC_complete.name: IPCC_complete,
    IPCC_low_carbon.name: IPCC_low_carbon,
    IPCC_high_carbon.name: IPCC_high_carbon,
    IPCC_fossil.name: IPCC_fossil,
    IPCC_biomass.name: IPCC_biomass,
    IPCC_hydro.name: IPCC_hydro,
    IPCC_Solar: IPCC_Solar,
    IPCC_Wind.name: IPCC_Wind,
    IPCC_variable_renewable_energy.name: IPCC_variable_renewable_energy,
    IPCC_renewable_with_hydro.name: IPCC_renewable_with_hydro,
    IPCC_renewable_without_hydro.name: IPCC_renewable_without_hydro,
    IPCC_nuclear_with_hydro.name: IPCC_nuclear_with_hydro,
    IPCC_nuclear_without_hydro.name: IPCC_nuclear_without_hydro,
}

MIX_complete = Mix(
    name="Complete",
    values=[
        "Renewable",
        "Nuclear",
        "Hydro",
        "Fossil",
        "Other",
    ],
)

MIX_mixes = {
    MIX_complete.name: MIX_complete,
}
