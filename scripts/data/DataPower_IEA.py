from datetime import datetime
from typing import List

import numpy
import pandas as pandas
import world_bank_data as wb
from lib.entsoe.entsoepy.entsoe import EntsoePandasClient
from lib.pandas import PandasOperation, PandasSelection, PandasToSQL

from data import Country, Index, Units
from data.Data import GenericData


class DataIEA(GenericData):
    def downloadData(
        self: GenericData,
        countries: List[Country.Country],
        startYear: int,
        endYear: int,
        deltaTime: str = "60T",
        downloadIfPresent: bool = False,
        verbose: bool = True,
    ) -> GenericData:
        """
        Download the IEA Data and store it in the SQL database

        Parameters
        ----------
        data: Data.GenericData
        countries: List[Country]
            List of country to download
        start, end: datetime
            Date interval to respect for download
        deltaTime: string
        downloadIfPresent: bool
            Check if data is present before to download it
        """

        if verbose:
            print("Get the data from the IEA platform")
            print(self.downloadFunction)
            print("start = " + str(startYear))
            print("end = " + str(endYear))
            print("unit = " + self.unit.name)

        # Work with UTC dates
        # start = start.tz_convert("UTC")
        # end = end.tz_convert("UTC")

        ## Create dictionnary between country name and country ISO
        listCountriesIEA = wb.get_countries()
        listCountriesIEA = (
            listCountriesIEA.reset_index()
            .set_index("name")
            .drop(
                labels=[
                    "id",
                    "region",
                    "adminregion",
                    "incomeLevel",
                    "lendingType",
                    "capitalCity",
                    "longitude",
                    "latitude",
                ],
                axis=1,
            )
        )
        isoNameRelation = dict(zip(listCountriesIEA.index, listCountriesIEA.values.flatten()))

        ## Format date
        if startYear == endYear:
            requestDate = str(startYear)
        else:
            requestDate = str(startYear) + ":" + str(endYear)

        ## Send request to the IEA platform
        df = wb.get_series("SP.POP.TOTL", date=requestDate).to_frame()

        """ Restructure the data """
        if verbose:
            print("Restructure data")

        ## Add two next year data (to get complet years)
        lastYear = PandasSelection.selectDataframe(df, parameter="Year", value=endYear)
        lastYear = lastYear.reset_index()
        lastYear["Year"] = pandas.to_numeric(lastYear["Year"])
        lastYear["Year"] += 2
        lastYear = lastYear.set_index(["Country", "Series", "Year"])
        df = pandas.concat([df, lastYear])

        ## Change country name to iso
        df = df.rename(index=isoNameRelation, level=0)
        ## Change the column name
        df = df.reset_index()
        df[Index.country] = df["Country"]
        df[Index.datetime] = df.apply(
            lambda x: pandas.Timestamp(datetime(int(x["Year"]), 1, 1), tz="UTC"), axis=1
        )
        df = df.drop(columns=["Country", "Series", "Year"])
        df = df.set_index(keys=[Index.country, Index.datetime])

        ## Keep only wanted countries
        isoCountries = [c.iso for c in countries]
        df = PandasSelection.selectDataframe(df, parameter=Index.country, value=isoCountries)

        ## Ressample data
        df = PandasOperation.resampleOverTime(data=df, level=Index.datetime, freq=deltaTime)
        df = df.tz_convert(tz="UTC", axis=0, level=Index.datetime)

        # Turn column to index
        if self.columnName:
            df = df.rename(columns=self.columnName)
        df = df.stack(list(range(df.columns.nlevels)))  # Production to index
        df = df.to_frame()  # Turn it as a frame
        df.columns = [self.name]

        # Fill hole in data
        df = df.fillna(value=0)
        df = df.groupby(axis=1, level=[0]).sum()
        df = df.replace([numpy.inf, -numpy.inf], numpy.nan)

        ## Name axis and columns
        df.index.names = self.indexAxisName  # Make sure the index has the correct name

        if verbose:
            print("Store data")

        self.dataframe = df
        PandasToSQL.store(self, verbose=verbose)

        if verbose:
            print("Data stored")


class Population(DataIEA):

    name = "Population"
    sqlTable = "population"
    dataframe = None
    downloadFunction = staticmethod(EntsoePandasClient.query_day_ahead_prices)
    indexAxisName = [
        Index.country,
        Index.category,
        Index.datetime,
    ]
    columnName = {"SP.POP.TOTL": "Population"}
    unit = Units.capita
