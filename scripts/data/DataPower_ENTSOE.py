from datetime import datetime
from typing import Callable, Dict, List, Optional

import Key
import numpy
import pandas as pandas
from lib.entsoe.entsoepy.entsoe import EntsoePandasClient
from lib.pandas import PandasStorage
from pandas import Series

from data import Country, Units
from data.Data import Downloader

client = EntsoePandasClient(api_key=Key.ENTSOEToken)


class DownloaderENTSOE(Downloader):

    downloadFunction: Optional[Callable] = None
    downloadParameters: Dict[str, str] = {}

    def download(
        self,
        start: datetime,
        end: datetime,
        listCountry: List[Country.Country],
        indexAxisName: List[str],
        deltaTime: str = "60T",
        unit: Units.Unit = Units.MWh,
        verbose: bool = True,
    ):
        """
        Download the ENTSO-E Data and store it in the SQL database

        Parameters
        ----------
        data: Data.GenericData
        entsoeClientFunction: Callable
            Function from entsoe-py to use to download the data
        country: List[Country]
            List of country to download
        start, end: datetime
            Date interval to respect for download
        deltaTime: string
        unit: Units.Unit
        """

        if self.downloadFunction == None:
            raise Exception("No download function provided")

        if verbose:
            print("Get the data from the Transparency ENTSO-E platform")
            print(self.downloadFunction)
            print("start = " + str(start))
            print("end = " + str(end))
            print("unit = " + self.unit.name)

        # Work with UTC dates
        start = start.tz_convert("UTC")
        end = end.tz_convert("UTC")

        # For each countries
        for country in listCountry:

            try:

                toContinue = True
                toBreak = False

                while toContinue:

                    """Get Data"""
                    if verbose:
                        print(
                            "Get {dataName} data of {countryName} [{countryISO}] - {start} {end}".format_map(
                                {
                                    "dataName": self.name,
                                    "countryName": country.name,
                                    "countryISO": country.iso,
                                    "start": start.strftime("%Y/%m/%d-%H"),
                                    "end": end.strftime("%Y/%m/%d-%H"),
                                }
                            )
                        )

                    # Get the data from the transparency ENTSO-E platform for a country
                    try:
                        df = self.downloadFunction(
                            self=client,
                            country_code=country.iso,
                            start=start,
                            end=end,
                            **self.downloadParameters
                        )
                        toContinue = False  # data are downloaded, break the while loop
                        toBreak = False

                    except ConnectionError as err:
                        print(
                            "Error occurred for the country {countryName} [{countryISO}]".format_map(
                                {
                                    "countryName": country.name,
                                    "countryISO": country.iso,
                                }
                            )
                        )
                        print(type(err))
                        print(err.args)
                        print(err)
                        print("Restart this loop")
                        # raise err
                        toContinue = True
                        toBreak = False

                    except Exception as err:
                        print(
                            "Error occurred for the country {countryName} [{countryISO}]".format_map(
                                {
                                    "countryName": country.name,
                                    "countryISO": country.iso,
                                }
                            )
                        )
                        print(type(err))
                        print(err.args)
                        print(err)
                        toContinue = False
                        # toBreak = True
                        toBreak = False

                if toBreak:
                    return

                if verbose:
                    print("Data collected")

                # Check the data is dataframe
                if isinstance(df, Series):
                    df = df.to_frame()

                """ Restructure the data """
                if verbose:
                    print("Restructure data")

                # Resample the data
                df = df.resample(deltaTime).mean()
                df = df.tz_convert("UTC")

                # Turn column to index
                if hasattr(self, "columnName"):
                    df = df.rename(columns=self.columnName)
                df = df.stack(list(range(df.columns.nlevels)))  # Production to index
                df = df.to_frame()  # Turn it as a frame

                # Fill hole in data
                df = df.fillna(value=0)
                df = df.groupby(axis=1, level=[0]).sum()
                df = df.replace([numpy.inf, -numpy.inf], numpy.nan)

                ## Add country
                df = pandas.concat([df], keys=[country.iso])

                ## Name axis and columns
                df.index.names = indexAxisName  # Make sure the index has the correct name
                df.columns = ["data"]

                if verbose:
                    print("Store data")

                self.dataframe = df
                PandasStorage.store(data=self)

                if verbose:
                    print("Data stored")
            except Exception as e:
                print(e)


class Generation_ENTSOE(DownloaderENTSOE):
    downloadFunction = staticmethod(EntsoePandasClient.query_generation)
    downloadParameters = {"nett": True}
    columnName = {0: "Load"}

    name: str = "ENTSO-E Generation"
    unit = Units.MWh


class Load_ENTSOE(DownloaderENTSOE):
    dataframe = None
    downloadFunction = staticmethod(EntsoePandasClient.query_load)
    columnName = {0: "Load"}

    name: str = "ENTSO-E Load"
    unit = Units.MWh


class Import_ENTSOE(DownloaderENTSOE):
    dataframe = None
    downloadFunction = staticmethod(EntsoePandasClient.query_import)

    name: str = "ENTSO-E Import"
    unit = Units.MWh


class Export_ENTSOE(DownloaderENTSOE):
    dataframe = None
    downloadFunction = staticmethod(EntsoePandasClient.query_export)

    name: str = "ENTSO-E Export"
    unit = Units.MWh


class DayAheadPrices_ENTSOE(DownloaderENTSOE):
    dataframe = None
    downloadFunction = staticmethod(EntsoePandasClient.query_day_ahead_prices)

    name: str = "ENTSO-E Day Ahead Price"
    unit = Units.euros
