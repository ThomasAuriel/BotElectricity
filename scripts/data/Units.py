# import re
from typing import Optional, Set

# regexNameUnit = re.compile(r"(?P<name>[\S\S]*)\s*(\((?P<unit>.*)\))+")


class Unit(object):
    """
    Unit to represent values
    It is possible to convert units with the same unit but not the same multiple using the values

    Parameter
    ---------
    name : str
        Unit name
    symbol: str
        Symbol of the unit
    value: float
        Value of the multiple
    """

    name: str
    symbol: str
    value: float

    def __init__(self, name: str, symbol: str, value: float):
        self.name = name
        self.symbol = symbol
        self.value = value


unitList: Set[Unit] = set()

""" Default Values """

noUnit = Unit(name="No Unit", symbol="NoUnit", value=1)

unitList.add(noUnit)

""" Ratio """

Ratio = Unit(name="Ratio", symbol="0-1", value=1)
Percent = Unit(name="Percent [0-100]", symbol="%", value=100)

unitList.add(Ratio)
unitList.add(Percent)

""" Energy """

Wh = Unit(name="Wh", symbol="Wh", value=1)
kWh = Unit(name="kWh", symbol="kWh", value=1e3)
MWh = Unit(name="MWh", symbol="MWh", value=1e6)
GWh = Unit(name="GWh", symbol="GWh", value=1e9)
TWh = Unit(name="TWh", symbol="TWh", value=1e12)

unitList.add(Wh)
unitList.add(kWh)
unitList.add(MWh)
unitList.add(GWh)
unitList.add(TWh)

""" Puissance """

W = Unit(name="W", symbol="W", value=1)
kW = Unit(name="kW", symbol="kW", value=1e3)
MW = Unit(name="MW", symbol="MW", value=1e6)
GW = Unit(name="GW", symbol="GW", value=1e9)
TW = Unit(name="TW", symbol="TW", value=1e12)

unitList.add(W)
unitList.add(kW)
unitList.add(MW)
unitList.add(GW)
unitList.add(TW)

""" Duration """

s = Unit(name="second", symbol="s", value=1)
h = Unit(name="hour", symbol="h", value=3600)
d = Unit(name="day", symbol="d", value=86400)

unitList.add(s)
unitList.add(h)
unitList.add(d)

""" Mass """

g = Unit(name="gramme", symbol="g", value=1)
kg = Unit(name="kilogram", symbol="kg", value=1e3)
t = Unit(name="ton", symbol="t", value=1e6)
kt = Unit(name="kilo-ton", symbol="kt", value=1e9)
Mt = Unit(name="mega-ton", symbol="Mt", value=1e12)
Gt = Unit(name="giga-ton", symbol="Gt", value=1e15)

unitList.add(g)
unitList.add(kg)
unitList.add(t)
unitList.add(kt)
unitList.add(Mt)
unitList.add(Gt)

""" Mass of CO2eq """

gCO2eq = Unit(name="gramme CO2eq", symbol="gCO2eq", value=1)
kgCO2eq = Unit(name="kilogram CO2eq", symbol="kgCO2eq", value=1e3)
tCO2eq = Unit(name="ton CO2eq", symbol="tCO2eq", value=1e6)
ktCO2eq = Unit(name="kilo-ton CO2eq", symbol="ktCO2eq", value=1e9)
MtCO2eq = Unit(name="mega-ton CO2eq", symbol="MtCO2eq", value=1e12)
GtCO2eq = Unit(name="giga-ton CO2eq", symbol="GtCO2eq", value=1e15)

unitList.add(gCO2eq)
unitList.add(kgCO2eq)
unitList.add(tCO2eq)
unitList.add(ktCO2eq)
unitList.add(MtCO2eq)
unitList.add(GtCO2eq)

""" CO2eq Intensity """

gCO2eq_kWh = Unit(name="gramme CO2eq per kilowatt-hour", symbol="gCO2eq/kWh", value=1)
kgCO2eq_kWh = Unit(name="kilogram CO2eq per kilowatt-hour", symbol="kgCO2eq/kWh", value=1e3)
kgCO2eq_MWh = Unit(name="kilogram CO2eq per megawatt-hour", symbol="kgCO2eq/MWh", value=1)
tCO2eq_MWh = Unit(name="ton CO2eq per megawatt-hour", symbol="tCO2eq/MWh", value=1e3)
tCO2eq_GWh = Unit(name="ton CO2eq per gigawatt-hour", symbol="tCO2eq/GWh", value=1)

unitList.add(gCO2eq_kWh)
unitList.add(kgCO2eq_kWh)
unitList.add(kgCO2eq_MWh)
unitList.add(tCO2eq_MWh)
unitList.add(tCO2eq_GWh)


""" Price """
euros = Unit(name="Euros", symbol="EUR", value=1)

unitList.add(euros)


""" Capita """

capita = Unit(name="Capita", symbol="cap", value=1)
millionCapita = Unit(name="Million Capita", symbol="Mcap", value=1000000)

unitList.add(capita)
unitList.add(millionCapita)


def getUnitByName(name: str) -> Optional[Unit]:
    """
    Get the unit based on the symbol

    Parameter
    ---------
    name: str
        Name of the unit to return

    Return
    ------
    Return the unit or None if not found
    """

    try:
        for unit in unitList:
            if unit.name == name:
                return unit
    except Exception:
        # Do nothing. No unit
        pass

    return None


def getUnitBySymbol(symbol: str) -> Optional[Unit]:
    """Get the unit based on the symbol

    Parameter
    ---------
    symbol:str
        Name of the unit to return

    Return
    ------
    Return the unit or None if not found"""

    try:
        for unit in unitList:
            if unit.symbol == symbol:
                return unit
    except Exception:
        # Do nothing. No unit
        pass

    return None
