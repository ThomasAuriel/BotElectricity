from datetime import datetime
from typing import Callable, Dict, List, Optional

import Key
import numpy
import pandas
from ElexonDataPortal import api as ElexonDataPortalAPI
from lib.pandas import PandasStorage
from pandas import Series

from data import Country, Index, Units
from data.Data import Downloader, GenericData

client = ElexonDataPortalAPI.Client(Key.ElexonKey)


class DownloaderElexon(Downloader):

    downloadFunction: Optional[Callable] = None
    downloadParameters: Dict[str, str] = {}

    def download(
        self,
        start: datetime,
        end: datetime,
        listCountry: List[Country.Country],
        indexAxisName: List[str],
        deltaTime: str = "60T",
        unit: Units.Unit = Units.MWh,
        verbose: bool = True,
    ) -> GenericData:
        """
        Download the Elexon data (United Kingdom data) and store it

        Parameters
        ----------
        data: Data.GenericData
        start, end: datetime
            Date interval to respect for download
        deltaTime: string
        unit: units.Unit
        downloadIfPresent: bool
            Check if data is present before to download it
        """

        if self.downloadFunction == None:
            raise Exception("No download function provided")

        if verbose:
            print("Get the data from the Elexon platform")
            print(self.downloadFunction)
            print("start = " + str(start))
            print("end = " + str(end))
            print("unit = " + self.unit.name)

        # Work with UTC dates
        start = start.tz_convert("UTC")
        end = end.tz_convert("UTC")

        try:

            toContinue = True
            toBreak = False

            while toContinue:

                """Get Data"""
                if verbose:
                    print(
                        "Get {dataName} data of {countryName} [{countryISO}] - {start} {end}".format_map(
                            {
                                "dataName": self.name,
                                "countryName": Country.GB.name,
                                "countryISO": Country.GB.iso,
                                "start": start.strftime("%Y/%m/%d-%H"),
                                "end": end.strftime("%Y/%m/%d-%H"),
                            }
                        )
                    )

                # Get the data from the transparency ENTSO-E platform for a country
                try:
                    df = self.downloadFunction(
                        client=client,
                        start=start,
                        end=end,
                    )
                    toContinue = False  # data are downloaded, break the while loop
                    toBreak = False

                except ConnectionError as err:
                    print(
                        "Error occurred for the country {countryName} [{countryISO}]".format_map(
                            {
                                "countryName": Country.GB.name,
                                "countryISO": Country.GB.iso,
                            }
                        )
                    )
                    print(type(err))
                    print(err.args)
                    print(err)
                    print("Restart this loop")
                    # raise err
                    toContinue = True
                    toBreak = False

                except Exception as err:
                    print(
                        "Error occurred for the country {countryName} [{countryISO}]".format_map(
                            {
                                "countryName": Country.GB.name,
                                "countryISO": Country.GB.iso,
                            }
                        )
                    )
                    print(type(err))
                    print(err.args)
                    print(err)
                    toContinue = False
                    # toBreak = True
                    toBreak = False

            if toBreak:
                return

            if verbose:
                print("Data collected")

            # Check the data is dataframe
            if isinstance(df, Series):
                df = df.to_frame()

            """ Restructure the data """
            if verbose:
                print("Restructure data")

            # Resample and set index
            df = (
                df.groupby([Index.country, Index.category])
                .resample("60T", level=Index.datetime)
                .sum()
            )

            # Fill hole in data
            df = df.fillna(value=0)
            df = df.replace([numpy.inf, -numpy.inf], numpy.nan)

            if verbose:
                print("Store data")

            self.dataframe = df
            PandasStorage.store(data=self)

            if verbose:
                print("Data stored")
        except Exception as e:
            print(e)

        return self


class Generation_Elexon(DownloaderElexon):
    def downloadFunction(self, client, start, end):
        df = client.get_B1620(
            start_date=start.strftime("%Y-%m-%d %H:%M"),
            end_date=end.strftime("%Y-%m-%d %H:%M"),
        )

        # Clean the data
        df = df.drop(
            df.columns.difference(["local_datetime", "quantity", "powerSystemResourceType"]), 1
        )

        # Set indexes
        df = df.rename(
            {
                "local_datetime": Index.datetime,
                "quantity": "data",
                "powerSystemResourceType": Index.category,
            },
            axis=1,
        )

        # Format data
        df["data"] = df["data"].astype(float)

        # Format datetime
        df[Index.datetime] = df[Index.datetime].dt.tz_convert("UTC")

        # Format categories
        df[Index.category] = df[Index.category].str.replace('"', "")
        df = df.set_index(keys=[Index.category, Index.datetime])

        ## Format country
        df = pandas.concat([df], keys=[Country.GB.iso])

        ## Name axis and columns
        df.index.names = [
            Index.country,
            Index.category,
            Index.datetime,
        ]  # Make sure the index has the correct name
        df.columns = ["data"]

        return df

    # downloadParameters = {"nett": True}
    columnName = {0: "Load"}

    name: str = "Elexon Generation"
    unit = Units.MWh


class Load_Elexon(DownloaderElexon):
    def downloadFunction(self, client, start, end):
        df = client.get_B0610(
            start_date=start.strftime("%Y-%m-%d %H:%M"),
            end_date=end.strftime("%Y-%m-%d %H:%M"),
        )

        # Clean the data
        df = df.drop(df.columns.difference(["local_datetime", "quantity"]), 1)

        # Set indexes
        df = df.rename({"local_datetime": Index.datetime, "quantity": "data"}, axis=1)

        # Format data
        df["data"] = df["data"].astype(float)

        # Format datetime
        df[Index.datetime] = df[Index.datetime].dt.tz_convert("UTC")
        df = df.set_index(keys=[Index.datetime])

        # Format categories
        df = pandas.concat([df], keys=["Actual Load"])

        ## Format country
        df = pandas.concat([df], keys=[Country.GB.iso])

        ## Name axis and columns
        df.index.names = [
            Index.country,
            Index.category,
            Index.datetime,
        ]  # Make sure the index has the correct name
        df.columns = ["data"]

        return df

    columnName = {0: "Load"}

    name: str = "Elexon Load"
    unit = Units.MWh


class DayAheadPrices_Elexon(DownloaderElexon):
    def downloadFunction(self, client, start, end):
        df = client.get_B1770(
            start_date=start.strftime("%Y-%m-%d %H:%M"),
            end_date=end.strftime("%Y-%m-%d %H:%M"),
        )

        # Clean the data
        df = df.drop(
            df.columns.difference(["local_datetime", "imbalancePriceAmountGBP", "priceCategory"]),
            axis=1,
        )
        df = df.set_index(keys=["priceCategory"])
        df = df.loc["Excess balance"]
        df = df.reset_index().drop(["priceCategory"], axis=1)
        # Set indexes
        df = df.rename(
            {"local_datetime": Index.datetime, "imbalancePriceAmountGBP": "data"}, axis=1
        )

        # Format data
        df["data"] = df["data"].astype(float)

        # Format datetime
        df[Index.datetime] = df[Index.datetime].dt.tz_convert("UTC")
        df = df.set_index(keys=[Index.datetime])

        # Format categories
        df = pandas.concat([df], keys=[0])

        ## Format country
        df = pandas.concat([df], keys=[Country.GB.iso])

        ## Name axis and columns
        df.index.names = [
            Index.country,
            Index.category,
            Index.datetime,
        ]  # Make sure the index has the correct name
        df.columns = ["data"]

        return df

    name: str = "Elexon Day Ahead Price"
    unit = Units.MWh
