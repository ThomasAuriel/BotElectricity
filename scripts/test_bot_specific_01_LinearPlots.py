from datetime import datetime

import matplotlib.dates as mdates
import pandas

from data import Country, DataPower, Index, DataCO2
from figure.Figure import Figure, Subplot
from figure.genericfigures import LinearPlot
from data import Units
from lib import StringUtils

# Dates
start = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2023, 1, 1), tz="UTC")
# Countries
listCountries = [
    Country.BE,
    Country.CH,
    Country.DE,
    Country.DK,
    Country.ES,
    Country.FR,
    Country.GB,
    Country.IT,
    Country.NO,
    Country.PL,
    Country.PT,
    Country.SE,
]
listCountries = [
    Country.FR,
]
# Categories
listCategory = None

for c in listCountries:

    generation = DataPower.GenerationBalance(start=start, end=end, listCountry=[c])
    load = DataPower.Load(start=start, end=end, listCountry=[c])

    filename = StringUtils.format_map(
        "./output/test/test_bot_specific/O1/01-test_Generation_{country}.png", {"country": c.iso}
    )
    title = StringUtils.format_map(
        "Electricity generation, balance and load for {country}\n{start} to {end}",
        {"country": c.iso},
    )

    # Linear figures
    figure = Figure(
        filename=filename,
        data=[generation, load],
        title=title,
    )
    s1 = Subplot(
        functionPlot=LinearPlot.stackplot,
        dataIndex=[0],
        abscisseIndex=Index.datetime,
        categoryIndex=Index.category,
        majorLocatorInterval=mdates.MonthLocator(interval=1),
        nrows=1,
        ncols=1,
        index=1,
    )
    s2 = Subplot(
        functionPlot=LinearPlot.linearplot,
        dataIndex=[1],
        abscisseIndex=Index.datetime,
        categoryIndex=Index.category,
        majorLocatorInterval=mdates.MonthLocator(interval=1),
        nrows=1,
        ncols=1,
        index=1,
    )
    figure.addSubplots(subplots=[s1, s2])
    figure.plot(plotIfExist=True)

    # Balance
    balance = DataPower.Balance(start=start, end=end, listCountry=[c])

    filename = StringUtils.format_map(
        "./output/test/test_bot_specific/01/01-test_Balance_{country}.png", {"country": c.iso}
    )
    title = StringUtils.format_map(
        "Detailed electricity balance for {country}\n{start} to {end}", {"country": c.iso}
    )

    # Linear figures
    figure = Figure(
        filename=filename,
        data=[balance],
        title=title,
    )
    s1 = Subplot(
        functionPlot=LinearPlot.stackplot,
        dataIndex=[0],
        abscisseIndex=Index.datetime,
        categoryIndex=Index.category,
        majorLocatorInterval=mdates.MonthLocator(interval=1),
        nrows=1,
        ncols=1,
        index=1,
    )
    figure.addSubplots(subplots=[s1])
    figure.plot(plotIfExist=True)

    # CO2
    generation = DataPower.Generation(start=start, end=end, listCountry=[c])
    CO2Emission = DataCO2.CO2Emission(start=start, end=end, listCountry=[c], generation=generation)
    CO2Emission = CO2Emission.convert(Units.tCO2eq)

    filename = StringUtils.format_map(
        "./output/test/test_bot_specific/01/01-test_CO2_{country}.png", {"country": c.iso}
    )
    title = StringUtils.format_map(
        "Electricity generation CO2 emissions for {country}\n{start} to {end}", {"country": c.iso}
    )
    figure = Figure(
        filename=filename,
        data=[CO2Emission],
        title=title,
    )
    s1 = Subplot(
        functionPlot=LinearPlot.stackplot,
        dataIndex=[0],
        abscisseIndex=Index.datetime,
        categoryIndex=Index.category,
        majorLocatorInterval=mdates.MonthLocator(interval=1),
        nrows=1,
        ncols=1,
        index=1,
    )
    figure.addSubplots(subplots=[s1])
    figure.plot(plotIfExist=True)


print("end")
