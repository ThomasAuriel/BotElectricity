from collections.abc import Iterable
from datetime import datetime
from typing import List, Union, Any

import numpy


def convertDatetime(dates: Union[List[Any], datetime]) -> Union[List[Any], datetime]:
    # TODO
    """Convert a numpy.datetime64 or a array of numpy.datetime64 into a datetime or an array of datetime"""

    if isinstance(dates, Iterable):
        return [convertDatetime(date) for date in dates]
    elif isinstance(dates, numpy.datetime64):
        return datetime.utcfromtimestamp(
            (dates - numpy.datetime64("1970-01-01T00:00:00Z")) / numpy.timedelta64(1, "s")
        )
    else:
        return dates
