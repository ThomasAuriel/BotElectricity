from __future__ import annotations

from enum import Enum

from data import Data

from . import PandasToCSV, PandasToSQL


class StorageType(Enum):
    SQL = 0
    CSV = 1


type = StorageType.SQL


def store(data: Data.GenericData, verbose: bool = True):
    # TODO

    if type == StorageType.SQL:
        PandasToSQL.store(data=data, verbose=verbose)
    else:
        PandasToCSV.store(data=data, verbose=verbose)


def get(data: Data.GenericData, verbose: bool = True) -> Data.GenericData:
    # TODO

    if type == StorageType.SQL:
        return PandasToSQL.get(data=data, verbose=verbose)
    else:
        return PandasToCSV.get(data=data, verbose=verbose)
