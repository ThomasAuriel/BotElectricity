import numpy
from pandas import DataFrame


def removeDuplicate(data: DataFrame):
    # TODO

    return data[~data.index.duplicated(keep="first")]


def removeNaNInf(data: DataFrame) -> DataFrame:
    # TODO

    tmp = data.copy()
    tmp = tmp.replace(to_replace=[numpy.nan, numpy.inf], value=0)
    return tmp
