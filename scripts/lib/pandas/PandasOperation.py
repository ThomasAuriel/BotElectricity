from typing import List, Optional, Union

from data import Index

import pandas as pandas
from pandas import DataFrame

from calculation.Grouping import Grouping


def sumOver(
    data: DataFrame,
    levels: Union[List[str], List[int], str, int],
    cumulativeAxis: Optional[str] = None,
) -> DataFrame:
    # TODO
    """
    Sum over the selected levels

    data: DataFrame
        data to sum
    levels: Union[List[str], List[int], str, int]
        levels to sum over
    cumulativeAxis: ptional[str]
        Add a step to sum cumulatively along the index provided
    """

    if type(levels) is list:
        levels = list(set(levels))
    toReturn = data.groupby(level=levels).sum()

    if cumulativeAxis:
        index = list(toReturn.index.names)
        index.remove(cumulativeAxis)
        toReturn = toReturn.groupby(level=index).cumsum()

    return toReturn


# def sumOverTime(data: DataFrame, level: str = Index.datetime, freq: str = "D") -> DataFrame:

#     level_values = data.index.get_level_values
#     data = data.groupby(
#         [level_values(i) for i in [0, 1]] + [pandas.Grouper(freq=freq, level=level)]
#     ).sum()

#     return data


def meanOver(data: DataFrame, levels: List[str]) -> DataFrame:
    # TODO
    """
    Mean over the selected levels

    data:
        data to sum
    levels:
        levels to sum over
    """

    return data.groupby(by=levels).mean()


def ratio(
    data1: pandas.DataFrame,
    data2: Union[pandas.DataFrame, None] = None,
    levels: List = [],
) -> DataFrame:
    # TODO
    """
    Ratio of data1 over data2

    data1, data2:
        data used to compute ratio
    levels:
        levels to sum over
    """

    if data2 is None:
        data2 = data1

    result = data1.div(data2.groupby(level=levels).max(), fill_value=0).fillna(0)

    return result


# def resampleOverTime(
#     data: pandas.DataFrame, level: str = index.datetime, freq: str = "D"
# ) -> pandas.DataFrame:

#     indexNames = data.index.names
#     indexNotLevel = [i for i in indexNames if i is not level]

#     data = (
#         data.reset_index(indexNotLevel)
#         .groupby(indexNotLevel, as_index=False)
#         .resample(freq)
#         .ffill()
#         .droplevel(0)  # Remove added numerical index
#         .reset_index()
#         .set_index(indexNames)  # Restore MultiIndex
#     )

#     return data


def resampleOverTime(
    data: DataFrame,
    levels: List[str] = [Index.datetime],
    freq: str = "D",
    grouping: Grouping = Grouping.sum,
) -> DataFrame:
    # TODO
    def getGrouper(indexName: str):
        if indexName in levels:
            return pandas.Grouper(freq=freq, level=indexName)
        else:
            return pandas.Grouper(level=indexName)

    indexNames = data.index.names

    if grouping == Grouping.sum:
        df = data.groupby([getGrouper(name) for name in indexNames]).sum()
    elif grouping == Grouping.mean:
        df = data.groupby([getGrouper(name) for name in indexNames]).mean()

    return df


def stats(data):
    return data.describe()


# def getMaxMin(
#     dfs: List[pandas.DataFrame], category: str = Index.datetime, stack: bool = False
# ) -> Tuple[float, float]:

#     maxs = [0]
#     mins = [0]

#     for df in dfs:
#         ## Find the maximum
#         df_max = df.copy()
#         df_max[df_max < 0] = 0
#         if stack:
#             df_max = sumOver(df_max, category).max()
#         df_min = df.copy()
#         df_min[df_min > 0] = 0
#         if stack:
#             df_min = sumOver(df_min, category).min()

#         maxs.append(df_max.max())
#         mins.append(df_min.min())

#     maxValue = max(maxs) * 1.05
#     minValue = min(mins) * 1.05

#     return maxValue, minValue
