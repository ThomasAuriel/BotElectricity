from typing import List, Union

from data import Index

import pandas as pd
from pandas import DataFrame


def getIndexeUniqueValues(data: DataFrame, level):
    # TODO

    return data.index.unique(level=level)


def getIndexeValues(data, level):
    # TODO

    return data.index.get_level_values(level)


def addIndexeLevel(data, levelName, levelValue):
    # TODO

    # Convert index to dataframe
    old_idx = data.index.to_frame()

    # Insert new level at specified location
    old_idx.insert(0, levelName, levelValue)

    # Convert back to MultiIndex
    data.index = pd.MultiIndex.from_frame(old_idx)

    return data


def defineTimeInterval(
    data: DataFrame,
    timeInterval: Union[List[str], str],
    category: str,
):
    # TODO
    data = data.reset_index()
    # Define the time interval and add it to the index
    if isinstance(timeInterval, str):
        timeInterval = [timeInterval]

    for interval in timeInterval:

        try:
            dt = data[Index.datetime].dt
        except Exception:
            dt = pd.to_datetime(data[Index.datetime]).dt

        if interval == "hours":
            data[interval] = dt.hour.apply("{:02d}".format)
        elif interval == "weekdays":
            dayofweek = (dt.dayofweek + 1).apply("{:02d}".format)
            nameofday = dt.day_name()
            data[interval] = dayofweek.str.cat(nameofday, sep=" - ")
        elif interval == "week":
            data[interval] = dt.week.apply("{:02d}".format)
        elif interval == "months":
            month = dt.month.apply("{:02d}".format)
            monthName = dt.month_name()
            data[interval] = month.str.cat(monthName, sep=" - ")
        elif interval == "years":
            data[interval] = dt.year.apply("{:02d}".format)
        else:
            raise Exception("timeInterval shall be : 'hours', 'weekdays', 'months', 'years'")

    data = data.set_index([category, Index.datetime], append=False)
    data["timeInterval"] = data[timeInterval].agg(" - ".join, axis=1)
    data = (
        data.drop(timeInterval, axis=1)
        .reset_index()
        .set_index([category, Index.datetime, "timeInterval"], append=False)
    )
    return data


def defineTimeIntervalBetween(
    data: DataFrame,
    indexCategory: str,
    intervalValues: List[object],
    intervalLabels: List[str],
    indexName: str,
):
    # TODO
    data = data.reset_index()
    data[indexName] = pd.cut(data[indexCategory], bins=intervalValues, labels=intervalLabels)
    return data
