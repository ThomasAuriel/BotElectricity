from __future__ import annotations


import pandabase
from data import Data, Index
from lib import FileUtils
from sqlalchemy import create_engine

import pandas as pandas
from pandas import DataFrame

SQLFile: str = "./input/ENTSOEData.db"
SQLCon: str = "sqlite:///" + SQLFile


engine = create_engine(SQLCon, echo=False)

SQLFolder = FileUtils.getParent(SQLFile)
FileUtils.createFolder(SQLFolder)


def store(data: Data.GenericData, verbose: bool = True):
    # TODO

    if verbose:
        print("Store " + data.name)

    FileUtils.createFolder(FileUtils.getParent(SQLFile))

    df = data.dataframe
    df = df.rename(columns=lambda s: "data")

    pandabase.to_sql(
        df,
        con=SQLCon,
        table_name=data.sqlTable,
        how="upsert",
        add_new_columns=True,
        # auto_index=False,
    )

    if verbose:
        print("Stored data")


def get(data: Data.GenericData, verbose: bool = True) -> Data.GenericData:
    # TODO

    if verbose:
        print(
            "Get data : {query} - {start} : {end}".format_map(
                {"query": data.name, "start": str(data.start), "end": str(data.end)}
            )
        )
    df = pandabase.read_sql(
        table_name=data.sqlTable,
        con=SQLCon,
        lowest=data.getDatabaseCriteria({Index.datetime: data.start}),
        highest=data.getDatabaseCriteria({Index.datetime: data.end}),
    )
    df = DataFrame(df)  # To modin dataframe

    if verbose:
        print("Complete the missing data")

    newIndex = pandas.MultiIndex.from_product(
        [df.index.unique(level=name) for name in data.indexAxisName],
        names=data.indexAxisName,
    )
    df = df.reindex(newIndex, axis=0)
    df = df.fillna(0)

    if verbose:
        print("Short data")

    df = df.sort_index()

    if verbose:
        print("Return data")

    data.dataframe = df
    return data
