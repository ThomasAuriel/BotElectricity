import os

import dask

# With modin
# from distributed import Client

# client = Client()

dask.config.set(scheduler="threads")

## https://github.com/modin-project/modin
os.environ["MODIN_ENGINE"] = "dask"  # Modin will use Dask

import pandas

# pandas.set_option("display.max_rows", 500)
pandas.set_option("display.max_columns", 500)
pandas.set_option("display.width", 1000)
