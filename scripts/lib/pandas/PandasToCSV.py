from __future__ import annotations

from data import Data, Index
from lib import FileUtils
from lib.pandas import PandasUtils

import pandas

CSVFile_default: str = "./input/data_{name}.csv"
CSVFolder_default: str = FileUtils.getParent(CSVFile_default)
FileUtils.createFolder(CSVFolder_default)


def write(data: pandas.DataFrame, CSVFile: str = CSVFile_default, verbose: bool = True):
    # TODO

    if verbose:
        print("Store CSV " + CSVFile)

    FileUtils.createFolder(FileUtils.getParent(CSVFile))

    df = data.copy()
    df = df.sort_index(axis=0)
    df = df[~df.index.duplicated(keep="first")]  # Remove duplicates
    df.to_csv(path_or_buf=CSVFile)

    if verbose:
        print("Stored data")


def read(CSVFile: str = CSVFile_default, verbose: bool = True) -> pandas.DataFrame:
    # TODO

    if verbose:
        print("Read CSV " + CSVFile)

    try:
        dataframe = pandas.read_csv(filepath_or_buffer=CSVFile)
        if Index.datetime in list(dataframe.keys()):
            dataframe[Index.datetime] = pandas.to_datetime(
                dataframe[Index.datetime], format="%Y-%m-%d %H:%M:%S%z"
            )

        newIndex = [
            value
            for value in [Index.country, Index.category, Index.datetime]
            if value in list(dataframe.keys())
        ]
        dataframe = dataframe.set_index(keys=newIndex)
        dataframe = PandasUtils.removeDuplicate(dataframe)
    except Exception as e:
        print(e)
        print("No database found")
        dataframe = pandas.DataFrame()

    if verbose:
        print("Read data")

    return dataframe


def store(data: Data.GenericData, CSVFile: str = CSVFile_default, verbose: bool = True):
    # TODO

    if verbose:
        print("Get current DB")
    currentDB = type(data)()
    currentDB = get(data=currentDB, CSVFile=CSVFile, verbose=verbose)

    if verbose:
        print("Append the collected data to the current DB")
    if not currentDB.dataframe.empty:
        data.dataframe = data.dataframe.reset_index().set_index(
            keys=[Index.country, Index.category, Index.datetime]
        )
        currentDB.dataframe = currentDB.dataframe.reset_index().set_index(
            keys=[Index.country, Index.category, Index.datetime]
        )
        data.dataframe = pandas.concat([data.dataframe, currentDB.dataframe])

        ## Remove duplicate indexes
        data.dataframe = PandasUtils.removeDuplicate(data.dataframe)
        # Sort data
        # data.dataframe = data.dataframe.sort_index(axis=0)

    if verbose:
        print("Store the whole")
    write(
        data=data,
        CSVFile=CSVFile.format_map({"name": data.name.replace(" ", "_")}),
        verbose=verbose,
    )


def get(
    data: Data.GenericData,
    CSVFile: str = CSVFile_default,
    verbose: bool = True,
) -> Data.GenericData:
    # TODO

    dataframe = read(
        CSVFile=CSVFile.format_map({"name": data.name.replace(" ", "_")}), verbose=verbose
    )
    data.dataframe = dataframe

    if data.start and data.end:
        from lib.pandas import PandasSelection

        data = PandasSelection.selectBetween(
            data, parameter=Index.datetime, start=data.start, end=data.end
        )

    return data
