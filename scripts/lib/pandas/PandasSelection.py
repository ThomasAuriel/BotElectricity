from __future__ import annotations

from datetime import datetime
from typing import Any, Callable, Dict, List, Union

import numpy
from _collections_abc import Iterable
from data import Index
from data.Data import GenericData
from lib.pandas import PandasIndexes

import pandas as pandas
from pandas import DataFrame, Series


# def getColumn(data: GenericData, columnNames: Union[str, Unit]) -> GenericData:
#     """
#     Get the columns of the data
#     """

#     if isinstance(columnNames, Unit):
#         data.dataframe = data.dataframe.loc[
#             :,
#             [column for column in data.columns if columnNames.symbol.lower() in column.lower()][0],
#         ]
#         # return data.loc[:, columns.symbol.lower()]
#     else:
#         data.dataframe = data.dataframe.loc[:, columnNames]

#     return data


def select(
    data: GenericData,
    parameter: Union[List[str], str],
    value: Union[List[str], str],
    operator: str = "or",
) -> GenericData:
    """
    Select data following the parameter and the value provided.
    If parameters and values are two list, a zip of the two is done.

    data:
        data to filter
    parameter: Union[List[str], str]
        list of the parameter to use (index name)
    values: Union[List[str], str]
        list of the values to use.
    operator: str
        The operator function to associate the diferents parameters
    """

    data.dataframe = selectDataframe(
        df=data.dataframe,
        parameter=parameter,
        value=value,
        operator=operator,
    )
    return data


def selectDataframe(
    df: DataFrame,
    parameter: Union[List[str], str],
    value: Union[List[str], str],
    operator: str = "or",
) -> DataFrame:
    """
    Select data following the parameter and the value provided.
    If parameters and values are two list, a zip of the two is done.

    df: Dataframe
        data to filter
    parameter: Union[List[str], str]
        list of the parameter to use (index name)
    values: Union[List[str], str]
        list of the values to use.
    operator: str
        The operator function to associate the diferents parameters
    """

    try:
        if isinstance(df, Series):
            df = df.to_frame()

        queryString = ""
        if (
            isinstance(parameter, Iterable)
            and not isinstance(parameter, str)
            and isinstance(value, Iterable)
            and not isinstance(value, str)
        ):
            zipValues = list(zip(parameter, value))
            queryString = (" " + operator + " ").join(
                [p + " == " + str(v) + "" for p, v in zipValues]
            )
        elif (
            isinstance(parameter, str)
            and isinstance(value, Iterable)
            and not isinstance(value, str)
        ):
            queryString = (" " + operator + " ").join(
                [str(parameter) + " == '" + str(v) + "'" for v in value]
            )
        else:
            queryString = str(parameter) + " == '" + str(value) + "'"

        return df.query(queryString)
    except:
        return df


def selectBetween(
    data: GenericData,
    parameter: str,
    start: Any,
    end: Any,
    includeStart: bool = True,
    includeEnd: bool = True,
):
    """
    Select data following the parameter and the value provided

    data:
        data to filter
    country:
        country to select
    """

    queryString = ""
    if includeStart:
        queryString += str(parameter) + ' >= "' + str(start) + '"'
    else:
        queryString += str(parameter) + ' > "' + str(start) + '"'

    queryString += " and "

    if includeEnd:
        queryString += str(parameter) + ' <= "' + str(end) + '"'
    else:
        queryString += str(parameter) + ' < "' + str(end) + '"'

    data.dataframe = data.dataframe.query(queryString)

    return data


def selectCountry(data: GenericData, country: List[str]) -> GenericData:
    """
    Select only the country

    data:
        data to filter
    country:
        country to select
    """

    data = select(data, Index.country, country)
    return data


def selectCategory(data: GenericData, category: List[str]) -> GenericData:
    """
    Select only the category

    data:
        data to filter
    category:
        category to select
    """

    data = select(data, Index.category, category)
    return data


# def selectProduction(data: GenericData, production: List[str]) -> GenericData:
#     """
#     Select only the production

#     data:
#         data to filter
#     category:
#         production to select
#     """

#     data = select(data, Index.production, production)
#     return data


def __naiveDatetime(date: Union[str, datetime]) -> DataFrame:

    if isinstance(date, str):
        date = pandas.to_datetime(date, box=True)

    if date.tzinfo is None:
        date.tz_localize("Europe/Brussels")

    return date


def selectDatetime(data: GenericData, date: datetime) -> DataFrame:
    """
    Select only the datetime

    data:
        data to filter
    date:
        date to select
    """

    datetime = __naiveDatetime(date)

    data = select(data, Index.datetime, str(datetime))
    return data


def selectBetweenDatetime(
    data: GenericData,
    datetimeStart: datetime,
    datetimeEnd: datetime,
    includeStart: bool = True,
    includeEnd: bool = True,
) -> GenericData:
    # TODO
    """
    Select only the datetime

    data:
        data to filter
    category:
        datetime to select
    """

    datetimeStart = __naiveDatetime(datetimeStart)
    datetimeEnd = __naiveDatetime(datetimeEnd)

    data = selectBetween(
        data,
        Index.datetime,
        str(datetimeStart),
        str(datetimeEnd),
        includeStart,
        includeEnd,
    )
    return data


# def dropEmptyIndex(data: DataFrame, level: List[str], verbose: bool = False, **kwargs) -> DataFrame:

#     dataGroup = data.groupby(level=level)

#     toReturn = []
#     for key, group in dataGroup:

#         if group.any():

#             if verbose:
#                 print("Keep : " + str(key))

#             toReturn.append(group)

#     toReturn = pandas.concat(toReturn)
#     toReturn.name = data.name

#     return toReturn


def dropIndex(
    data: DataFrame, levelValues: List[str], levelNames: str, verbose: bool = False, **kwargs
) -> DataFrame:
    # TODO
    """
    Drop row matching all the designated levels
    """

    data = data.drop(levelValues, level=levelNames)
    return data


def getCommonCountries(*datas: List[DataFrame]):
    # TODO

    countries = None
    for idx in range(len(datas)):
        if countries is None:
            countries = PandasIndexes.getIndexeUniqueValues(datas[0], Index.country)
        else:
            countries = countries.intersection(
                other=PandasIndexes.getIndexeUniqueValues(datas[idx], Index.country),
                sort=False,
            )

    return countries


def selectCommonCountries(*datas: List[DataFrame]):
    # TODO

    countries = getCommonCountries(*datas)
    return tuple([selectCountry(data, countries) for data in datas])


def convertCategory(
    data: DataFrame, categories: Dict[str, str], groupingOperation: Callable = numpy.sum
) -> DataFrame:
    """Convert catgories to another one based on the dict provided

    Parameter
    ---------
    data: DataFrame
        DataFrame to convert
    categories: Dict[str, str]
        Categories convertion to use

    Return
    ------
    DataFrame"""

    columns = list(data.index.names)

    data = data.reset_index()
    data[Index.category + "_tmp"] = data[Index.category].map(categories)
    data = data.set_index([*columns, Index.category + "_tmp"])

    columns.remove(Index.category)
    data = data.groupby([*columns, Index.category + "_tmp"]).agg(groupingOperation)
    data.index.names = [*columns, Index.category]

    return data
