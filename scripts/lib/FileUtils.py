import collections
import os
import pickle
import re
import zipfile
from os import path, walk
from pathlib import Path
from typing import Any, List, Optional, Union


def exists(file: str) -> bool:
    """
    Check if a file or a folder exists

    Parameter
    ---------
    file: str
        Path file or path folder to check

    Return
    ------
    Boolean to indicate if the file or folder exist
    """
    return path.exists(file)


def createFolder(folder: str) -> None:
    """
    Create a folder if it does not exists

    Parameter
    ---------
    folder: str
        Path of the folder to create
    """
    if not exists(folder):
        os.makedirs(folder, exist_ok=True)


def listFiles(folderPath: str, recursive: bool = False, extension: List[str] = None) -> List[str]:
    """
    Provide the list of file contained in a folder.

    Parameter
    ---------
    folderPath: str
        Path folder where the search shall start
    recursive: bool, optional
        boolean indicating if the search shall include subfolder
    extension: List[str], str, optional
        file with the specific extentions to keep (does not include the point of the extension (for instance, text file are defined as 'txt'

    Return
    ---------
    files: List[str]
        List of files with their complet path
    """

    # return [f for f in listdir(folderPath) if isfile(join(folderPath, f))]

    files = []
    for (dirpath, dirnames, filenames) in walk(top=folderPath):
        files.extend([os.sep.join([dirpath, filename]) for filename in filenames])
        if not recursive:
            break

    if extension is not None:

        # Turn extension to str
        if isinstance(extension, collections.Iterable) and not isinstance(extension, str):
            extension = "$|".join(extension) + "$"
            files = [f for f in files if re.search(extension, f)]
        else:
            files = [f for f in files if f.endswith(extension)]

    return files


def getBasename(filepath: str) -> str:
    """
    Get the filename of a file pointed by the provided filepath

    Parameter
    ---------
    filepath: str
        Filepath from which the filename shall be extracted

    Return
    ------
    Return the filename
    """

    return path.basename(filepath)


def getFileExtension(filepath: str) -> str:
    """
    Get the extension of the file

    Parameter
    ---------
    filepath: str
        Filepath from the extension is to be determined

    Return
    ------
    Return the filename and the extension
    """

    name = getBasename(filepath)
    return os.path.splitext(name)


def getParent(filename: str) -> Path:
    """
    Get the parent directory path of a file

    Parameter
    ---------
    filename: str
        Filename of the file

    Return
    ------
    parent folder path
    """

    path = Path(filename)
    return path.parent


def getParentString(filename: str) -> str:
    """
    Get the parent directory path of a file as a string path

    Parameter
    ---------
    filename: str
        Filename of the file

    Return
    ------
    parent folder path as a string
    """

    return os.sep.join(getParent(filename).parts)


## Access to file
def readFile(filepath: str, verbose: bool = True) -> str:
    """
    Read a file content as a string

    Parameter
    ---------
    filepath: str
        Filepath of the file to read
    verbose: bool

    Return
    ------
    Content of the file
    """

    f = open(filepath, "r")
    toReturn = f.read()
    f.close()

    if verbose:
        print("File " + filepath + " succefully wrote.")

    return toReturn


def writeFile(filepath: str, fileContent: str, verbose: bool = True):
    """
    Write a string in a file

    Parameter
    ---------
    filepath: str
        Filepath of the file to write
    fileContent: str
        String to write in the file
    verbose: bool
    """

    f = open(filepath, "w")
    f.write(fileContent)
    f.close()

    if verbose:
        print("File " + filepath + " succefully wrote.")


def deleteFile(filepath: Union[str, List[str]], verbose: bool = True):
    """
    Delete a file

    Parameter
    ---------
    filepath: str
        File path of the file to delete
    verbose: bool
    """

    if isinstance(filepath, str):
        os.remove(filepath)
        if verbose:
            print("File " + filepath + " succefully deleted.")
    elif isinstance(filepath, collections.Iterable):
        for f in filepath:
            deleteFile(f, verbose=verbose)
    else:
        if verbose:
            print("No file to delete for: " + str(filepath))


## Zip files
def uncompressFile(
    filepath: Union[str, List[str]],
    folderTarget: Optional[str] = None,
    filenameTarget: Optional[str] = None,
    verbose: bool = True,
) -> List[str]:
    """
    Uncompress a zip file or a list of zip file

    Parameter
    ---------
    filepath: str, List[str]
        Filepath of the zip file to unzip
    folderTarget: str, optional
        Folder where to unzip the file. If none, then unzip in the same folder than the zip file
    filenameTarget: Optional[str]
        If provided, the file in the zip are renamed with this name
    verbose: bool

    Return
    ------
    Return the list of file extracted
    """

    # If unique file
    if isinstance(filepath, str):
        filepath = [filepath]  # Turn it a as a list

    extractedFiles = []
    for f in filepath:
        if not zipfile.is_zipfile(f):
            if verbose:
                print(f + " is not a zip file.")
            continue

        if not folderTarget:
            folderTarget = getParent(f)

        with zipfile.ZipFile(f, "r") as zip_ref:

            zipinfo = zip_ref.infolist()
            if filenameTarget:
                for index, filenameInZip in enumerate(zipinfo):
                    if len(zipinfo) > 2:
                        filenameInZip.filename = (
                            getFileExtension(getBasename(filenameTarget))[0]
                            + "_"
                            + str(index)
                            + getFileExtension(filenameInZip.filename)[1]
                        )
                    else:
                        filenameInZip.filename = (
                            getFileExtension(getBasename(filenameTarget))[0]
                            + getFileExtension(filenameInZip.filename)[1]
                        )
            # zip_ref.extractall(folderTarget)
            zip_ref.extractall(path=folderTarget, members=zipinfo)
            extractedFiles.extend(
                [folderTarget + os.sep + newFile for newFile in zip_ref.namelist()]
            )

        if verbose:
            print("File " + str(f) + " unzipped in " + str(folderTarget))

    return extractedFiles


# Store object
def parse(data: Any, filename: str):
    """
    Parse an object to the file defined by the filename.

    Parameter
    ---------
    data: Any
        Data to store
    filename: str
        Filepath where to store the data
    """

    with open(filename, "wb") as file:
        pickle.dump(data, file)
    # with open(filename, "w") as file:
    #     frozen = jsonpickle.encode(data, indent=4)
    #     print(frozen, file=file)


def unparse(filename: str) -> object:
    """
    Unparse an object to the file defined by the filename.

    Parameter
    ---------
    data: Any
        Data to store
    filename: str
        Filepath where to store the data

    Return
    ------
    Object unparsed from the file (or None if the file does not exist)
    """

    with open(filename, "rb") as file:
        data = pickle.load(file)
        # data = jsonpickle.decode(file.read())
        return data

    return None
