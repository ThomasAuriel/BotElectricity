from enum import Enum
from typing import Any, Dict, List, Optional, Tuple, Union

import cartopy
import numpy
from data import Country

import matplotlib
from matplotlib import pyplot
from matplotlib.figure import Figure
from matplotlib.pyplot import subplot

matplotlib.use("agg")

defaultSource = "ENTSO-E, IPCC 2014"


class Projection(Enum):

    projection_2D = "2D"
    projection_3D = "3D"
    projection_map = "Map"


def getFigure(
    size: Tuple[int, int],
    dpi: int = 360,
    font={"family": "normal", "size": 22},
    author: Optional[str] = "@BotElectricity v0.8",
    source: Optional[str] = defaultSource,
    **kwargs
):
    # TODO

    matplotlib.rc("font", **font)

    fig = pyplot.figure(figsize=size, dpi=dpi)
    fig.text(
        1.00 - 0.10 / size[0],
        1.00 - 0.10 / size[1],
        author + "\nSource: " + source,
        horizontalalignment="right",
        verticalalignment="top",
        fontdict=font,
    )

    artists: List[Any] = []

    return fig, artists


def addSubplot(
    fig: Figure,
    artists: List,
    type: Projection = Projection.projection_2D,
    nrows: int = 1,
    ncols: int = 1,
    index: int = 1,
    subplot_kwargs={"aspect": "auto"},
    lefttext: Optional[str] = None,
    righttext: Optional[str] = None,
    projection: Optional[cartopy.crs.Projection] = cartopy.crs.PlateCarree(),
    extents=[-13, 44, 34, 70],  # Europe
    color: str = "#FFFFFF",
    showGrid: bool = True,
    mapAx: Optional[Dict] = None,
    **kwargs
):
    # TODO

    if mapAx:
        try:
            ax = mapAx[str(nrows) + str(ncols) + str(index)]
        except:
            ax = None
    else:
        ax = None

    if ax is None:
        if type == Projection.projection_2D:
            ax = fig.add_subplot(nrows, ncols, index)

            # Horizontal grid
            if showGrid:
                ax.yaxis.grid(True)

        elif type == Projection.projection_3D:
            ax = fig.add_subplot(nrows, ncols, index, projection="3d")

        elif type == Projection.projection_map:
            extents = [-13, 44, 34, 70]  # Europe
            ax = fig.add_subplot(
                nrows,
                ncols,
                index,
                projection=projection,
            )
            if extents:
                ax.set_extent(extents=extents, crs=projection)

            # Decoration
            ax.coastlines(resolution="10m")
            ax.add_feature(cartopy.feature.BORDERS, linestyle=":", alpha=1, zorder=10)
            # ax.add_feature(cartopy.feature.OCEAN, facecolor=("lightblue"))
            # ax.add_feature(cartopy.feature.LAND)

        ax.set_facecolor(color)

        if lefttext:
            a = ax.text(
                0.00,
                1.00,
                lefttext,
                horizontalalignment="left",
                verticalalignment="bottom",
                transform=ax.transAxes,
            )
            artists.append(a)
        if righttext:
            a = ax.text(
                1.00,
                1.00,
                righttext,
                horizontalalignment="right",
                verticalalignment="bottom",
                transform=ax.transAxes,
            )
            artists.append(a)

    mapAx[str(nrows) + str(ncols) + str(index)] = ax

    return ax, artists, mapAx


def addLegend(ax):
    # TODO

    lgd = ax.legend(loc="upper left", bbox_to_anchor=(1, 1), ncol=1, markerscale=2)

    # Change country name in the legend
    for idx, text in enumerate(lgd.get_texts()):

        countryNames = Country.getAllCountriesName()

        if text.get_text() in countryNames:
            newText = "[" + text.get_text() + "] " + countryNames[text.get_text()]
            text.set_text(newText)
        # else:
        #     Else do nothing
        #     text.set_text(text.get_text())

    return lgd


def addLine(
    ax: subplot,
    a: Union[int, List[int]] = numpy.arange(0.1, 2, 0.2),
    b: Union[int, List[int]] = numpy.zeros(20),
    color: str = "dimgrey",
):
    """
    Add a specific line to the plot

    Parameter
    ---------
    ax: subplot
        Plot where the line should be added
    a, b: Union[int, List[int]]
        Coef to apply to create the wanted line : y = a*x+b
        If a list is provide, several lines are drawn.
        They must be the same size
    color: str
        Color of the line
    """

    if isinstance(a, int):
        a = [a]
    if isinstance(b, int):
        b = [b]

    x = numpy.linspace(*ax.get_xlim())

    for (a_coef, b_coef) in list(zip(a, b)):

        y = a_coef * x + b_coef
        ax.plot(x, y, color=color, linewidth=0.2)

        # ax.axline(
        #     (0, b_coef),
        #     slope=a_coef,
        #     color=color,
        #     linewidth=0.2,
        # )

        # ax.annotate(
        #     str(a_coef) + " * x + " + str(b_coef),
        #     xy=(x[1], y[1]),
        #     xytext=(6, 0),
        #     color=color,
        #     textcoords="offset points",
        #     size=6,
        #     va="center",
        # )


def setMinMax(
    ax: subplot,
    minimum: float,
    maximum: float,
):
    # TODO

    pad = numpy.max([numpy.abs(minimum), numpy.abs(maximum)]) * 0.1
    ax.set_ylim(minimum - pad, maximum + pad)
