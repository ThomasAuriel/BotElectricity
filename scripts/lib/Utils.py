import time
from typing import Callable, Dict


def timing(fn):
    """
    To use, use the wrapper:
    @timing
    """

    def wrapper(*args, **kwargs):
        start = time.time()
        ret = fn(**kwargs)
        end = time.time()
        print(f"The function {fn.__name__} tooks:\n- {end-start} to finish execution.")
        return ret

    return wrapper


import multiprocessing as mp


class Parallelization(object):
    """
    Asynchronous parallelization
    """

    # Documentation https://www.machinelearningplus.com/python/parallel-processing-python/

    def __init__(self, nbThread: int = mp.cpu_count(), asynchronous: bool = True) -> None:
        super().__init__()

        print("Create a pool with {} thread".format(nbThread))
        self.pool = mp.Pool(nbThread)
        self.asynchronous = asynchronous

    def addJob(self, func: Callable, kwargs: Dict = {}, callback: Callable = None):
        """
        Add a job to the pool

        Parameters
        ----------
        self
        func:
            Function to perform
        kwargs:
            Arguments of the function
        """
        if self.asynchronous:
            self.pool.apply_async(func, kwds=kwargs, callback=callback)
        else:
            return self.pool.apply(func, kwds=kwargs)

    def execute(self):
        """
        Execute the jobs and wait for the end
        """

        if self.asynchronous:
            self.pool.close()
            self.pool.join()
        else:
            self.pool.close()
