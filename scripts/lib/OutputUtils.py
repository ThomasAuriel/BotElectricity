import sys


def printFunction(fn):
    # TODO
    """
    To use, use the wrapper:
    @printFunction
    """

    def wrapper(*args, **kwargs):
        Console.print("Start function " + fn.__name__)
        ret = fn(*args)
        Console.print("End function " + fn.__name__)
        return ret

    return wrapper


class Console:
    @staticmethod
    def print(message: str):
        sys.stdout.flush()
        sys.stdout.write("\r" + message)
