import Key
import tweepy


def authentification():
    # TODO

    auth = tweepy.OAuthHandler(Key.consumer_key, Key.consumer_secret)
    auth.set_access_token(Key.access_token, Key.access_token_secret)
    api = tweepy.API(auth)

    return api


def postMessage(
    message,
    api=None,
    previousStatus=None,
    verbose=False,
):
    # TODO
    """
    Post a message on twitter
    @param message: Message to post
                    Structure of the message:
                    message = {'text': text, 'media': [filepaths]}
    @param api: API to use to post the message

    @return: return the last posted status and the api
    """

    return None, None

    # Twitter
    if not api:
        api = authentification()

    media_ids = []
    if "media" in message.keys():
        for filename in message["media"]:
            res = api.media_upload(filename)
            media_ids.append(res.media_id)

    try:
        if not previousStatus:
            status = api.update_status(status=message["text"], media_ids=media_ids)
        else:
            status = api.update_status(
                status=message["text"],
                media_ids=media_ids,
                in_reply_to_status_id=previousStatus.id,
            )
    except Exception as err:
        print(err)

    if verbose:
        print("Posted " + message["text"])

    return status, api
