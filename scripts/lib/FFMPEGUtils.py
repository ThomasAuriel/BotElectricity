import os
from typing import Optional, Union


def createVideo(
    parentFolder: str,
    mp4Filepath: str = "./00-output.mp4",
    videoSpeed: Union[int, str] = 24,
    resolution: Optional[str] = "1920x1080",
    interpolation: Optional[Union[int, str]] = None,
    verbose: bool = False,
    **kwargs
):
    """Create a video from all the PNG stored in a folder.

    Parameter
    ---------
    parentFolder: str
        - Folder where the images to use are stored. All the images in the folder are used for the video.
    mp4Filepath: str
        - File where the video is stored
    videoSpeed: Union[int, str]
        - Speed of the video (in fps)
    interpolation: Union[int, str]
        - Use the interpolation method to simulate of frame rate equale to the interpolation value
    verbose: bool

    Return
    ------
    mp4Filepath: str
        File path where the video is stored
    """
    parameters = []
    parameters.append("-y")
    parameters.append("-pattern_type glob -i '*.png'")
    # parameters.append("-vcodec libx264")
    # parameters.append("-vf 'scale=640:trunc(ow/a/2)*2'")
    # parameters.append("-acodec aac")
    # parameters.append("-vb 1024k")
    # parameters.append("-minrate 1024k")
    # parameters.append("-maxrate 1024k")
    # parameters.append("-bufsize 1024k")
    # parameters.append("-ar 44100")
    # parameters.append("-strict experimental")
    parameters.append("-r " + str(videoSpeed))
    if resolution:
        parameters.append("-s " + resolution)
    if interpolation:
        parameters.append("-filter \"minterpolate='fps=" + str(interpolation) + "'\"")
    parameters.append("'" + mp4Filepath + "'")
    cmdLine = "cd '" + parentFolder + "' && " + "ffmpeg " + " ".join(parameters)

    if verbose:
        print("Start to create the video file")
        print(cmdLine)

    os.system(cmdLine)

    if verbose:
        print("Video file created")

    return mp4Filepath
