from typing import Dict


class FormatDict(dict):
    def __missing__(self, key):
        return "{" + key + "}"


def format_map(str: str, values: Dict):
    # TODOs
    return str.format_map(FormatDict(**values))
