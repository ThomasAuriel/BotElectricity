from datetime import datetime

import matplotlib.dates as mdates
import pandas

from data import Country, DataPower, DataPower_ENTSOE, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import HistogramChart

# Dates
start = pandas.Timestamp(datetime(2021, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2021, 2, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
listCountry = [Country.FR, Country.DE, Country.DK, Country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)
generation.downloader = DataPower_ENTSOE.Generation_ENTSOE()

# generation.download()

# Linear figures
figure = Figure(
    filename="./output/test/test_figure/test_histogram/test_histogram_01.png",
    data=[generation],
    title="Generation",
    size=(35, 14),
)
s1 = Subplot(
    functionPlot=HistogramChart.histogram,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=HistogramChart.histogram,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=2,
    plotArgs={"bins": 1000},
)
s3 = Subplot(
    functionPlot=HistogramChart.histogram,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.category,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=3,
)
s4 = Subplot(
    functionPlot=HistogramChart.histogram,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.category,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=4,
    plotArgs={"bins": 1000},
)
figure.addSubplots(subplots=[s1, s2, s3, s4])
figure.plot(plotIfExist=True)

print("end")
