from __future__ import annotations

import warnings

from lib.pandas import PandasSettings

warnings.simplefilter(action="ignore", category=FutureWarning)
