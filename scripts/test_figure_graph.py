from datetime import datetime

import pandas

from data import Country, DataPower, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import GraphChart

# Dates
start = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 2, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
# listCountry = [country.DE, country.DK, country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

# balance = DataPower.Import(
#     start=start, end=end
# )
# balance.downloader = DataPower_ENTSOE.Import_ENTSOE()
# balance.download()
# balance = DataPower.Export(
#     start=start, end=end
# )
# balance.downloader = DataPower_ENTSOE.Export_ENTSOE()
# balance.download()

balance = DataPower.Balance(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)

# Box figures
figure = Figure(
    filename="./output/test/test_figure/test_graph/test_graph_01.png",
    data=[balance],
    title="Balance",
    size=(16, 16),
)
s1 = Subplot(
    functionPlot=GraphChart.plot,
    abscisseIndex=Index.category,
    categoryIndex=Index.country,
    nrows=1,
    ncols=1,
    index=1,
    showGrid=False,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)


print("end")
