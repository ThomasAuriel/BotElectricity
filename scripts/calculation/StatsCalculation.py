from ctypes import Union
from typing import List, Union

import numpy
from data import Index
from data.Data import GenericData


def getPercentile(
    data: GenericData,
    levels: Union[str, List[str], None] = [Index.country, Index.category],
    percentil: float = 50,
) -> GenericData:
    """
    Get the value corresponding to the asked percentile.

    Parameter
    ---------
    data:
        Data to use for the calculation
    level: Union[str, List[str], None]
        Level to use to compute the std
        By default, levels is set to [index.country, index.category]
    percentils: float
        Percentile to use

    Return
    ------
    GenericData:
        The percentile value
        Same unit as the provided data
    """

    percentil_df = data.copy()
    percentil_df.name = percentil_df.name + " - [" + str(percentil) + "] percentils"
    percentil_df.sqlTable = None

    percentil_df.dataframe = percentil_df.dataframe.groupby(level=levels).apply(
        lambda x: numpy.percentile(a=x, q=percentil)
    )
    return percentil_df


def getMin(
    data: GenericData, levels: Union[str, List[str], None] = [Index.country, Index.category]
) -> GenericData:
    """
    Get the min of dataframe

    Parameter
    ---------
    data:
        Data to use for the calculation
    level: Union[str, List[str], None]
        Levels used to compute the minimum value
        By default, levels is set to [index.country, index.category]

    Return
    ------
    GenericData:
        The minimum value
        Same unit as the provided data
    """

    minValue = data.copy()
    minValue.name = minValue.name + " - Minimum value"
    minValue.sqlTable = None

    minValue.dataframe = minValue.dataframe.groupby(level=levels).apply(lambda x: numpy.min(a=x))
    return minValue


def getMax(
    data: GenericData, levels: Union[str, List[str], None] = [Index.country, Index.category]
) -> GenericData:
    """
    Get the max of dataframe

    Parameter
    ---------
    data:
        Data to use for the calculation
    level: Union[str, List[str], None]
        Levels used to compute the maximum value
        By default, levels is set to [index.country, index.category]

    Return
    ------
    GenericData:
        The maximum value
        Same unit as the provided data
    """

    maxValue = data.copy()
    maxValue.name = maxValue.name + " - Maximum value"
    maxValue.sqlTable = None

    maxValue.dataframe = maxValue.dataframe.groupby(level=levels).apply(lambda x: numpy.max(a=x))
    return maxValue


def getStd(
    data: GenericData, levels: Union[str, List[str], None] = [Index.country, Index.category]
) -> GenericData:
    """
    Compute the Standard Deviation of the generic data

    Parameter
    ---------
    data: GenericData
        Data to use for the calculation
    level: Union[str, List[str], None]
        Levels used to compute the maximum value
        By default, levels is set to [index.country, index.category]

    Return
    ------
    GenericData:
        The std value
        Same unit as the provided data
    """

    stdValue = data.copy()
    stdValue.name = stdValue.name + " - Standard deviation value"
    stdValue.sqlTable = None

    stdValue.dataframe = stdValue.dataframe.groupby(level=levels).apply(lambda x: numpy.std(a=x))
    return stdValue
