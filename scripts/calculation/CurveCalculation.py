from typing import List, Tuple, Union, Optional
import numpy

import pandas as pandas
from data import Index, Mix, Units
from data.Data import GenericData
from lib.pandas import PandasIndexes, PandasSelection
from pandas import DataFrame
import numpy


def getDurationCurve(
    data: Union[DataFrame, GenericData], ascending: bool = False
) -> Union[DataFrame, GenericData]:
    """
    Define the duration curve of data (data are sorted for the biggest to the lower)

    Parameter
    ---------
    data: Union[DataFrame, GenericData]
        data to sort
    ascending: bool = False
        Order of the sorting

    Return
    ------
    Sorted DataFrame or GeneraticData
    """

    # Sort values
    if type(data) is DataFrame:
        data = data.sort_values(by=["data"], ascending=ascending)
        data["index"] = numpy.arange(len(data))
        data = data.set_index(keys="index", append=True)
        return data
    elif issubclass(type(data), GenericData):
        data.dataframe = getDurationCurve(data.dataframe, ascending=ascending)
        data.sqlTable = None
        data.getFromDatabase = None
        data.downloadFunction = None

        return data


def getEffectiveCapacityFactor(
    data: GenericData, dataRef: Optional[GenericData], level: str
) -> GenericData:
    """
    Compute the effective Capacity Factor
    This is a load factor using the max production value as installed capacity

    Parameter
    ---------
    data: GenericData
        Data to use to compute the effective capacity factor
    dataRef: Optional[GenericData]
        data used for reference. If None, then the data parameter is used.
    level: str
        level used as axis for the computation

    Return
    ------
    GenericData containing the result
    """

    if dataRef is None:
        dataRef = data.copy()
    dataRef = dataRef.copy().convert(data.unit)

    effectiveCapacityFactor = data.copy()
    effectiveCapacityFactor.name = "Effective capacity factor"
    effectiveCapacityFactor.unit = Units.Ratio
    effectiveCapacityFactor.sqlTable = None

    ratio = data.dataframe / dataRef.dataframe.groupby(level).max()
    effectiveCapacityFactor.dataframe = ratio.to_frame().fillna(0)

    return effectiveCapacityFactor


def getGenerationShare(
    generation: GenericData,
    totalGeneration: Optional[GenericData] = None,
    levels: List[str] = [Index.category, Index.datetime],
    totalLevels: List[str] = [Index.datetime],
) -> GenericData:
    """
    Compute generation share over total generation

    Parameter
    ---------
    generation: GenericData
        Generation used for the calculation
    totalGeneration: Optional[GenericData]
        Total generation use as reference for the calculus.
        If the parameter is None, then generation is used as reference (using sum levels)
    levels: List[str]
        Levels parameters is used to aggregate levels of generation.
        If none, this step is not used the factor is computed for each [index.category, index.datetime]
    totalLevels: List[str]
        Levels parameters is used to aggregate levels of totalGeneration
        If none, this step is not used the factor is computed for each [index.datetime]

    Return
    ------
    GenericData containing the result
    """

    generationShare = generation.copy()
    generationShare.name = "Share"
    generationShare.unit = Units.Ratio
    generationShare.sqlTable = None

    if totalGeneration is None:
        totalGeneration = generation

    generation = generation.dataframe.copy()
    generation = generation.groupby(by=levels).sum()
    totalGeneration = totalGeneration.dataframe.copy()
    totalGeneration = totalGeneration.groupby(by=totalLevels).sum()

    generationShare.dataframe = generation / totalGeneration

    return generationShare


def getPenetrationFactor(
    generation: GenericData, load: GenericData, levels: Optional[List[str]] = None
) -> GenericData:
    """
    Compute the penetration factor (how much load is covered by the generation)

    Parameter
    ---------
    generation: GenericData
        Generation used for the calculation
    load: GenericData
        Load used for the calculation
    levels: Optional[List[str]]
        Levels parameters is used to aggregate levels for the calculation
        If none, this step is not used the factor is computed for each index.category

    Return
    ------
    GenericData containing the result
    """

    penetrationFactor = generation.copy()
    penetrationFactor.name = "Penetration factor"
    penetrationFactor.unit = Units.Ratio
    penetrationFactor.sqlTable = None

    generation = generation.dataframe.copy()
    load = load.dataframe.copy()

    if levels:
        df_generation = generation.groupby(level=[Index.country, Index.datetime]).sum()
        df_load = load.groupby(level=[Index.country, Index.datetime]).sum()
        penetrationFactor.dataframe = (df_generation / df_load).fillna(0).to_frame()
    else:
        tmp = penetrationFactor.dataframe

        def divide(x, y):
            return x.droplevel(level=Index.category) / y.droplevel(level=Index.category)

        penetrationFactor.dataframe = tmp.groupby(by=[Index.category]).apply(
            lambda x: divide(x, load)
        )

    return penetrationFactor


def getReverseDuckCurve(
    generation: DataFrame,
    balance: DataFrame,
    load: DataFrame,
    mixToKeep: Mix.Mix = Mix.ENTSOE_variable_renewable_energy,
    unit: Units.Unit = Units.MWh,
) -> Tuple[DataFrame, DataFrame]:
    """
    A define duck curve as the generation and balance on one hand and the load on another land. Data are grouped by time interval.
    The only the generation categories keep are defined in the mixToKeep list.

    Parameter
    ---------
    generation: DataFrame
        Generation used for the calculation
    balance: DataFrame
        Balance used for the calculation
    load: DataFrame
        Load used for the calculation
    mixToKeep: Mix.Mix
        Only the mix is kept in the generation data.
    unit: Units.Unit
        All the data are convert in this unit.

    Return
    ------
    Return the generation and balance concatenated and group by time interval and return the load group by time interval
    """

    indexValues = PandasIndexes.getIndexeValues(Index.category)
    indexValuesToRemove = [
        indexValue for indexValue in indexValues if indexValue not in mixToKeep.values
    ]

    return __defineDuckCurve(generation, balance, load, mixToRemove=indexValuesToRemove, unit=unit)


def getDuckCurve(
    generation: DataFrame,
    balance: DataFrame,
    load: DataFrame,
    mixToRemove: Optional[Mix.Mix] = None,
    unit: Units.Unit = Units.MWh,
) -> Tuple[DataFrame, DataFrame]:
    """
    A define duck curve as the generation and balance on one hand and the load on another land. Data are grouped by time interval.
    The mix defined in `mixToRemove` are removed from the generation data.

    Parameter
    ---------
    generation: DataFrame
        Generation used for the calculation
    balance: DataFrame
        Balance used for the calculation
    load: DataFrame
        Load used for the calculation
    mixToRemove: Optional[Mix.Mix]
        Mixes defined in the `mixToRemove` list are removed in the generation data.
    unit: Units.Unit
        All the data are convert in this unit.

    Return
    ------
    Return the generation and balance concatenated and group by time interval and return the load group by time interval
    """

    if mixToRemove:
        return __defineDuckCurve(
            generation, balance, load, mixToRemove=mixToRemove.values, unit=unit
        )
    else:
        return __defineDuckCurve(generation, balance, load, mixToRemove=None, unit=unit)


def __getTimeInterval(
    df: DataFrame, timeInterval: str = "hours", indexLevel: str = Index.category
) -> DataFrame:
    df = PandasIndexes.defineTimeInterval(df, timeInterval, indexLevel)
    df = df.mean(level=[Index.category, "timeInterval"])
    return df


def __defineDuckCurve(
    generation: GenericData,
    balance: GenericData,
    load: GenericData,
    mixToRemove: Optional[List[str]],
    unit: Units.Unit = Units.MWh,
) -> Tuple[DataFrame, DataFrame]:
    """
    A define duck curve as the generation and balance on one hand and the load on another land. Data are grouped by time interval.
    The categories defined in `mixToRemove` are removed from the generation data.

    Parameter
    ---------
    generation: DataFrame
        Generation used for the calculation
    balance: DataFrame
        Balance used for the calculation
    load: DataFrame
        Load used for the calculation
    mixToRemove: Optional[Mix.Mix]
        Categories defined in the `mixToRemove` list are removed in the generation data.
    unit: Units.Unit
        All the data are convert in this unit.

    Return
    ------
    Return the generation and balance concatenated and group by time interval and return the load group by time interval
    """

    generation = generation.copy()
    balance = balance.copy()
    load = load.copy()

    # Convert
    generation.convert(unit=unit)
    balance.convert(unit=unit)
    load.convert(unit=unit)

    # Remove defined categories
    if mixToRemove:
        generation.dataframe = PandasSelection.dropIndex(
            generation.dataframe, levelNames=Index.category, levelValues=mixToRemove
        )

    # Group balance
    balance.dataframe = balance.dataframe.groupby(level=[Index.datetime, Index.country]).sum()
    balance.dataframe = pandas.concat([balance.dataframe], keys=["Balance"], names=[Index.category])

    # Concat data
    df = pandas.concat(
        [generation.dataframe.reset_index(), balance.dataframe.reset_index()]
    ).set_index([Index.country, Index.category, Index.datetime])
    dfLoad = pandas.concat([load.dataframe], keys=["Load"], names=[Index.category])

    # Define the interval
    df = __getTimeInterval(df, "hours", Index.category)
    dfLoad = __getTimeInterval(dfLoad, "hours", Index.category)

    return df, dfLoad
