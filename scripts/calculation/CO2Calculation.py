from typing import Dict, List, Optional, Tuple, Union
import numpy
from data import CO2IntensityDictionnary, Units
from data.Data import GenericData
from lib.pandas import PandasOperation, PandasSelection
from pandas import DataFrame, Series


def computeCO2Intensity(
    generation: GenericData,
    CO2Emission: Optional[GenericData] = None,
    CO2IntensityDict: CO2IntensityDictionnary.CO2IntensityCategories = CO2IntensityDictionnary.CO2Intensity_ENTSOE(),
    convertCategory: Union[Dict[str, str], None] = None,
    levels: Union[List[str], None] = None,
    verbose: bool = True,
    **kwargs,
) -> Tuple[DataFrame, Units.Unit]:
    """Compute the CO2 intensity based on the CO2IntensityDict

    Parameter
    ---------
    generation: generation
        generation representing the power (in Wh) to use for the calculation
    CO2IntensityDict: Dict[str, float]
        The dictionnary to convert categories to CO2 intensity
    convertCategory: Union[List[str], None]
        Dictionnary used to convert categories to another (for instance ENTSO-E to IPCC)
    levels: Union[List[str], None]
        Array to group levels

    Return
    ------
    Return dataframe containing the CO2 intensity and its unit
    """

    if verbose:
        print("Compute the CO2 intensity")

    if levels == None:

        if verbose:
            print("Define CO2 intensity values directly from the dictionnary")

        df = generation.dataframe
        CO2Int = CO2IntensityDict.reindexToMatch(df)
        CO2Unit = CO2IntensityDict.unit

        # Convert categories if required
        if convertCategory:
            if verbose:
                print("Convert categories")
            CO2Int = PandasSelection.convertCategory(data=CO2Int, categories=convertCategory)
    else:

        if verbose:
            print("Define CO2 intensity values from the CO2 Emissions")

        if CO2Emission is None:
            if verbose:
                print("Get CO2 Emission values")

            df_CO2Emission, CO2EmiUnit = computeCO2Emission(generation=generation)
            CO2Emission = GenericData()
            CO2Emission.dataframe = df_CO2Emission
            CO2Emission.unit = CO2EmiUnit
            CO2Emission = CO2Emission.convert(Units.gCO2eq)
        else:
            if verbose:
                print("Has already the CO2 Emission values")

            CO2Emission = CO2Emission.copy()
            CO2Emission = CO2Emission.convert(Units.gCO2eq)
            CO2Emission = CO2Emission.convert(Units.gCO2eq)

        if verbose:
            print("Compute the CO2 Intensity values")

        power = generation.copy()
        power = power.convert(Units.kWh)
        # Remove negative power:
        # They are due to negative energy production (storage of hydro pump).
        power.dataframe = power.dataframe.clip(lower=0)

        # Convert categories if required
        if convertCategory:
            if verbose:
                print("Convert categories")
            power.dataframe = PandasSelection.convertCategory(
                data=power.dataframe, categories=convertCategory
            )
            CO2Emission.dataframe = PandasSelection.convertCategory(
                data=CO2Emission.dataframe, categories=convertCategory
            )

        power = power.dataframe.groupby(by=levels).sum()
        CO2Emission = CO2Emission.dataframe.groupby(by=levels).sum()
        CO2Int = CO2Emission / power
        if type(CO2Int) is Series:
            CO2Int = CO2Int.to_frame()
        CO2Int = CO2Int.replace([numpy.inf, -numpy.inf], numpy.nan).fillna(0)

        CO2Unit = Units.gCO2eq_kWh

    # Remove negative emissions:
    # They are due to negative energy production (storage of hydro pump). The emissions are considered when they are producing.
    CO2Int = CO2Int.clip(lower=0)

    return CO2Int, CO2Unit


def computeCO2Emission(
    generation: GenericData,
    CO2Intensity: Optional[GenericData] = None,
    CO2IntensityDict: CO2IntensityDictionnary.CO2IntensityCategories = CO2IntensityDictionnary.CO2Intensity_ENTSOE(),
    convertCategory: Union[Dict[str, str], None] = None,
    levels: Union[List[str], None] = None,
    verbose: bool = True,
    **kwargs,
) -> DataFrame:
    """Compute the CO2 Emissions based on the CO2IntensityDict

    Parameter
    ---------
    generation: GenericData
        generation representing the power (in Wh) to use for the calculation
    CO2IntensityDict: Dict[str, float]
        The dictionnary to convert categories to CO2 intensity
    convertCategory: Union[List[str], None]
        Dictionnary used to convert categories to another (for instance ENTSO-E to IPCC)
    levels: Union[List[str], None]
        Array to group levels

    Return
    ------
    Return dataframe containing the CO2 emissions
    """

    if verbose:
        print("Compute the CO2 Emissions")

    # Convert to kWh for the calculation
    power = generation.copy()
    power = power.convert(Units.kWh)

    if CO2Intensity is None:

        if verbose:
            print("Get the CO2 intensity values associated to generation")

        CO2Intensity, unit = computeCO2Intensity(
            generation=power,
            CO2IntensityDict=CO2IntensityDict,
            verbose=verbose,
            **kwargs,
        )

        # Convert to the wanted unit
        if Units.gCO2eq_kWh.value != unit.value:
            CO2Intensity = CO2Intensity * Units.gCO2eq_kWh.value / unit.value
    else:
        if verbose:
            print("Has already the CO2 Intensity values")

        CO2Intensity = CO2Intensity.copy()
        CO2Intensity = CO2Intensity.convert(Units.gCO2eq_kWh)
        CO2Intensity = CO2Intensity.dataframe

    if verbose:
        print("Compute the CO2 Emission values")

    CO2Emission = CO2Intensity * power.dataframe
    CO2EmissionUnit = Units.gCO2eq
    if type(CO2Emission) is Series:
        CO2Emission = CO2Emission.to_frame()

    # Remove negative emissions:
    # They are due to negative energy production (storage of hydro pump). The emissions are considered when they are producing.
    CO2Emission = CO2Emission.clip(lower=0)

    # Convert categories if required
    if convertCategory:
        if verbose:
            print("Convert categories")
        CO2Emission = PandasSelection.convertCategory(data=CO2Emission, categories=convertCategory)

    if levels:
        CO2Emission = PandasOperation.sumOver(CO2Emission, levels=levels)

    return CO2Emission, CO2EmissionUnit
