import numpy
import pandas
import scipy.fft
from data import Index, Units
from data.Data import GenericData


def getIntermittency(data: GenericData, hours: int = 1) -> GenericData:
    """
    Intermittency computed with the method of Defining and Quantifying Intermittency in
    the Power Sector (Daniel Suchet, Adrien Jeantet, Thomas Elghozi and Zacharie Jehl).

    Changed made compared to the orginal article:
        P_max instead of P_mean
    """

    power = data.dataframe.copy()
    power = power.replace(to_replace=0, value=numpy.NaN)

    # Rolling average value
    # P_mean = (
    #     power.reset_index(level=[index.country, index.category])
    #     .groupby([index.country, index.category])
    #     .rolling(str(hours * 60) + "min")
    #     .mean()
    # )
    P_max = (
        power.reset_index(level=[Index.country, Index.category])
        .groupby([Index.country, Index.category])
        .rolling(str(hours * 60) + "min")
        .max()
    )

    # Select data freq-later
    P_DT = power.copy()
    indexName = P_DT.index.names
    P_DT = P_DT.reset_index()
    P_DT[Index.datetime] = P_DT[Index.datetime] + pandas.DateOffset(hours=hours)
    P_DT = P_DT.set_index(keys=indexName)

    P = power.copy()

    # I = (P_DT - P) / P_mean
    I = (P_DT - P) / P_max

    I = I.replace(to_replace=numpy.NaN, value=0)

    intermittency = GenericData(I)
    intermittency.name = "Intermitency {DT}h of {name}".format_map(
        {"DT": str(hours), "name": data.name}
    )
    intermittency.unit = Units.Ratio

    return intermittency


def getFFT(data: GenericData) -> GenericData:

    fft = scipy.fft.fft(data.dataframe.values, axis=0)

    fft = pandas.DataFrame(fft)
    fft.index.name = "freq"
    intermittency = GenericData(fft)
    intermittency.name = "FFT intermitency of {name}".format_map({"name": data.name})
    intermittency.unit = data.unit

    return intermittency
