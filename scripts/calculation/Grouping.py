from enum import Enum


class Grouping(Enum):
    sum = 0
    mean = 1
