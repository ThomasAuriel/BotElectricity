from datetime import datetime

import pandas

from data import Country, DataPower, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import ScatterPlot

# Dates
start = pandas.Timestamp(datetime(2021, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2021, 2, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
listCountry = [Country.FR, Country.DE, Country.DK, Country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)

# generation.downloader = DataPower_ENTSOE.Generation_ENTSOE()
# generation.download()

load = DataPower.Load(start=start, end=end, listCountry=listCountry, listCategory=listCategory)

# load.downloader = DataPower_ENTSOE.Load_ENTSOE()
# load.download()

# Linear figures
figure = Figure(
    filename="./output/test/test_figure/test_scatter/test_scatter_01.png",
    data=[generation, load],
    title="Generation vs Load",
)
s1 = Subplot(
    functionPlot=ScatterPlot.scatterchart,
    dataIndex=[0, 1],
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=None,
    minorLocatorInterval=None,
    majorFormatter=None,
    minorFormatter=None,
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=ScatterPlot.scatterchart,
    dataIndex=[0, 1],
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=None,
    minorLocatorInterval=None,
    majorFormatter=None,
    minorFormatter=None,
    nrows=2,
    ncols=2,
    index=2,
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

print("end")
