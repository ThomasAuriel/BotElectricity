from datetime import datetime

import matplotlib.dates as mdates
import pandas

from data import Country, DataPower, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import LinearPlot

# Dates
start = pandas.Timestamp(datetime(2021, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2021, 2, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
listCountry = [Country.FR, Country.DE, Country.DK, Country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)

# generation.downloader = DataPower_ENTSOE.Generation_ENTSOE()
# generation.download()

load = DataPower.Load(start=start, end=end, listCountry=listCountry, listCategory=listCategory)

# load.downloader = DataPower_ENTSOE.Load_ENTSOE()
# load.download()

# Linear figures
figure = Figure(
    filename="./output/test/test_figure/test_linear/test_linear_01.png",
    data=[generation],
    title="Generation",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=LinearPlot.stackplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=2,
)
s3 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.category,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=3,
)
s4 = Subplot(
    functionPlot=LinearPlot.stackplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.category,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=2,
    ncols=2,
    index=4,
)
figure.addSubplots(subplots=[s1, s2, s3, s4])
figure.plot(plotIfExist=True)


# Duck curves

# Dates
start = pandas.Timestamp(datetime(2021, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2021, 1, 2), tz="UTC")
# Countries
listCountry = [Country.FR]
# Categories
listCategory = None

generation = DataPower.GenerationBalance(start=start, end=end, listCountry=listCountry)

load = DataPower.Load(start=start, end=end, listCountry=listCountry)
# load.downloader = DataPower_ENTSOE.Load_ENTSOE()
# load.download()

figure = Figure(
    filename="./output/test/test_figure/test_linear/test_linear_02.png",
    data=[generation, load],
    title="Generation",
)
s1 = Subplot(
    functionPlot=LinearPlot.stackplot,
    dataIndex=[0],
    abscisseIndex=Index.datetime,
    categoryIndex=Index.category,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=1,
    ncols=1,
    index=1,
)
s2 = Subplot(
    functionPlot=LinearPlot.linearplot,
    dataIndex=[1],
    abscisseIndex=Index.datetime,
    categoryIndex=Index.category,
    majorLocatorInterval=mdates.MonthLocator(interval=1),
    nrows=1,
    ncols=1,
    index=1,
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

print("end")
