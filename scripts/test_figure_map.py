from datetime import datetime

import pandas

from data import Country, DataPower, DataPower_ENTSOE
from figure.Figure import Figure, Subplot
from figure.genericfigures import MapPlot
from lib.matplotlib.FigureUtils import Projection

# Dates
start = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 2, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
# listCountry = [country.DE, country.DK, country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)
generation.downloader = DataPower_ENTSOE.Generation_ENTSOE()

# generation.download()

# Box figures
figure = Figure(
    filename="./output/test/test_figure/test_map/test_map_01.png",
    data=[generation],
    title="Generation",
    size=(16, 16),
)
s1 = Subplot(
    functionPlot=MapPlot.plotMap,
    nrows=1,
    ncols=1,
    index=1,
    type=Projection.projection_map,
    showGrid=False,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)


print("end")
