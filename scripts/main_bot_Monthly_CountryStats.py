from datetime import datetime

import matplotlib.dates as mdates
import pandas as pandas
from dateutil.relativedelta import relativedelta

from data import DataCollector, Unit, country, index, mix
from data.Data import Generation
from figure import Figure_Periode
from lib.pandas import PandasSelection

if __name__ == "__main__":

    now = datetime.now().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    step = relativedelta(months=1)
    # step = relativedelta(days=1) # For tests
    start = pandas.Timestamp(now - step, tz="Europe/Brussels")
    end = pandas.Timestamp(now, tz="Europe/Brussels")

    listCountries = [
        country.BE,
        country.CH,
        country.DE,
        country.DK,
        country.ES,
        country.FR,
        country.GB,
        country.IT,
        country.NO,
        country.PL,
        country.PT,
        country.SE,
    ]

    listCountries = country.countries.values()

    kwargs = {
        "majorLocatorInterval": mdates.DayLocator(interval=1),
        "minorLocatorInterval": mdates.HourLocator(interval=1),
        "showTable": False,
    }

    # """
    # Get data
    # """
    # # Select data
    # generation = DataCollector.getData(
    #     data=Generation(), start=start, end=end, countries=listCountries
    # )
    # load = DataCollector.getData(data=Load(), start=start, end=end, countries=listCountries)
    # import_df = DataCollector.getData(data=Import(), start=start, end=end, countries=listCountries)
    # export_df = DataCollector.getData(data=Export(), start=start, end=end, countries=listCountries)
    # balance = Balance(import_df - export_df)

    # df_generationBalance = GenerationBalance(generation=generation, balance=balance)

    # generation.convert(unit=Unit.GWh)
    # load.convert(unit=Unit.GWh)
    # import_df.convert(unit=Unit.GWh)
    # export_df.convert(unit=Unit.GWh)
    # balance.convert(unit=Unit.GWh)
    # df_generationBalance.convert(unit=Unit.GWh)

    # generation.dataframe = generation.dataframe.mask(generation.dataframe > 100)
    # load.dataframe = load.dataframe.mask(load.dataframe > 100)
    # import_df.dataframe = import_df.dataframe.mask(import_df.dataframe > 100)
    # export_df.dataframe = export_df.dataframe.mask(export_df.dataframe > 100)
    # df_generationBalance.dataframe = df_generationBalance.dataframe.mask(df_generationBalance.dataframe > 100)

    # co2Intensity = CO2Intensity(generation=generation)
    # co2Intensity = co2Intensity.convert(unit=Unit.gCO2eq_kWh)
    # co2Emission = CO2Emission(generation=generation, CO2Intensity=co2Intensity)
    # co2Emission.convert(unit=Unit.tCO2eq)

    # """
    # Generate Figures
    # """
    # print("Generate Figures")

    # api = None
    # status = None

    # for c in listCountries:

    #     try:

    #         tmp_gb = PandasSelection.selectCountry(
    #             data=df_generationBalance.copy(), country=[c.iso]
    #         )
    #         tmp_load = PandasSelection.selectCountry(data=load.copy(), country=[c.iso])
    #         tmp_co2emission = PandasSelection.selectCountry(
    #             data=co2Emission.copy(), country=[c.iso]
    #         )

    #         # Plot
    #         filename_plot1 = "./output/bot_Monthly_CountryStat/{country}_Production_Linear.png".format_map(
    #             {"country": c.iso}
    #         )
    #         Figure_DuckCurve.plot(
    #             generationBalance=tmp_gb,
    #             load=tmp_load,
    #             filename=filename_plot1,
    #             start=start,
    #             end=end,
    #             timeInterval="hours",
    #             stack=True,
    #             title="Average production, balance and load of [{iso}] {country}".format_map(
    #                 {
    #                     "iso": c.iso,
    #                     "country": c.name,
    #                 }
    #             ),
    #         )

    #         filename_plot2 = "./output/bot_Monthly_CountryStat/{country}_Generation_Linear.png".format_map(
    #             {"country": c.iso}
    #         )
    #         Figure_Time.plot(
    #             data=tmp_gb,
    #             filename=filename_plot2,
    #             start=start,
    #             end=end,
    #             stack=True,
    #             title="Stacked electricity genration and balance of [{iso}] {country}".format_map(
    #                 {"iso": c.iso, "country": c.name}
    #             ),
    #             axisAbsica=index.datetime,
    #             axisOrdinate=index.category,
    #             **kwargs
    #         )

    #         # filename_plot3 = "./output/bot_Monthly_CountryStat/{country}_Load_Linear.png".format_map(
    #         #     {"country": c.iso}
    #         # )
    #         # Figure_Time.plot(
    #         #     data=tmp_load,
    #         #     filename=filename_plot3,
    #         #     start=start,
    #         #     end=end,
    #         #     stack=True,
    #         #     title="Stacked electricity genration and balance of [{iso}] {country}".format_map(
    #         #         {"iso": c.iso, "country": c.name}
    #         #     ),
    #         #     axisAbsica=index.datetime,
    #         #     axisOrdinate=index.category,
    #         #     **kwargs
    #         # )

    #         filename_plot4 = "./output/bot_Monthly_CountryStat/{country}_CO2Emission_Linear.png".format_map(
    #             {"country": c.iso}
    #         )
    #         Figure_Time.plot(
    #             data=tmp_co2emission,
    #             filename=filename_plot4,
    #             start=start,
    #             end=end,
    #             stack=True,
    #             title="Stacked electricity CO2 emissions of [{iso}] {country}".format_map(
    #                 {"iso": c.iso, "country": c.name}
    #             ),
    #             axisAbsica=index.datetime,
    #             axisOrdinate=index.category,
    #             **kwargs
    #         )

    #         # Post message
    #         print("Poste message for : " + str(c.name))
    #         message = {
    #             "text": "Stats for the country : [{iso}] {country}\nFrom ENTSO-E and IPCC 2014".format_map(
    #                 {"iso": c.iso, "country": c.name}
    #             ),
    #             "media": [
    #                 filename_plot1,
    #                 filename_plot2,
    #                 # filename_plot3,
    #                 filename_plot4,
    #             ],
    #         }
    #         status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

    #     except Exception as e:
    #         print(e)

    """
    Additional plots
    """

    ## Evolution over years
    start = pandas.Timestamp(datetime(2015, 1, 1), tz="Europe/Brussels")
    # end = end

    # Create message
    print("Post Message Tweet #2")
    # Generation
    generation_total = DataCollector.getData(data=Generation(), start=start, end=end)
    generation_total.convert(unit=Unit.TWh)

    # Create message
    print("Post Message Tweet #2")
    # Generation

    for c in listCountries:
        tmp_generation_total = PandasSelection.selectCountry(
            data=generation_total.copy(), country=[c.iso]
        )
        for m in [
            mix.ENTSOE_complete,
            mix.ENTSOE_wind,
            mix.ENTSOE_solar,
            mix.ENTSOE_nuclear_without_hydro,
            mix.ENTSOE_hydro,
            mix.ENTSOE_fossil,
        ]:
            tmp = tmp_generation_total.copy()
            tmp = PandasSelection.selectCategory(data=tmp, category=m.values)
            tmp.dataframe = tmp.dataframe.groupby(by=[index.category, index.datetime]).sum()
            tmp.dataframe = pandas.concat([tmp.dataframe], keys=[c.iso], names=[index.country])
            Figure_Periode.plot_cumulative(
                data=tmp,
                filename="./output/bot_Monthly_CountryStat/{country}_Generation_Cumulative_{mix}.png".format_map(
                    {"country": c.iso, "mix": m.name}
                ),
                start=start,
                end=end,
                title="Cumulative electricity generation for the {mix} in {country}".format_map(
                    {"country": c.iso, "mix": m.name}
                ),
                category=index.country,
                timeInterval=["years"],
                showTable=True,
            )

            del tmp

    del generation_total
