import sys
from datetime import datetime

import matplotlib.dates as mdates
import pandas as pandas
from dateutil.relativedelta import relativedelta

from data import DataCollector, Unit, country, index, mix
from data.Data import Balance, Export, Generation, GenerationBalance, Import, Load
from data.DataCO2 import CO2Emission, CO2Intensity
from figure import Figure_Box, Figure_DuckCurve, Figure_DurationCurve, Figure_Periode, Figure_Time
from lib.pandas import PandasSelection
from lib.Utils import Parallelization

generation_all = None
load_all = None
import_df_all = None
export_df_all = None


def getGenerationCallblack(result):
    global generation_all
    generation_all = result


def getLoadCallback(result):
    global load_all
    load_all = result


def getImportCallback(result):
    global import_df_all
    import_df_all = result


def getExportCallback(result):
    global export_df_all
    export_df_all = result


if __name__ == "__main__":

    args = sys.argv[1:]

    if args == []:
        years = [2016]
    else:
        years = [int(a) for a in args]

    for year in years:
        now = datetime.now().replace(
            year=year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0
        )
        step = relativedelta(years=1)
        # step = relativedelta(days=1) # For tests
        start = pandas.Timestamp(now, tz="Europe/Brussels")
        end = pandas.Timestamp(now + step, tz="Europe/Brussels")

        listCountries = [
            country.BE,
            country.CH,
            country.DE,
            country.DK,
            country.ES,
            country.FR,
            # country.GB,
            country.IT,
            country.NO,
            country.PL,
            country.PT,
            country.SE,
            # country.UK,
        ]
        listCountries = country.getAllCountries()

        kwargs = {
            "majorLocatorInterval": mdates.MonthLocator(interval=1),
            "minorLocatorInterval": mdates.WeekdayLocator(byweekday=mdates.MO),
        }

        """
        Get data
        """
        print("Get data")
        # Select data

        ## General country list

        ### Multithreading
        pool = Parallelization(nbThread=4, asynchronous=True)
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Generation(),
                "start": start,
                "end": end,
                "countries": listCountries,
            },
            callback=getGenerationCallblack,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Load(),
                "start": start,
                "end": end,
                "countries": listCountries,
            },
            callback=getLoadCallback,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Import(),
                "start": start,
                "end": end,
                "countries": listCountries,
            },
            callback=getImportCallback,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Export(),
                "start": start,
                "end": end,
                "countries": listCountries,
            },
            callback=getExportCallback,
        )
        pool.execute()

        import_df_all = PandasSelection.selectCategory(
            data=import_df_all, category=[c.iso for c in listCountries]
        )
        export_df_all = PandasSelection.selectCategory(
            data=export_df_all, category=[c.iso for c in listCountries]
        )

        balance_all = Balance(import_df_all - export_df_all)
        generationBalance_all = GenerationBalance(generation=generation_all, balance=balance_all)

        generation_all.convert(unit=Unit.GWh)
        load_all.convert(unit=Unit.GWh)
        import_df_all.convert(unit=Unit.GWh)
        export_df_all.convert(unit=Unit.GWh)
        balance_all.convert(unit=Unit.GWh)
        generationBalance_all.convert(unit=Unit.GWh)

        co2Intensity_all = CO2Intensity(generation=generation_all)
        co2Emission_all = CO2Emission(generation=generation_all, CO2Intensity=co2Intensity_all)
        co2Intensity_country_all = CO2Intensity(
            generation=generation_all,
            co2Emission=co2Emission_all,
            levels=[index.datetime, index.country],
        )
        co2Intensity_category_all = CO2Intensity(
            generation=generation_all,
            co2Emission=co2Emission_all,
            levels=[index.datetime, index.category],
        )

        co2Intensity_all.convert(unit=Unit.gCO2eq_kWh)
        co2Intensity_country_all.convert(unit=Unit.gCO2eq_kWh)
        co2Intensity_category_all.convert(unit=Unit.gCO2eq_kWh)
        co2Emission_all.convert(unit=Unit.tCO2eq)

        """
        Generate Figures
        """
        print("Generate Figures")

        api = None
        status = None

        for c in listCountries:

            tmp_generation = PandasSelection.selectCountry(
                data=generation_all.copy(), country=[c.iso]
            )
            tmp_gb = PandasSelection.selectCountry(
                data=generationBalance_all.copy(), country=[c.iso]
            )
            tmp_load = PandasSelection.selectCountry(data=load_all.copy(), country=[c.iso])
            tmp_co2emission = PandasSelection.selectCountry(
                data=co2Emission_all.copy(), country=[c.iso]
            )
            tmp_co2intensity = PandasSelection.selectCountry(
                data=co2Intensity_country_all.copy(), country=[c.iso]
            )

            # Plot
            try:
                filename1 = "./output/bot_Yearly_CountryStat_{year}/{country}_01_Linear_Generation.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_Time.plot(
                    data=tmp_gb,
                    filename=filename1,
                    start=start,
                    end=end,
                    stack=True,
                    title="Stacked electricity genration and balance of [{iso}] {country}".format_map(
                        {"iso": c.iso, "country": c.name}
                    ),
                    axisAbsica=index.datetime,
                    axisOrdinate=index.category,
                    showTable=True,
                    **kwargs,
                )
            except Exception as e:
                print(e)

            try:
                filename2 = "./output/bot_Yearly_CountryStat_{year}/{country}_02_DuckCurve_Generation.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_DuckCurve.plot(
                    generationBalance=tmp_gb,
                    load=tmp_load,
                    filename=filename2,
                    start=start,
                    end=end,
                    timeInterval="hours",
                    stack=True,
                    title="Average production, balance and load of [{iso}] over hours {country}".format_map(
                        {
                            "iso": c.iso,
                            "country": c.name,
                        }
                    ),
                    axisAbsica=index.datetime,
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename3 = "./output/bot_Yearly_CountryStat_{year}/{country}_03_Periode_Generation.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_Periode.plot(
                    data=tmp_gb,
                    filename=filename3,
                    start=start,
                    end=end,
                    timeInterval="week",
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename4 = "./output/bot_Yearly_CountryStat_{year}/{country}_04_Periode_Load.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_Periode.plot(
                    data=tmp_load,
                    filename=filename4,
                    start=start,
                    end=end,
                    timeInterval="week",
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename5 = "./output/bot_Yearly_CountryStat_{year}/{country}_05_Periode_CO2Emission.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_Periode.plot(
                    data=tmp_co2emission,
                    filename=filename5,
                    start=start,
                    end=end,
                    timeInterval="week",
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename6 = "./output/bot_Yearly_CountryStat_{year}/{country}_06_Periode_CO2Intensity.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_Periode.plot(
                    data=tmp_co2intensity,
                    filename=filename6,
                    start=start,
                    end=end,
                    timeInterval="week",
                    axisOrdinate=index.country,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename7 = "./output/bot_Yearly_CountryStat_{year}/{country}_07_DurationCurve_Generation.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_DurationCurve.plot(
                    data=tmp_gb,
                    filename=filename7,
                    start=start,
                    end=end,
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename8 = "./output/bot_Yearly_CountryStat_{year}/{country}_08_DurationCurve_Load.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_DurationCurve.plot(
                    data=tmp_load,
                    filename=filename8,
                    start=start,
                    end=end,
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename9 = "./output/bot_Yearly_CountryStat_{year}/{country}_09_DurationCurve_CO2Emission.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_DurationCurve.plot(
                    data=tmp_co2emission,
                    filename=filename9,
                    start=start,
                    end=end,
                    axisOrdinate=index.category,
                    showTable=True,
                )
            except Exception as e:
                print(e)

            try:
                filename10 = "./output/bot_Yearly_CountryStat_{year}/{country}_10_DurationCurve_CO2Intensity.png".format_map(
                    {"country": c.iso, "year": year}
                )
                Figure_DurationCurve.plot(
                    data=tmp_co2intensity,
                    filename=filename10,
                    start=start,
                    end=end,
                    axisOrdinate=index.country,
                    showTable=True,
                )

            except Exception as e:
                print(e)

            try:
                filename11 = "./output/bot_Yearly_CountryStat_{year}/{country}_11_Cumulative_{mix}.png".format_map(
                    {"country": c.iso, "year": year}
                )

                Figure_Periode.plot_cumulative(
                    data=tmp_co2emission,
                    filename=filename11.format_map(
                        {"mix": "CO2Emission", "country": c.iso, "year": year}
                    ),
                    start=start,
                    end=end,
                    category=index.country,
                    timeInterval=["years"],
                    showTable=True,
                )

                filenames = []
                for m in [
                    mix.ENTSOE_complete,
                    mix.ENTSOE_wind,
                    mix.ENTSOE_solar,
                    mix.ENTSOE_nuclear_without_hydro,
                    mix.ENTSOE_hydro,
                    mix.ENTSOE_fossil,
                ]:
                    tmp = tmp_generation.copy()
                    tmp = PandasSelection.selectCategory(data=tmp, category=m.values)
                    tmp.dataframe = tmp.dataframe.groupby(by=[index.country, index.datetime]).sum()

                    filename = filename11.format_map(
                        {"mix": m.name, "country": c.iso, "year": year}
                    )
                    filenames.append(filename)

                    Figure_Periode.plot_cumulative(
                        data=tmp,
                        filename=filename,
                        start=start,
                        end=end,
                        title="Cumulative electricity generation for the {mix} in {country}".format_map(
                            {"mix": m.name, "country": c.iso, "year": year}
                        ),
                        category=index.country,
                        timeInterval=["years"],
                        showTable=True,
                    )

            except Exception as e:
                print(e)

            try:
                filename12 = "./output/bot_Yearly_CountryStat_{year}/{country}_12_Categories_Generation.png".format_map(
                    {"country": c.iso, "year": year}
                )

                Figure_Box.plot(
                    data=tmp_generation,
                    filename=filename12,
                    start=start,
                    end=end,
                    title="Category representation of {title} in {country}".format_map(
                        {"title": tmp_generation.name, "country": c.iso, "year": year}
                    ),
                    axisAbsica=index.category,
                    axisOrdinate=index.datetime,
                    showTable=True,
                )

            except Exception as e:
                print(e)

            # # Post message
            # print("Poste message for : " + str(c.name))
            # message = {
            #     "text": "Stats for the country : [{iso}] {country}\nFrom ENTSO-E and IPCC 2014".format_map(
            #         {"iso": c.iso, "country": c.name}
            #     ),
            #     "media": [
            #         filename_plot1,
            #         filename_plot2,
            #         # filename_plot3,
            #         filename_plot4,
            #     ],
            # }
            # status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)
