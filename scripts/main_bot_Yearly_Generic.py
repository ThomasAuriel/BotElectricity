import sys
from datetime import datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pandas
from dateutil.relativedelta import relativedelta

from data import DataCollector, Unit, country, index, mix
from data.Data import Balance, Export, Generation, Import, Load
from data.DataCO2 import CO2Emission, CO2Intensity
from figure import (
    Figure_Box,
    Figure_DurationCurve,
    Figure_Graph,
    Figure_Map,
    Figure_Periode,
    Figure_Scatter,
    Figure_Square,
    Figure_Time,
)
from lib.pandas import PandasSelection, PandasToCSV
from lib.twitter import TwitterBot
from lib.Utils import Parallelization

generation_all = None
load_all = None
import_df_all = None
export_df_all = None


def getGenerationCallblack(result):
    global generation_all
    generation_all = result


def getLoadCallback(result):
    global load_all
    load_all = result


def getImportCallback(result):
    global import_df_all
    import_df_all = result


def getExportCallback(result):
    global export_df_all
    export_df_all = result


if __name__ == "__main__":

    args = sys.argv[1:]

    if args == []:
        years = [2019]
    else:
        years = [int(a) for a in args]

    for year in years:

        now = datetime.now().replace(
            year=year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0
        )
        step = relativedelta(years=1)
        # step = relativedelta(days=1) # For tests
        start = pandas.Timestamp(now, tz="Europe/Brussels")
        end = pandas.Timestamp(now + step, tz="Europe/Brussels")

        listCountries = [
            country.BE,
            country.CH,
            country.DE,
            country.DK,
            country.ES,
            # country.FI,
            country.FR,
            # country.GB,
            country.IT,
            country.NO,
            country.PL,
            country.PT,
            country.SE,
            # country.UK,
        ]
        listCountries_complete = country.countries.values()

        """
        Get data
        """
        print("Get data")
        # Select data

        ## General country list

        ### Multithreading
        pool = Parallelization(nbThread=4, asynchronous=True)
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Generation(),
                "start": start,
                "end": end,
                "countries": country.getAllCountries(),
            },
            callback=getGenerationCallblack,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Load(),
                "start": start,
                "end": end,
                "countries": country.getAllCountries(),
            },
            callback=getLoadCallback,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Import(),
                "start": start,
                "end": end,
                "countries": country.getAllCountries(),
            },
            callback=getImportCallback,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Export(),
                "start": start,
                "end": end,
                "countries": country.getAllCountries(),
            },
            callback=getExportCallback,
        )
        pool.execute()

        import_df_all = PandasSelection.selectCategory(
            data=import_df_all, category=[c.iso for c in country.getAllCountries()]
        )
        export_df_all = PandasSelection.selectCategory(
            data=export_df_all, category=[c.iso for c in country.getAllCountries()]
        )
        balance_all = Balance(import_df_all - export_df_all)

        generation_all.convert(unit=Unit.GWh)
        load_all.convert(unit=Unit.GWh)
        import_df_all.convert(unit=Unit.GWh)
        export_df_all.convert(unit=Unit.GWh)
        balance_all.convert(unit=Unit.GWh)

        co2Intensity_all = CO2Intensity(generation=generation_all)
        co2Emission_all = CO2Emission(generation=generation_all, CO2Intensity=co2Intensity_all)
        co2Intensity_country_all = CO2Intensity(
            generation=generation_all,
            co2Emission=co2Emission_all,
            levels=[index.datetime, index.country],
        )
        co2Intensity_category_all = CO2Intensity(
            generation=generation_all,
            co2Emission=co2Emission_all,
            levels=[index.datetime, index.category],
        )

        co2Intensity_all.convert(unit=Unit.gCO2eq_kWh)
        co2Intensity_country_all.convert(unit=Unit.gCO2eq_kWh)
        co2Intensity_category_all.convert(unit=Unit.gCO2eq_kWh)
        co2Emission_all.convert(unit=Unit.tCO2eq)

        # ### Monothreading
        # generation_all = DataCollector.getData(data=Generation(), start=start, end=end, countries=country.getAllCountries())
        # load_all = DataCollector.getData(data=Load(), start=start, end=end, countries=country.getAllCountries())
        # import_df_all = DataCollector.getData(data=Import(), start=start, end=end, countries=country.getAllCountries())
        # export_df_all = DataCollector.getData(data=Export(), start=start, end=end, countries=country.getAllCountries())
        # balance_all = Balance(import_df_all - export_df_all)

        # generation_all.convert(unit=Unit.GWh)
        # load_all.convert(unit=Unit.GWh)
        # import_df_all.convert(unit=Unit.GWh)
        # export_df_all.convert(unit=Unit.GWh)
        # balance_all.convert(unit=Unit.GWh)

        # co2Intensity_all = CO2Intensity(generation=generation_all)
        # co2Emission_all = CO2Emission(generation=generation_all, CO2Intensity=co2Intensity_all)
        # co2Intensity_country_all = CO2Intensity(
        #     generation=generation_all,
        #     co2Emission=co2Emission_all,
        #     levels=[index.datetime, index.country],
        # )
        # co2Intensity_category_all = CO2Intensity(
        #     generation=generation_all,
        #     co2Emission=co2Emission_all,
        #     levels=[index.datetime, index.category],
        # )

        # co2Intensity_all.convert(unit=Unit.gCO2eq_kWh)
        # co2Intensity_country_all.convert(unit=Unit.gCO2eq_kWh)
        # co2Intensity_category_all.convert(unit=Unit.gCO2eq_kWh)
        # co2Emission_all.convert(unit=Unit.tCO2eq)

        ## Limited country list
        generation = PandasSelection.selectCountry(
            data=generation_all.copy(), country=[c.iso for c in listCountries]
        )
        load = PandasSelection.selectCountry(
            data=load_all.copy(), country=[c.iso for c in listCountries]
        )
        import_df = PandasSelection.selectCountry(
            data=import_df_all.copy(), country=[c.iso for c in listCountries]
        )
        export_df = PandasSelection.selectCountry(
            data=export_df_all.copy(), country=[c.iso for c in listCountries]
        )
        balance = PandasSelection.selectCountry(
            data=balance_all.copy(), country=[c.iso for c in listCountries]
        )

        # generation.convert(unit=Unit.GWh)
        # load.convert(unit=Unit.GWh)
        # import_df.convert(unit=Unit.GWh)
        # export_df.convert(unit=Unit.GWh)
        # balance.convert(unit=Unit.GWh)

        co2Intensity = PandasSelection.selectCountry(
            data=co2Intensity_all.copy(), country=[c.iso for c in listCountries]
        )
        co2Intensity_country = PandasSelection.selectCountry(
            data=co2Intensity_country_all.copy(), country=[c.iso for c in listCountries]
        )
        co2Intensity_category = PandasSelection.selectCountry(
            data=co2Intensity_category_all.copy(), country=[c.iso for c in listCountries]
        )
        co2Emission = PandasSelection.selectCountry(
            data=co2Emission_all.copy(), country=[c.iso for c in listCountries]
        )

        # co2Intensity.convert(unit=Unit.gCO2eq_kWh)
        # co2Intensity_country.convert(unit=Unit.gCO2eq_kWh)
        # co2Intensity_category.convert(unit=Unit.gCO2eq_kWh)
        # co2Emission.convert(unit=Unit.tCO2eq)

        """
        Record data in specific CSV for analysis or debug
        """
        print("Writer data for further analysis")
        PandasToCSV.write(
            data=generation,
            CSVFile="./output/bot_Yearly_{year}/00-generation.csv".format_map({"year": year}),
        )
        PandasToCSV.write(
            data=load, CSVFile="./output/bot_Yearly_{year}/00-load.csv".format_map({"year": year})
        )
        PandasToCSV.write(
            data=import_df,
            CSVFile="./output/bot_Yearly_{year}/00-import.csv".format_map({"year": year}),
        )
        PandasToCSV.write(
            data=export_df,
            CSVFile="./output/bot_Yearly_{year}/00-export.csv".format_map({"year": year}),
        )
        PandasToCSV.write(
            data=balance,
            CSVFile="./output/bot_Yearly_{year}/00-balance.csv".format_map({"year": year}),
        )

        PandasToCSV.write(
            data=co2Intensity_country,
            CSVFile="./output/bot_Yearly_{year}/00-co2Intensity_country.csv".format_map(
                {"year": year}
            ),
        )
        PandasToCSV.write(
            data=co2Intensity_category,
            CSVFile="./output/bot_Yearly_{year}/00-co2Intensity_category.csv".format_map(
                {"year": year}
            ),
        )
        PandasToCSV.write(
            data=co2Emission,
            CSVFile="./output/bot_Yearly_{year}/00-co2Emission.csv".format_map({"year": year}),
        )

        """
        Generate Figures
        """

        """ Tweet #1"""
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/01-Scatter_Production_CO2Intensity.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/01-Square_Production_CO2Intensity.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #1")
        Figure_Scatter.plot(
            dataAbsica=generation,
            dataOrdinate=co2Intensity_country,
            filename=filename1,
            start=start,
            end=end,
            ylimTop=1000,
            axisData=[index.country, index.datetime],
        )
        Figure_Square.plot(
            dataAbsica=generation,
            dataOrdinate=co2Intensity_country,
            filename=filename2,
            start=start,
            end=end,
            cumulative=True,
            axisData=[index.country],
        )

        filename1_table = "./output/bot_Yearly_{year}/01-Scatter_Production_CO2Intensity_withTable.png".format_map(
            {"year": year}
        )
        filename2_table = (
            "./output/bot_Yearly_{year}/01-Square_Production_CO2Intensity_withTable.png".format_map(
                {"year": year}
            )
        )
        print("Create plot Tweet #1")
        Figure_Scatter.plot(
            dataAbsica=generation,
            dataOrdinate=co2Intensity_country,
            filename=filename1_table,
            start=start,
            end=end,
            ylimTop=1000,
            axisData=[index.country, index.datetime],
            showTable=True,
        )
        Figure_Square.plot(
            dataAbsica=generation,
            dataOrdinate=co2Intensity_country,
            filename=filename2_table,
            start=start,
            end=end,
            cumulative=True,
            ylimTop=1000,
            axisData=[index.country],
            showTable=True,
        )

        # Create message
        print("Post Message Tweet #1")
        message = {
            "text": "Electricity production (#MWh) per country and #CO2 (#GHG) Intensity and Emission\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=None, previousStatus=None)

        """ Tweet #2 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/02-Map_Production.png".format_map({"year": year})
        filename2 = "./output/bot_Yearly_{year}/02-Map_Load.png".format_map({"year": year})
        print("Create plot Tweet #2")
        Figure_Map.plot(
            data=generation_all,
            filename=filename1,
            title="Average electricity production per hour",
            start=start,
            end=end,
            ylimTop=1000,
            cmap=plt.cm.viridis,
            showTable=False,
        )
        Figure_Map.plot(
            data=load_all,
            filename=filename2,
            title="Average electricity load per hour",
            start=start,
            end=end,
            cmap=plt.cm.viridis,
            showTable=False,
        )
        # Create message
        print("Post Message Tweet #4")
        message = {
            "text": "Electricity production and load (#MWh) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #3 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/03-Map_CO2Intensity.png".format_map({"year": year})
        filename2 = "./output/bot_Yearly_{year}/03-Map_CO2Emission.png".format_map({"year": year})
        print("Create plot Tweet #3")
        Figure_Map.plot(
            data=co2Intensity_country_all,
            filename=filename1,
            title="Average CO2 intensity per hour",
            start=start,
            end=end,
            showTable=False,
        )
        Figure_Map.plot(
            data=co2Emission_all,
            filename=filename2,
            title="Total CO2 emission for electricity generation",
            start=start,
            end=end,
            grouping=Figure_Map.Grouping.sum,
            showTable=False,
        )
        # Create message
        print("Post Message Tweet #3")
        message = {
            "text": "CO2 intensity and emission (#GHG) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #4 """
        # balance_all.convert(unit=Unit.GWh)

        # Create plot
        filename1 = "./output/bot_Yearly_{year}/04-Exchange.png".format_map({"year": year})
        filename2 = "./output/bot_Yearly_{year}/04-Exchange_Realistic.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #4")
        Figure_Graph.plot(
            data=balance_all, filename=filename1, start=start, end=end, showTable=True
        )
        # Figure_Graph.plot(data=balance_all, filename=filename2, start=start, end=end, realisticMap=True, showTable=True)
        # Create message
        print("Post Message Tweet #4")
        message = {
            "text": "Exchange (#MWh) between countries\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                # filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        # balance_all.convert(unit=Unit.GWh)

        """ Tweet #5 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/05-Box_Production_Country.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/05-Box_Load_Country.png".format_map({"year": year})
        print("Create plot Tweet #5")
        Figure_Box.plot(data=generation, filename=filename1, start=start, end=end, showTable=True)
        Figure_Box.plot(data=load, filename=filename2, start=start, end=end, showTable=True)
        # Create message
        print("Post Message Tweet #5")
        message = {
            "text": "Electricity production and load (#MWh) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #6 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/06-Box_CO2Emission_Country.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/06-Box_CO2Intensity_Country.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #6")
        Figure_Box.plot(data=co2Emission, filename=filename1, start=start, end=end, showTable=True)
        Figure_Box.plot(
            data=co2Intensity_country, filename=filename2, start=start, end=end, showTable=True
        )
        # Create message
        print("Post Message Tweet #6")
        message = {
            "text": "Electricity CO2 emission and intensity (#GHG) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #7 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/07-Periode_generation_country.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/07-Periode_load_country.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #7")
        Figure_Periode.plot(
            data=generation,
            filename=filename1,
            start=start,
            end=end,
            timeInterval="months",
            axisOrdinate=index.country,
            showTable=True,
        )
        Figure_Periode.plot(
            data=load,
            filename=filename2,
            start=start,
            end=end,
            timeInterval="months",
            axisOrdinate=index.country,
            showTable=True,
        )
        # Create message
        print("Post Message Tweet #7")
        message = {
            "text": "Electricity production and load (#MWh) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #8 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/08-Periode_balance_country.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/08-Periode_CO2Intensity_country.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #8")
        Figure_Periode.plot(
            data=balance,
            filename=filename1,
            start=start,
            end=end,
            timeInterval="months",
            axisOrdinate=index.country,
            showTable=True,
        )
        Figure_Periode.plot(
            data=co2Intensity_country,
            filename=filename2,
            start=start,
            end=end,
            timeInterval="months",
            axisOrdinate=index.country,
            showTable=True,
        )
        # Create message
        print("Post Message Tweet #8")
        message = {
            "text": "Electricity balance (#MWh) and CO2 intensity (#GHG) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #9 """
        # Create plot
        for m in [
            mix.ENTSOE_low_carbon,
            mix.ENTSOE_high_carbon,
            mix.ENTSOE_renewable_without_hydro,
            mix.ENTSOE_nuclear_without_hydro,
        ]:
            filename = "./output/bot_Yearly_{year}/09-Scatter_{mix}_co2Intensity.png".format_map(
                {"mix": m.name, "year": year}
            )

            test = generation.copy()
            test = PandasSelection.selectCategory(data=test, category=m.values)
            Figure_Scatter.plot(
                dataAbsica=test,
                dataOrdinate=co2Intensity_country,
                filename=filename,
                start=start,
                end=end,
                axisData=[index.country, index.datetime],
            )

            # Create message
            print("Post Message Tweet #9")
            message = {
                "text": "Electricity production (#MWh) for {mix} compare to total CO2 intensity\nFrom ENTSO-E and IPCC 2014".format_map(
                    {"mix": m.name, "year": year}
                ),
                "media": [filename],
            }
            status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #10 """
        # Create plot
        for m in [
            mix.ENTSOE_low_carbon,
            mix.ENTSOE_high_carbon,
            mix.ENTSOE_renewable_without_hydro,
            mix.ENTSOE_nuclear_without_hydro,
        ]:
            filename = "./output/bot_Yearly_{year}/10-Scatter_{mix}_load.png".format_map(
                {"mix": m.name, "year": year}
            )

            test = generation.copy()
            test = PandasSelection.selectCategory(data=test, category=m.values)
            test.name = m.name + " generation"
            Figure_Scatter.plot(
                dataAbsica=test,
                dataOrdinate=load,
                filename=filename,
                start=start,
                end=end,
                axisData=[index.country, index.datetime],
            )

            # Create message
            print("Post Message Tweet #10")
            message = {
                "text": "Electricity production (#MWh) for {mix} compare to total CO2 intensity\nFrom ENTSO-E and IPCC 2014".format_map(
                    {"mix": m.name, "year": year}
                ),
                "media": [filename],
            }
            status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #11 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/11-DurationCurve_Production.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/11-DurationCurve_Load.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #11")
        Figure_DurationCurve.plot(
            data=generation,
            filename=filename1,
            start=start,
            end=end,
            showTable=False,
        )
        Figure_DurationCurve.plot(
            data=load,
            filename=filename2,
            start=start,
            end=end,
            showTable=False,
        )
        # Create message
        print("Post Message Tweet #11")
        message = {
            "text": "Duration cruve for electricity generation and load (#MWh) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #11 """
        # Create plot
        print("Create plot Tweet #11")
        filename1 = "./output/bot_Yearly_{year}/11-DurationCurve_Balance.png".format_map(
            {"year": year}
        )
        filename2 = "./output/bot_Yearly_{year}/11-DurationCurve_CO2Intensity.png".format_map(
            {"year": year}
        )
        Figure_DurationCurve.plot(
            data=balance,
            filename=filename1,
            start=start,
            end=end,
            showTable=False,
        )
        Figure_DurationCurve.plot(
            data=co2Intensity_country,
            filename=filename2,
            start=start,
            end=end,
            showTable=False,
        )
        # Create message
        print("Post Message Tweet #11")
        message = {
            "text": "Duration cruve for electricity balance and CO2 intensity (#MWh) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #11 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/11-Linear_Generation.png".format_map({"year": year})
        filename2 = "./output/bot_Yearly_{year}/11-Linear_Load.png".format_map({"year": year})
        print("Create plot Tweet #10")
        Figure_Time.plot(
            data=generation,
            filename=filename1,
            start=start,
            end=end,
            showTable=True,
            majorLocatorInterval=mdates.MonthLocator(interval=1),
            minorLocatorInterval=mdates.DayLocator(interval=7),
        )
        Figure_Time.plot(
            data=load,
            filename=filename2,
            start=start,
            end=end,
            showTable=True,
            majorLocatorInterval=mdates.MonthLocator(interval=1),
            minorLocatorInterval=mdates.DayLocator(interval=7),
        )
        # Create message
        print("Post Message Tweet #10")
        message = {
            "text": "Electricity generation and load (#MWh) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """ Tweet #11 """
        # Create plot
        filename1 = "./output/bot_Yearly_{year}/11-Linear_Balance.png".format_map({"year": year})
        filename2 = "./output/bot_Yearly_{year}/11-Linear_CO2Intensity.png".format_map(
            {"year": year}
        )
        print("Create plot Tweet #11")
        Figure_Time.plot(
            data=balance,
            filename=filename1,
            start=start,
            end=end,
            showTable=True,
            majorLocatorInterval=mdates.MonthLocator(interval=1),
            minorLocatorInterval=mdates.DayLocator(interval=7),
        )
        Figure_Time.plot(
            data=co2Intensity_country,
            filename=filename2,
            start=start,
            end=end,
            showTable=True,
            majorLocatorInterval=mdates.MonthLocator(interval=1),
            minorLocatorInterval=mdates.DayLocator(interval=7),
        )
        # Create message
        print("Post Message Tweet #11")
        message = {
            "text": "Electricity balance (#MWh) and CO2 intensity (#GHG) per country\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
                filename2,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        """
        Delete previous data from the memory
        """
        del generation
        del load
        del import_df
        del export_df
        del balance

        del generation_all
        del load_all
        del import_df_all
        del export_df_all
        del balance_all

        """
        Additional plots
        """
        message = {
            "text": "Additional plot : Overall EU-27",
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        ## Evolution over years
        start = pandas.Timestamp(datetime(2015, 1, 1), tz="Europe/Brussels")
        # end = end

        listCountries_EU27 = [
            country.DE,
            country.AT,
            country.BE,
            country.BG,
            country.HR,
            country.DK,
            country.ES,
            country.EE,
            country.FI,
            country.FR,
            country.GR,
            country.HU,
            country.IE,
            country.IT,
            country.LV,
            country.LT,
            country.LU,
            country.MT,
            country.NL,
            country.PL,
            country.PT,
            country.RO,
            country.SK,
            country.SI,
            country.SE,
            country.CZ,
        ]

        # Generation
        generation = DataCollector.getData(
            data=Generation(), start=start, end=end, countries=listCountries_EU27
        )
        generation.convert(unit=Unit.TWh)
        ### Load
        load = DataCollector.getData(
            data=Load(), start=start, end=end, countries=listCountries_EU27
        )
        load.convert(unit=Unit.TWh)
        # ### Balance
        # import_df = DataCollector.getData(data=Import(), start=start, end=end, countries=listCountries_EU27)
        # export_df = DataCollector.getData(data=Export(), start=start, end=end, countries=listCountries_EU27)
        # balance = Balance(import_df - export_df)
        # # import_df.convert(unit=Unit.TWh)
        # # export_df.convert(unit=Unit.TWh)
        # balance.convert(unit=Unit.TWh)

        ## General country list

        ### Multithreading
        pool = Parallelization(nbThread=4, asynchronous=True)
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Generation(),
                "start": start,
                "end": end,
                "countries": listCountries_EU27,
            },
            callback=getGenerationCallblack,
        )
        pool.addJob(
            DataCollector.getData,
            kwargs={
                "data": Load(),
                "start": start,
                "end": end,
                "countries": listCountries_EU27,
            },
            callback=getLoadCallback,
        )
        # pool.addJob(
        #     DataCollector.getData,
        #     kwargs={
        #         "data": Import(),
        #         "start": start,
        #         "end": end,
        #         "countries": listCountries_EU27,
        #     },
        #     callback=getImportCallback,
        # )
        # pool.addJob(
        #     DataCollector.getData,
        #     kwargs={
        #         "data": Export(),
        #         "start": start,
        #         "end": end,
        #         "countries": listCountries_EU27,
        #     },
        #     callback=getExportCallback,
        # )
        pool.execute()

        # balance_all = Balance(import_df_all - export_df_all)

        generation_all.convert(unit=Unit.TWh)
        load_all.convert(unit=Unit.TWh)
        # import_df_all.convert(unit=Unit.GWh)
        # export_df_all.convert(unit=Unit.GWh)
        # balance_all.convert(unit=Unit.GWh)

        """
        Plot data
        """
        # Create message
        print("Create plot Tweet #12")

        filenames = []
        for m in [
            mix.ENTSOE_complete,
            mix.ENTSOE_wind,
            mix.ENTSOE_solar,
            mix.ENTSOE_nuclear_without_hydro,
            mix.ENTSOE_hydro,
            mix.ENTSOE_fossil,
        ]:
            tmp = generation_all.copy()
            tmp = PandasSelection.selectCategory(data=tmp, category=m.values)
            tmp.dataframe = tmp.dataframe.groupby(by=[index.category, index.datetime]).sum()
            tmp.dataframe = pandas.concat([tmp.dataframe], keys=["EU-27"], names=[index.country])

            filename = "./output/bot_Yearly_{year}/12-Generation_{mix}.png".format_map(
                {"mix": m.name, "year": year}
            )
            filenames.append(filename)

            Figure_Periode.plot_cumulative(
                data=tmp,
                filename=filename,
                start=start,
                end=end,
                title="Cumulative electricity generation for the {mix} in EU-27".format_map(
                    {"mix": m.name, "year": year}
                ),
                category=index.country,
                timeInterval=["years"],
                showTable=True,
            )

        # Create message
        print("Post Message Tweet #12")
        message = {
            "text": "Cumulative electricity production (#MWh) over years in Europe\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filenames[0],
                filenames[1],
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)
        message = {
            "text": "Cumulative electricity production (#MWh) over years in Europe\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filenames[2],
                filenames[3],
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        # Create message
        print("Create plot Tweet #13")

        tmp = load_all.copy()
        tmp.dataframe = tmp.dataframe.groupby(by=[index.category, index.datetime]).sum()
        tmp.dataframe = pandas.concat([tmp.dataframe], keys=["EU-27"], names=[index.country])
        filename = "./output/bot_Yearly_{year}/13-Load.png".format_map({"year": year})
        Figure_Periode.plot_cumulative(
            data=tmp,
            filename=filename,
            start=start,
            end=end,
            title="Cumulative load of EU-27",
            category=index.country,
            timeInterval=["years"],
            showTable=True,
        )
        # Create message
        print("Post Message Tweet #13")
        message = {
            "text": "Cumulative electricity load (#MWh) over years in Europe\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        # # Create message
        # print("Post Message Tweet #14")

        # balance.dataframe = load.dataframe.groupby(by=[index.category, index.datetime]).sum()
        # balance.dataframe = pandas.concat([load.dataframe], keys=["EU-27"], names=[index.country])
        # Figure_Periode.plot_cumulative(
        #     data=tmp,
        #     filename="./output/bot_Yearly_{year}/14-Balance.png",
        #     start=start,
        #     end=end,
        #     title="Cumulative balance of EU-27 (including internal exchanges)",
        #     category=index.country,
        #     timeInterval=["years"],
        #     showTable=True,
        # )

        print("Create plot Tweet #15")
        filename1 = "./output/bot_Yearly_{year}/15-Production.png".format_map({"year": year})
        Figure_Box.plot(
            data=generation_all,
            filename=filename1,
            start=start,
            end=end,
            axisAbsica=index.category,
            showTable=False,
            ylimTop=0.12,  # Remove few aberrant values
        )
        # Create message
        print("Post Message Tweet #15")
        message = {
            "text": "Electricity production (#MWh) in Europe per category\nFrom ENTSO-E and IPCC 2014",
            "media": [
                filename1,
            ],
        }
        status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

        del generation_all
        del load_all
        # del import_df_all
        # del export_df_all
        # del balance_all

        print("end")
