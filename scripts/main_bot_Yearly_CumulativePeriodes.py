from datetime import datetime

import pandas as pandas

from data import DataCollector, Unit, country, index, mix
from data.Data import Balance, Export, Generation, Import, Load
from data.DataCO2 import CO2Emission, CO2Intensity
from figure import Figure_Periode
from lib.pandas import PandasSelection

if __name__ == "__main__":

    now = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    # step = relativedelta(days=1) # For tests
    start = pandas.Timestamp(datetime(2016, 1, 1), tz="Europe/Brussels")
    end = pandas.Timestamp(now, tz="Europe/Brussels")

    # # For tests
    # start = pandas.Timestamp(datetime(2016, 12, 1), tz="Europe/Brussels")
    # end = pandas.Timestamp(datetime(2017, 2, 1), tz="Europe/Brussels")

    listCountries = [
        country.BE,
        country.CH,
        country.DE,
        country.DK,
        country.ES,
        country.FR,
        # country.GB,
        country.IT,
        country.NO,
        country.PL,
        country.PT,
        country.SE,
        # country.UK,
    ]
    listCountries = country.getAllCountries()

    # # For tests
    # listCountries = [
    #     country.DE,
    #     country.DK,
    #     country.FR,
    # ]

    # Generation
    generation_df = DataCollector.getData(
        data=Generation(), start=start, end=end, countries=listCountries
    )
    generation_df.convert(unit=Unit.TWh)

    for c in listCountries:
        filename_generation = (
            "./output/bot_Yearly_Cumulative/{country}_Generation_Cumulative_{mix}.png".format_map(
                {"country": c.iso, "mix": "{mix}"}
            )
        )
        filenames = []
        for m in [
            mix.ENTSOE_complete,
            mix.ENTSOE_wind,
            mix.ENTSOE_solar,
            mix.ENTSOE_nuclear_without_hydro,
            mix.ENTSOE_hydro,
            mix.ENTSOE_fossil,
        ]:
            tmp = generation_df.copy()
            tmp = PandasSelection.selectCountry(data=tmp, country=c.iso)
            tmp = PandasSelection.selectCategory(data=tmp, category=m.values)
            tmp.dataframe = tmp.dataframe.groupby(by=[index.country, index.datetime]).sum()

            filename = filename_generation.format_map({"mix": m.name})
            filenames.append(filename)

            Figure_Periode.plot_cumulative(
                data=tmp,
                filename=filename,
                start=start,
                end=end,
                title="Cumulative electricity generation for the {mix} in {country}".format_map(
                    {"mix": m.name, "country": c.iso}
                ),
                category=index.country,
                timeInterval=["years"],
            )

    del generation_df

    # Load
    load_df = DataCollector.getData(data=Load(), start=start, end=end, countries=listCountries)
    load_df.convert(unit=Unit.TWh)

    for c in listCountries:
        filename_load = "./output/bot_Yearly_Cumulative/{country}_Load_Cumulative.png".format_map(
            {"country": c.iso}
        )

        tmp = load_df.copy()
        tmp = PandasSelection.selectCountry(data=tmp, country=c.iso)
        tmp.dataframe = tmp.dataframe.groupby(by=[index.country, index.datetime]).sum()

        filename = filename_load

        Figure_Periode.plot_cumulative(
            data=tmp,
            filename=filename,
            start=start,
            end=end,
            title="Cumulative electricity load in {country}".format_map({"country": c.iso}),
            category=index.country,
            timeInterval=["years"],
        )

    del load_df

    import_df = DataCollector.getData(data=Import(), start=start, end=end, countries=listCountries)
    export_df = DataCollector.getData(data=Export(), start=start, end=end, countries=listCountries)
    balance_df = Balance(import_df - export_df)
    balance_df.convert(unit=Unit.TWh)

    for c in listCountries:
        filename_balance = (
            "./output/bot_Yearly_Cumulative/{country}_Balance_Cumulative.png".format_map(
                {"country": c.iso}
            )
        )

        tmp = balance_df.copy()
        tmp = PandasSelection.selectCountry(data=tmp, country=c.iso)
        tmp.dataframe = tmp.dataframe.groupby(by=[index.country, index.datetime]).sum()

        filename = filename_balance

        Figure_Periode.plot_cumulative(
            data=tmp,
            filename=filename,
            start=start,
            end=end,
            title="Cumulative interconnexion balance in {country}".format_map({"country": c.iso}),
            category=index.country,
            timeInterval=["years"],
        )

    del import_df
    del export_df
    del balance_df

    co2Intensity_df = CO2Intensity(generation=generation_df)
    co2Emission_df = CO2Emission(generation=generation_df, CO2Intensity=co2Intensity_df)
    co2Emission_df.convert(unit=Unit.MtCO2eq)

    for c in listCountries:
        filename_CO2Emission = (
            "./output/bot_Yearly_Cumulative/{country}_CO2Emission_Cumulative.png".format_map(
                {"country": c.iso}
            )
        )

        tmp = co2Emission_df.copy()
        tmp = PandasSelection.selectCountry(data=tmp, country=c.iso)
        tmp.dataframe = tmp.dataframe.groupby(by=[index.country, index.datetime]).sum()

        filename = filename_CO2Emission

        Figure_Periode.plot_cumulative(
            data=tmp,
            filename=filename,
            start=start,
            end=end,
            title="Cumulative CO2 emission in {country}".format_map({"country": c.iso}),
            category=index.country,
            timeInterval=["years"],
        )

    del co2Emission_df

    # co2Intensity_df_country = CO2Intensity(
    #     generation=generation_df,
    #     co2Emission=co2Emission_df,
    #     levels=[index.datetime, index.country],
    # )

    # for c in listCountries:
    #     filename_CO2Intensity = "./output/bot_Yearly_Cumulative/{country}_CO2Intensity_Cumulative.png".format_map(
    #                 {"country": c.iso}
    #             )

    #     tmp = co2Intensity_df_country.copy()
    #     tmp = PandasSelection.selectCountry(data=tmp, country=c.iso)
    #     tmp.dataframe = tmp.dataframe.groupby(by=[index.country, index.datetime]).sum()

    #     filename=filename_CO2Intensity

    #     Figure_Time.plot(
    #         data=tmp,
    #         filename=filename,
    #         start=start,
    #         end=end,
    #         title="Cumulative CO2 intensity in {country}".format_map(
    #             {"country": c.iso}
    #         ),
    #         axisAbsica = ["years"],
    #         axisOrdinate = index.country,
    #     )
    #
    # del co2Intensity_df_country
