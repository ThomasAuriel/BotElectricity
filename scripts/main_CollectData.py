from datetime import datetime

import pandas
from dateutil.relativedelta import relativedelta

from data import Country, DataPower, DataPower_ENTSOE, DataPower_Elexon

# Get data
now = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
step = relativedelta(years=1)
start = pandas.Timestamp(now - step, tz="Europe/Brussels")
end = pandas.Timestamp(now, tz="Europe/Brussels")

listCountries = Country.getAllCountries()
# For tests
# step = relativedelta(days=1)
# listCountries = [Country.FR]

downloadIfPresent = True

## ENTSO-E data
generation_ENTSOE = DataPower.Generation(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
generation_ENTSOE.downloader = DataPower_ENTSOE.Generation_ENTSOE()
generation_ENTSOE.download()

load_ENTSOE = DataPower.Load(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
load_ENTSOE.downloader = DataPower_ENTSOE.Load_ENTSOE()
load_ENTSOE.download()

export_ENTSOE = DataPower.Export(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
export_ENTSOE.downloader = DataPower_ENTSOE.Export_ENTSOE()
export_ENTSOE.download()

import_ENTSOE = DataPower.Import(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
import_ENTSOE.downloader = DataPower_ENTSOE.Import_ENTSOE()
import_ENTSOE.download()

price_ENTSOE = DataPower.DayAheadPrices(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
price_ENTSOE.downloader = DataPower_ENTSOE.DayAheadPrices_ENTSOE()
price_ENTSOE.download()

## Elexon
dataElexion = DataPower.Generation(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
dataElexion.downloader = DataPower_Elexon.Generation_Elexon()
dataElexion.download()

dataElexion = DataPower.Load(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
dataElexion.downloader = DataPower_Elexon.Load_Elexon()
dataElexion.download()

dataElexion = DataPower.DayAheadPrices(
    dataframe=pandas.DataFrame(), start=start, end=end, listCountry=listCountries
)
dataElexion.downloader = DataPower_Elexon.DayAheadPrices_Elexon()
dataElexion.download()
