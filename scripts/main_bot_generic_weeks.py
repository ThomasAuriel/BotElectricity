from datetime import datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy
import pandas
from dateutil.relativedelta import relativedelta
from pandas import DataFrame

from data import Country, DataCO2, DataPower, Index, Units
from figure.Figure import Figure, Subplot
from figure.genericfigures import (
    BoxChart,
    GraphChart,
    LinearPlot,
    MapPlot,
    PeriodPlot,
    ScatterPlot,
    SquarePlot,
    TablePlot,
)
from lib.matplotlib.FigureUtils import Projection
from lib.twitter import TwitterBot

# Get data
now = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
step = relativedelta(weeks=2)
# step = relativedelta(days=1) # For tests
start = pandas.Timestamp(now - step, tz="Europe/Brussels")
end = pandas.Timestamp(now, tz="Europe/Brussels")


# Countries
listCountries = [
    Country.BE,
    Country.CH,
    Country.DE,
    Country.DK,
    Country.ES,
    Country.FR,
    Country.GB,
    Country.IT,
    Country.NO,
    Country.PL,
    Country.PT,
    Country.SE,
]
listCountries_all = Country.getAllCountries()
# Categories
listCategory = None

# For tests
start = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 2), tz="UTC")
listCountries = [
    Country.GB,
]

# Selected country data
generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountries, listCategory=listCategory
)
load = DataPower.Load(start=start, end=end, listCountry=listCountries, listCategory=listCategory)
balance = DataPower.Balance(
    start=start, end=end, listCountry=listCountries, listCategory=listCountries
)
co2Intensity = DataCO2.CO2Intensity(
    generation=generation, start=start, end=end, levels=[Index.datetime, Index.country]
)
co2Emission = DataCO2.CO2Emission(
    generation=generation, start=start, end=end, levels=[Index.datetime, Index.country]
)
co2Emission = co2Emission.convert(unit=Units.tCO2eq)

# Every countries data
generation_all = DataPower.Generation(
    start=start, end=end, listCountry=listCountries_all, listCategory=listCategory
)
generation_all = generation_all.convert(unit=Units.GWh)
load_all = DataPower.Load(
    start=start, end=end, listCountry=listCountries_all, listCategory=listCategory
)
load_all = load_all.convert(unit=Units.GWh)
balance_all = DataPower.Balance(
    start=start, end=end, listCountry=listCountries_all, listCategory=listCountries_all
)
balance_all = balance_all.convert(unit=Units.GWh)
co2Intensity_all = DataCO2.CO2Intensity(
    generation=generation_all, start=start, end=end, levels=[Index.datetime, Index.country]
)
co2Emission_all = DataCO2.CO2Emission(
    generation=generation_all, start=start, end=end, levels=[Index.datetime, Index.country]
)
co2Emission_all = co2Emission_all.convert(unit=Units.MtCO2eq)

api = None
status = None

"""
Scatter and square plots
"""
filename1 = "./output/bot_weekly/01-Scatter_01.png"
filename2 = "./output/bot_weekly/01-Square_02.png"

# Scatter and square figures
figure = Figure(
    filename=filename1,
    data=[generation, co2Intensity],
    title="Electricity generation compared to CO2 intensity per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=ScatterPlot.scatterchart,
    dataIndex=[0, 1],
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=None,
    minorLocatorInterval=None,
    majorFormatter=None,
    minorFormatter=None,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename2,
    data=[generation, co2Intensity],
    title="Electricity generation compared to CO2 intensity per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=SquarePlot.squareplot,
    dataIndex=[0, 1],
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=None,
    minorLocatorInterval=None,
    majorFormatter=None,
    minorFormatter=None,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Power grid statistics from {start} to {end}\n\nSquare and scatter representation of CO2 intensity".format_map(
        {"start": start.strftime("%H:%M %d/%m/%Y"), "end": end.strftime("%H:%M %d/%m/%Y")}
    ),
    "media": [
        filename1,
        filename2,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

"""
Map
"""
filename1 = "./output/bot_weekly/02-map_01_Generation.png"
filename2 = "./output/bot_weekly/02-map_02_Load.png"
filename3 = "./output/bot_weekly/02-map_03_Balance.png"
filename4 = "./output/bot_weekly/03-map_04_CO2Intensity.png"
filename5 = "./output/bot_weekly/03-map_05_CO2Emission.png"

# Map figures
figure = Figure(
    filename=filename1,
    data=[generation_all],
    title="Summed generation per country\n{start} to {end}",
    source="ENTSO-E",
)

s1 = Subplot(
    functionPlot=MapPlot.plotMap,
    type=Projection.projection_map,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    plotArgs={"grouping": DataFrame.sum, "cmap": plt.cm.viridis},
    subplot_kwargs={},
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename2,
    data=[load_all],
    title="Summed load per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=MapPlot.plotMap,
    type=Projection.projection_map,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    plotArgs={"grouping": DataFrame.sum, "cmap": plt.cm.viridis},
    subplot_kwargs={},
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

tmp = DataFrame.sum(balance_all.dataframe, level=[Index.country])
min = tmp.min()
max = tmp.max()
max = numpy.max([numpy.abs(max), numpy.abs(min)])

# from matplotlib.colors import ListedColormap, LinearSegmentedColormap
# colormap_custom_r = LinearSegmentedColormap.from_list('balance', colors=["#694573", "#5778d6", "#fffee9", "#fffcc4", "#d994cd"])

figure = Figure(
    filename=filename3,
    data=[balance_all],
    title="Summed interconnexion balance per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=MapPlot.plotMap,
    type=Projection.projection_map,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    plotArgs={
        "grouping": DataFrame.sum,
        "cmap": plt.cm.PRGn_r,
        "normalizer_positions": [0, 0.5, 1],
        "normalizer_value": [-max, 0, max],
    },
    subplot_kwargs={},
    lefttext="Positive value: import\nNegative value: export",
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename4,
    data=[co2Intensity_all],
    title="Mean electricty generation CO2 intensity per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=MapPlot.plotMap,
    type=Projection.projection_map,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    plotArgs={"grouping": DataFrame.mean},
    subplot_kwargs={},
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename5,
    data=[co2Emission_all],
    title="Summed CO2 emissions per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=MapPlot.plotMap,
    type=Projection.projection_map,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    plotArgs={"grouping": DataFrame.sum},
    subplot_kwargs={},
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Map representation",
    "media": [
        filename1,
        filename2,
        filename3,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)
print("Poste message")
message = {
    "text": "Map representation",
    "media": [
        filename4,
        filename5,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

"""
Graph plot
"""
filename1 = "./output/bot_weekly/04-Balance_01_graph.png"

# Graph plot
figure = Figure(
    filename=filename1,
    data=[balance_all],
    title="Summed exchange balance per country\n{start} to {end}",
    size=(16, 16),
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=GraphChart.plot,
    dataIndex=[0],
    showGrid=False,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Interconnexion exchanges",
    "media": [
        filename1,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

"""
Box plots
"""
filename1 = "./output/bot_weekly/05-boxChart_01_Generation.png"
filename2 = "./output/bot_weekly/05-boxChart_02_Load.png"
filename3 = "./output/bot_weekly/05-boxChart_03_Balance.png"
filename4 = "./output/bot_weekly/05-boxChart_04_CO2Intensity.png"

# Box plot figure
figure = Figure(
    filename=filename1,
    data=[generation],
    title="Electricity generation\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=BoxChart.boxchart,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=1,
)
s2 = Subplot(
    functionPlot=TablePlot.createStatisticalTable,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=2,
    interval="",
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

# Box plot figure
figure = Figure(
    filename=filename2,
    data=[load],
    title="Electricity load\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=BoxChart.boxchart,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=1,
)
s2 = Subplot(
    functionPlot=TablePlot.createStatisticalTable,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=2,
    interval="",
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

# Box plot figure
figure = Figure(
    filename=filename3,
    data=[balance],
    title="Electricity interconnexion balance\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=BoxChart.boxchart,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=1,
    lefttext="Positive value: import\nNegative value: export",
)
s2 = Subplot(
    functionPlot=TablePlot.createStatisticalTable,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=2,
    interval="",
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

# Box plot figure
figure = Figure(
    filename=filename4,
    data=[co2Intensity],
    title="Electricity generation CO2 intensity\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=BoxChart.boxchart,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=1,
)
s2 = Subplot(
    functionPlot=TablePlot.createStatisticalTable,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    nrows=2,
    ncols=1,
    index=2,
    interval="",
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Box chart",
    "media": [
        filename1,
        filename2,
        filename3,
        filename4,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

"""
Periode Graphs
"""
filename1 = "./output/bot_weekly/06-Periode_01_Generation.png"
filename2 = "./output/bot_weekly/06-Periode_02_Load.png"
filename3 = "./output/bot_weekly/06-Periode_03_Balance.png"
filename4 = "./output/bot_weekly/06-Periode_04_CO2Intensity.png"

figure = Figure(
    filename=filename1,
    data=[generation],
    title="Electricity generation per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=PeriodPlot.barchart,
    abscisseIndex="hours",
    categoryIndex=Index.country,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename2,
    data=[load],
    title="Electricity load per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=PeriodPlot.barchart,
    abscisseIndex="hours",
    categoryIndex=Index.country,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename3,
    data=[balance],
    title="Electricity balance per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=PeriodPlot.barchart,
    abscisseIndex="hours",
    categoryIndex=Index.country,
    lefttext="Positive value: import\nNegative value: export",
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename4,
    data=[co2Intensity],
    title="Electricity CO2 intensity per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=PeriodPlot.barchart,
    abscisseIndex="hours",
    categoryIndex=Index.country,
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Periode graphs",
    "media": [
        filename1,
        filename2,
        filename3,
        filename4,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

"""
Linear plots
"""
filename1 = "./output/bot_weekly/07-Linear_01_Generation.png"
filename2 = "./output/bot_weekly/07-Linear_02_Load.png"
filename3 = "./output/bot_weekly/07-Linear_03_Balance.png"
filename4 = "./output/bot_weekly/07-Linear_04_CO2Intensity.png"
filename5 = "./output/bot_weekly/07-Linear_05_CO2Emission.png"

figure = Figure(
    filename=filename1,
    data=[generation],
    title="Electricity generation per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.HourLocator(interval=1),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename2,
    data=[load],
    title="Electricity load per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.HourLocator(interval=1),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename3,
    data=[balance],
    title="Electricity balance per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.HourLocator(interval=1),
    lefttext="Positive value: import\nNegative value: export",
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename4,
    data=[co2Intensity],
    title="CO2 intensity per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.HourLocator(interval=1),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename5,
    data=[co2Emission],
    title="CO2 emissions per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.DayLocator(interval=1),
    minorLocatorInterval=mdates.HourLocator(interval=1),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Linear plots\n\nAllows to control if holes are present in the data",
    "media": [
        filename1,
        filename2,
        filename3,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)
print("Poste message")
message = {
    "text": "Linear plots\n\nAllows to control if holes are present in the data",
    "media": [
        filename4,
        filename5,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

"""
Cumulative Linear
"""
filename1 = "./output/bot_weekly/08-Cumulative_01_Generation.png"
filename2 = "./output/bot_weekly/08-Cumulative_02_Load.png"
filename3 = "./output/bot_weekly/08-Cumulative_03_Balance.png"
filename4 = "./output/bot_weekly/08-Cumulative_04_CO2Emission.png"

generation = generation.convert(unit=Units.GWh)
load = load.convert(unit=Units.GWh)
balance = balance.convert(unit=Units.GWh)
co2Emission = co2Emission.convert(unit=Units.MtCO2eq)

figure = Figure(
    filename=filename1,
    data=[generation],
    title="Cumulative generation per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    plotArgs={"cumulative": True},
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.WeekdayLocator(byweekday=mdates.MO),
    minorLocatorInterval=mdates.AutoDateLocator(),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename2,
    data=[load],
    title="Cumulative load per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    plotArgs={"cumulative": True},
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.WeekdayLocator(byweekday=mdates.MO),
    minorLocatorInterval=mdates.AutoDateLocator(),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename3,
    data=[balance],
    title="Cumulative exchange balance per country\n{start} to {end}",
    source="ENTSO-E",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    plotArgs={"cumulative": True},
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.WeekdayLocator(byweekday=mdates.MO),
    minorLocatorInterval=mdates.AutoDateLocator(),
    lefttext="Positive value: import\nNegative value: export",
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

figure = Figure(
    filename=filename4,
    data=[co2Emission],
    title="Cumulative CO2 emissions per country\n{start} to {end}",
)
s1 = Subplot(
    functionPlot=LinearPlot.linearplot,
    plotArgs={"cumulative": True},
    abscisseIndex=Index.datetime,
    categoryIndex=Index.country,
    majorLocatorInterval=mdates.WeekdayLocator(byweekday=mdates.MO),
    minorLocatorInterval=mdates.AutoDateLocator(),
)
figure.addSubplots(subplots=[s1])
figure.plot(plotIfExist=True)

# Post message
print("Poste message")
message = {
    "text": "Cumulative plots",
    "media": [
        filename1,
        filename2,
        filename3,
        filename4,
    ],
}
status, api = TwitterBot.postMessage(message, api=api, previousStatus=status)

print("end")
