from datetime import datetime

import pandas

from data import Country, DataPower, DataPower_ENTSOE, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import BarChart
from lib.pandas import PandasSelection

# Dates
start = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2022, 3, 1), tz="UTC")
start = pandas.Timestamp(datetime(2021, 12, 31, 12), tz="UTC")
end = pandas.Timestamp(datetime(2022, 1, 1, 12), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
listCountry = [Country.DE, Country.DK, Country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)
generation.downloader = DataPower_ENTSOE.Generation_ENTSOE()

generation.download()

# Bar figures
figure = Figure(
    filename="./output/test/test_figure/test_bar/test_bar_01.png",
    data=[generation],
    title="Generation",
    size=(48, 32),
)
s1 = Subplot(
    functionPlot=BarChart.barchart,
    abscisseIndex=Index.category,
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=BarChart.stack_barchart,
    abscisseIndex=Index.category,
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=2,
)
s3 = Subplot(
    functionPlot=BarChart.barchart,
    abscisseIndex=Index.country,
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=3,
)
s4 = Subplot(
    functionPlot=BarChart.stack_barchart,
    abscisseIndex=Index.country,
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=4,
)
figure.addSubplots(subplots=[s1, s2, s3, s4])
figure.plot(plotIfExist=True)


figure = Figure(
    filename="./output/test/test_figure/test_bar/test_bar_02.png",
    data=[generation],
    title="Generation",
    size=(48, 32),
)
s1 = Subplot(
    functionPlot=BarChart.cumulative_barchart,
    abscisseIndex=Index.category,
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=BarChart.cumulative_barchart,
    abscisseIndex=Index.country,
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=2,
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

# Difference cumulative bar plot
dataStart = PandasSelection.selectDatetime(data=generation.copy(), date=start)
dataStart.dataframe = dataStart.dataframe.groupby([Index.country, Index.category]).sum()
dataEnd = PandasSelection.selectDatetime(data=generation.copy(), date=end)
dataEnd.dataframe = dataEnd.dataframe.groupby([Index.country, Index.category]).sum()

generation.dataframe = dataEnd.dataframe - dataStart.dataframe

figure = Figure(
    filename="./output/test/test_figure/test_bar/test_bar_03.png",
    data=[generation],
    title="Generation diffrence between {start} to {end}".format_map(
        {"start": start.strftime("%H:%M %d/%m/%Y"), "end": end.strftime("%H:%M %d/%m/%Y")}
    ),
    size=(35, 14),
)
s1 = Subplot(
    functionPlot=BarChart.cumulative_barchart,
    abscisseIndex=Index.country,
    categoryIndex=Index.country,
    nrows=1,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=BarChart.cumulative_barchart,
    abscisseIndex=Index.category,
    categoryIndex=Index.category,
    nrows=1,
    ncols=2,
    index=2,
)
figure.addSubplots(subplots=[s1, s2])
figure.plot(plotIfExist=True)

print("end")
