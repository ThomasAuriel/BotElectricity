from datetime import datetime

import pandas
from matplotlib import ticker

from data import Country, DataPower, Index
from figure.Figure import Figure, Subplot
from figure.genericfigures import PeriodPlot

# Dates
start = pandas.Timestamp(datetime(2021, 1, 1), tz="UTC")
end = pandas.Timestamp(datetime(2023, 1, 1), tz="UTC")
# Countries
listCountry = Country.getAllCountries()
listCountry = [Country.DE, Country.DK, Country.NL]
# Categories
listCategory = None
# listCategory = ["Hydro Water Reservoir", "Hydro Pumped Storage"]

generation = DataPower.Generation(
    start=start, end=end, listCountry=listCountry, listCategory=listCategory
)

# Bar figures
figure = Figure(
    filename="./output/test/test_figure/test_periode/test_periode_01.png",
    data=[generation],
    title="Generation",
    size=(48, 32),
)
s1 = Subplot(
    functionPlot=PeriodPlot.barchart,
    abscisseIndex="weekdays",
    categoryIndex=index.country,
    nrows=2,
    ncols=2,
    index=1,
)
s2 = Subplot(
    functionPlot=PeriodPlot.barchart,
    abscisseIndex="weekdays",
    categoryIndex=index.category,
    nrows=2,
    ncols=2,
    index=2,
)
s3 = Subplot(
    functionPlot=PeriodPlot.boxchart,
    abscisseIndex="weekdays",
    categoryIndex=index.country,
    nrows=2,
    ncols=2,
    index=3,
)
s4 = Subplot(
    functionPlot=PeriodPlot.boxchart,
    abscisseIndex="weekdays",
    categoryIndex=index.category,
    nrows=2,
    ncols=2,
    index=4,
)
figure.addSubplots(subplots=[s1, s2, s3, s4])
figure.plot(plotIfExist=True)


# Linear Figure
figure = Figure(
    filename="./output/test/test_figure/test_periode/test_periode_02.png",
    data=[generation],
    title="Generation",
    size=(48, 32),
)
s1 = Subplot(
    functionPlot=PeriodPlot.linear,
    abscisseIndex="years",
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=1,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
)
s2 = Subplot(
    functionPlot=PeriodPlot.linear,
    abscisseIndex="years",
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=2,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
)
s3 = Subplot(
    functionPlot=PeriodPlot.linear,
    plotArgs={"cumulative": False},
    abscisseIndex="years",
    categoryIndex=Index.country,
    nrows=2,
    ncols=2,
    index=3,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
)
s4 = Subplot(
    functionPlot=PeriodPlot.linear,
    plotArgs={"cumulative": False},
    abscisseIndex="years",
    categoryIndex=Index.category,
    nrows=2,
    ncols=2,
    index=4,
    majorLocatorInterval=ticker.AutoLocator(),
    minorLocatorInterval=ticker.AutoMinorLocator(),
    majorFormatter=None,
    minorFormatter=None,
)
figure.addSubplots(subplots=[s1, s2, s3, s4])
figure.plot(plotIfExist=True)

print("end")
