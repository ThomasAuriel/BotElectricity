# Get last version
# git reset origin/master --hard
# git pull -v origin master

source ./.env/bin/activate

# Execute the bot scripts

# Collect data
python ./scripts/test_figure_bar.py
python ./scripts/test_figure_box.py
python ./scripts/test_figure_correlation.py
python ./scripts/test_figure_durationCurve.py
python ./scripts/test_figure_graph.py
python ./scripts/test_figure_histogram.py
python ./scripts/test_figure_linear.py
python ./scripts/test_figure_map.py
python ./scripts/test_figure_periode.py
python ./scripts/test_figure_scatter.py
python ./scripts/test_figure_square.py