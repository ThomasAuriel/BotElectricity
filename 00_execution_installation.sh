
python -m venv ./.env/
source ./.env/bin/activate

sudo dnf install -y python3-setuptools python3-scipy geos-devel proj-devel
sudo dnf install -y gcc python3-devel python3-pip libxml2-devel libxslt-devel zlib-devel g++

sudo dnf install -y python-fiona

# Update pip
pip install --upgrade pip

# Handle data
pip install pandas beautifulsoup4 pandabase modin[dask] -U
pip install numba -U
pip install pyproj -U
pip install networkx -U
pip install psutil -U

# Map
pip install geopandas
pip install cartopy -U
# Solve issue with cartopy
pip uninstall shapely
pip install --no-binary :all: shapely
# pip install git+https://github.com/snowman2/cartopy.git -U

# Data sources
pip install world_bank_data -U
pip install ElexonDataPortal

# Publishing
pip install tweepy -U