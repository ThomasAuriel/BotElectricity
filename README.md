# BotElectricity

BotElectricity is a set of script allowing displaying graphs about the electricity generation in Europe. The new version of @botelectricity can be downloaded at [the GitLab repository](https://gitlab.com/ThomasAuriel/BotElectricity).

All the data is collected from [ENTSO-E Transparency platform](https://transparency.entsoe.eu/).

The scripts use [entsoe-py](https://pypi.org/project/entsoe-py/), [pysql](https://pypi.org/project/pysql/) [pandas](https://pandas.pydata.org/), [matplotlib](https://matplotlib.org/) and [tweepy](https://www.tweepy.org/) libraries to collect, store, compute, display and publish.

By default, production, load, import/export data are downloadable. You can increase the possibility by using the function of [entsoe-py](https://github.com/EnergieID/entsoe-py). If a country publishes its data on the ENTSO-E platform, you can download them. This includes almost all the European countries. Since GB does not publish data on the ENTSO-E platform, ELexon platform is used throught the [ElexonDataProtal](https://github.com/OSUKED/ElexonDataPortal) python library.

Available plot :

-   **linear plot**: time representation of data
-   **bar plot**: data grouped and averaged by periods (hours, weekdays, months, years).
-   **box plot**: data are grouped by category (or countries) and displayed using [box plot](https://en.wikipedia.org/wiki/Box_plot)
-   **duration curve**: values are sorted by ascending order. The goal is similar to box plot but more common in the electricity sector.
-   **scatter plot**: two sets of values are displayed in on [scatter chart](https://en.wikipedia.org/wiki/Scatter_plot). This is useful to identify behaviour and relation between elements (for instance production and load).
-   **exchange map**: only the balance (import/export) can be displayed using a map (or a simplified map)
-   **video**: it is possible to make videos from the plots.

## How to use it

### Installation

This tool required `python3` and `python-dev` (for Fedora `python3-devel`)

For Fedora users:

Create a venv for your dependencies
```sh
python -m venv ./.env/
source ./.env/bin/activate
```

```sh
sudo dnf install -y python3-setuptools python3-scipy geos-devel proj-devel
sudo dnf install -y gcc python3-devel python3-pip libxml2-devel libxslt-devel zlib-devel g++

sudo dnf install -y python-fiona

# Update pip
pip install --upgrade pip

# Handle data
pip install pandas beautifulsoup4 pandabase modin[dask] -U
pip install numba -U
pip install pyproj -U
pip install networkx -U
pip install psutil -U

# Map
pip install geopandas cartopy -U
pip install matplotlib seaborn -U
# Solve issue with cartopy
pip uninstall shapely
pip install --no-binary :all: shapely
# pip install git+https://github.com/snowman2/cartopy.git -U

# Data sources
pip install world_bank_data -U
pip install ElexonDataPortal

# Publishing
pip install tweepy -U
```

Basemap is required for the graphs with maps.
To install it, follow the instruction on their website: [Basemap installation instruction](https://matplotlib.org/basemap/users/installing.html)

The [entsoe-py](https://github.com/EnergieID/entsoe-py) is used to get data from ENTSO-E Transparency platform. However, it is not necessary to install it since it is included directly in the code. Several modifications of the library has been done.

### `Keys.py` file

To be able to use this code, a key file, containing API keys, is required. Two set of keys are required to use all the functions:

-   ENTSO-E keys: They allow accessing ENTSO-E data. To get this keys, you must follow instructions from here: [Authentication and Authorisation](https://transparency.entsoe.eu/content/static_content/Static%20content/web%20api/Guide.html#_authentication_and_authorisation)
-   Elexon Poral: The website provides information on the UK grid. The key is available on the the [profile page](https://www.elexonportal.co.uk/profile/basic)
-   Twitter keys: They allow posting messages on twitter from the bot scripts. You should create a twitter development account and follow instructions to create an app if you want to do the same.

Final `Keys.py` file should look like this:

```python
# Entsoe API Keys
ENTSOEAPI = "https://transparency.entsoeLibrary.eu/api"
ENTSOEToken = "your-token"

# Elexon Key
ElexonKey = "your-token"

# Twitter API keys
consumer_key = "your-token"
consumer_secret = "your-token"
access_token = "your-token"
access_token_secret = "your-token"
```

## Development

### Coding rules

Before to submit a commit, apply :

```sh
./.env/bin/autoflake -i -r --expand-star-imports --remove-all-unused-imports --remove-unused-variables --exclude=__init__.py ./scripts/* && black -l 100 *
```

To install formatters :

```sh
pip install --upgrade black
pip install --upgrade autoflake
```

### TODO List

- [ ] Add documentation (#TODO tags are placed in the code where it is necessary)
- [ ] Add data source
  - [ ] Elexon for UK