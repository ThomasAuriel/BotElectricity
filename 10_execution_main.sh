# Get last version
# git reset --hard
# git pull -v origin master
git pull

source ./.env/bin/activate

# Execute the bot scripts

# Collect data
python ./scripts/main_CollectData.py

# Publish graphs
python ./scripts/main_bot_generic.py