python .env/lib/python3.10/site-packages/autoflake.py -i -r --expand-star-imports --remove-all-unused-imports --remove-unused-variables --exclude=__init__.py ./scripts/*
black -l 100 *